# Configuration Data For the STM logic Analyzer

The STM will receive configuration data from the GUI desktop application.

## Contents of the configuration:

### General Sampling Configuration:
1. The Sampling Rate.
2. The Sampling Memory (Per Channel).
3. The Logic Level.
4. The Number of Selected Channels.

### Channel Specific Configuration (specified for each channel via the channel index)
1. The Channel Trigger Mode. 

The configuration is sent via USB to the STM as a sequence of formatted bytes.

___

## Configuration Data Format

___
### Header Bytes
___

1. Request Type - 1 byte (Value 1 for a "Start Sampling" request, 0 for a "Stop Sampling" request).
___ 
### General Configuration Bytes
___

1. The Sampling Rate - 8 bytes (interpreted as a double value).
2. The Sampling Memory - 4 bytes (interpreted as an unsigned integer value).
3. The logic Level - 1 byte (pre-prepared encoding for the 8-bit DAC).
4. The Number of selected channels - 1 byte (interpreted as an unsigned integer value).
___
### Channel Specific Configuration Bytes (**Multiplied by the number of channels**)
___

1. The Channel index - 1 byte (interpreted as an unsigned integer value).
2. The Channel Trigger Mode - 1 byte (Enum).
