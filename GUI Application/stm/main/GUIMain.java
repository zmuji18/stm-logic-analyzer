import channel.ChannelPanel;
import com.formdev.flatlaf.FlatDarculaLaf;
import control.ControlPanel;
import decoder.DecoderViewPane;
import logging.ConfiguredLogger;
import styling.Styler;
import terminal.TerminalPanel;

import javax.swing.*;
import java.awt.*;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Logger;

import static styling.GUIConstants.DEFAULT_TERMINAL_PANEL_HEIGHT;

/**
 * Class: GUIMain
 * Date Created: 04.07.21
 *
 * === Description --------------------------------------------- *
 * This file contains code for the GUI of the STM based Digital Logic Analyzer.
 */
public class GUIMain extends JFrame {

    private static final Logger logger = ConfiguredLogger.getLogger("GUIMain");

    private static final String VERSION = "V1.0.0";

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Constructor for the Graphical User Interface Frame of the STM Logic Analyzer.
     */
    public GUIMain() {

        super("STM Logic Analyzer - " + VERSION);

        setLocationByPlatform(true);

        setGUILayout();

        Container contentPane = getContentPane();

        contentPane.add(createBody(), BorderLayout.CENTER);
        contentPane.add(createHeader(), BorderLayout.PAGE_START);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.setExtendedState(MAXIMIZED_BOTH);

        pack();
        setVisible(true);
    }


    /** === Static Interface Functions --------------------------------------- **/

    /**
     * Adds all the GUI components for the STM Logic Analyzer.
     */
    public static void addGUI() {

        FlatDarculaLaf.setup();

        try {
            UIManager.setLookAndFeel(new FlatDarculaLaf());
        } catch (Exception ignored) {}

        new GUIMain();
    }


    /** === Main ------------------------------------------------------------- **/

    public static void main(String[] args) {
        SwingUtilities.invokeLater(GUIMain::addGUI);
    }


    /** === Private Functions ------------------------------------------------ **/

    /**
     * Sets the layout of the GUI Frame.
     */
    private void setGUILayout() {

        BorderLayout layout = new BorderLayout(0, 0);
        this.getContentPane().setLayout(layout);
    }

    /**
     * Creates the header panel of the GUI.
     *
     * @return The header as a {@link JPanel} object.
     */
    private JPanel createHeader() {

        JPanel headerPanel = new JPanel();
        headerPanel.setLayout(new BorderLayout());

        Styler.mark(headerPanel);

        headerPanel.add(createControlPanel(), BorderLayout.CENTER);

        return headerPanel;
    }

    /**
     * Creates the body panel of the GUI.
     *
     * @return The body as a {@link JSplitPane} object.
     */
    private JSplitPane createBody() {

        JSplitPane bodyPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        bodyPane.setContinuousLayout(true);
        bodyPane.setOneTouchExpandable(true);

        JSplitPane centerPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        centerPane.setContinuousLayout(true);
        centerPane.setOneTouchExpandable(true);

        centerPane.setLeftComponent(createChannelPanel());
        centerPane.setRightComponent(createAdditionalOptionsPanel());

        bodyPane.setTopComponent(centerPane);
        bodyPane.setBottomComponent(createTerminalPanel());

        Styler.style(centerPane, Styler.Style.GUI_BODY);
        Styler.style(bodyPane, Styler.Style.GUI_BODY);

        return bodyPane;
    }

    /**
     * Creates a control panel for the GUI.
     *
     * @return The control panel as a {@link ControlPanel} object.
     */
    private ControlPanel createControlPanel() {

        this.controlPanel = new ControlPanel(this.channelPanel, this.decoderViewPanel);
        return this.controlPanel;
    }

    /**
     * Creates a {@link ChannelPanel} with all of the channel related information.
     *
     * @return The Channel Panel as a {@link ChannelPanel} object.
     */
    private ChannelPanel createChannelPanel() {

        this.channelPanel = new ChannelPanel();
        return this.channelPanel;
    }

    /**
     * Creates an Additional Options panel consisting of the Decoder view and Time Markers TODO: Time Markers
     *
     * @return The Additional Options panel as a {@link JPanel}.
     */
    private JPanel createAdditionalOptionsPanel(){

        JPanel additionalOptionsPanel = new JPanel();

        additionalOptionsPanel.setLayout(new BoxLayout(additionalOptionsPanel, BoxLayout.Y_AXIS));

        this.decoderViewPanel =
                new DecoderViewPane(() -> this.channelPanel.getChannels(), () -> this.controlPanel.getModelSnapshot());

        additionalOptionsPanel.add(this.decoderViewPanel);

        return additionalOptionsPanel;
    }

    /**
     * Creates an embedded Terminal panel for the user, which should work with the native shell commands.
     *
     * @return The Terminal panel as {@link JPanel}.
     */
    private JPanel createTerminalPanel() {

        this.terminalPanel = new TerminalPanel();
        this.terminalPanel.setPreferredSize(new Dimension(0, DEFAULT_TERMINAL_PANEL_HEIGHT));

        return terminalPanel;
    }


    /** === Instance Variables ----------------------------------------------- **/

    private ControlPanel controlPanel;
    private ChannelPanel channelPanel;
    private DecoderViewPane decoderViewPanel;
    private TerminalPanel terminalPanel;


    /** === Static Variables ------------------------------------------------- **/

    static {

        Thread.setDefaultUncaughtExceptionHandler((thread, throwable) -> {

            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);

            throwable.printStackTrace(printWriter);

            logger.severe("Uncaught exception for thread: " + thread.getName() + "\n" +
                    "Exception Trace: " + stringWriter);
        });
    }
}
