package channel;

import listeners.ZoomListener;
import logging.ConfiguredLogger;
import popups.PopupTextPanel;
import utility.Sample;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import static styling.GUIConstants.*;

/**
 * Class: PlotPanel
 * Date Created: 10.07.21 09:59
 *
 * === Description ============================================= *
 * This class represents a JPanel with a plot of digital samples.
 */
public class PlotPanel extends JPanel {

    private final Logger logger = ConfiguredLogger.getLogger("PlotPanel");


    /** === Constant Definitions --------------------------------------------- **/

    public static final String NO_UNIT = "";
    public static final String SECONDS = "s";
    public static final String MILLI_SECONDS = "ms";
    public static final String MICRO_SECONDS = "us";
    public static final String NANO_SECONDS = "ns";

    /** === These constants represent what percentage of the width the respective margins should be --- **/

    public static final double LEFT_MARGIN_P = 0.15;
    public static final double RIGHT_MARGIN_P = 0.10;

    public static final int LEFT_MARGIN_MIN = 15;
    public static final int LEFT_MARGIN_MAX = 40;
    public static final int RIGHT_MARGIN_MIN = 5;
    public static final int RIGHT_MARGIN_MAX = 20;

    /** === These constants represent what percentage of the height the respective margins should be --- **/

    public static final double BOTTOM_MARGIN_P = 0.12;
    public static final double TOP_MARGIN_P = 0.15;

    public static final int BOTTOM_MARGIN_MIN = 15;
    public static final int BOTTOM_MARGIN_MAX = 30;
    public static final int TOP_MARGIN_MIN = 5;
    public static final int TOP_MARGIN_MAX = 25;

    public static final int AXIS_LABEL_V_OFFSET = 20;
    public static final int AXIS_LABEL_H_OFFSET = 10;

    public static final double MARKERS_PER_PIXEL = 0.005;
    public static final int MARKERS_PER_LABEL = 2;
    public static final int MARKER_LENGTH = 7;

    public static final int MARKER_POPUP_OFFSET = 5;
    public static final int NUMBERS_AFTER_DECIMAL = 7;

    public enum AxisType {
        FIXED_LABEL,
        DYNAMIC_LABEL,
    }

    public enum Axis {
        HORIZONTAL,
        VERTICAL
    }

    private static final boolean DEBUG = false;
    private static final Color DEBUG_COLOR = Color.WHITE;


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a digital plot panel with default parameters.
     */
    public PlotPanel() {this(false);}

     /**
     * Creates a digital plot of the specified initial width and height.
     *
     * @param gridEnabled - A grid will be drawn if set to True, otherwise the plot will not have a grid.
     */
    public PlotPanel(boolean gridEnabled) {

        super();

        this.gridEnabled = gridEnabled;

        this.xAxisType = AxisType.FIXED_LABEL;
        this.yAxisType = AxisType.FIXED_LABEL;

        this.sampleView = new SampleView();
        this.markedPoints = new CopyOnWriteArrayList<>();
        this.zoomListeners = new CopyOnWriteArrayList<>();

        this.addComponentListeners();
        this.addMouseClickListener();
        this.addMouseZoomListener();
    }

    /**
     * Sets the visibility of the plot grid.
     *
     * @param gridEnabled If true the grid lines will be visible on the plot.
     */
    public void setGridEnabled(boolean gridEnabled) {
        this.gridEnabled = gridEnabled;
        this.repaint();
    }

    public void setAxisType(Axis axis, AxisType axisType) {

        if(axis == Axis.HORIZONTAL) this.xAxisType = axisType;
        else this.yAxisType = axisType;
    }

    /**
     * Plots a lien according to the specified points. The {@link Number} arrays must be of equal
     * length.
     *
     * @param x The x coordinates for the points.
     * @param y The y coordinates for the points.
     */
    public void plot(Number[] x, Number[] y) {
        this.plot(x, y, NO_UNIT, NO_UNIT);
    }

    /**
     * Plots a lien according to the specified points. The {@link Number} arrays must be of equal
     * length. The specified units will be displayed for the corresponding axes.
     *
     * @param x The x coordinates for the points.
     * @param y The y coordinates for the points.
     * @param xUnit The unit of measure for the x axis as a {@link String}.
     * @param yUnit The unit of measure for the y axis as a {@link String}.
     */
    public void plot(Number[] x, Number[] y, String xUnit, String yUnit) {

        assert x != null : "Passed X vector cannot be null.";
        assert y != null : "Passed Y vector cannot be null.";
        assert x.length == y.length : "Vectors X and Y are of different lengths, " + x.length + " and " + y.length + " respectively.";

        int nSamples = x.length;

        Sample[] samples = new Sample[nSamples];
        this.xUnit = xUnit;
        this.yUnit = yUnit;

        for(int i = 0; i < nSamples; i++) samples[i] = new Sample(x[i], y[i]);

        Arrays.sort(samples);

        this.sampleView.setSamples(samples);

        SwingUtilities.invokeLater(() -> {
            this.repaint();
            this.revalidate();
        });
    }

    /**
     * Zooms the currently displayed plot panel to show all samples in the specified range.
     *
     * @param startIndex The start of the range as the index of the first sample to be included.
     * @param endIndex The end of the range as the index of last last to be included.
     */
    public void zoom(int startIndex, int endIndex) {

        this.sampleView.zoom(startIndex, endIndex);

        SwingUtilities.invokeLater(() -> {
            this.repaint();
            this.revalidate();
        });
    }

    /**
     * Reverts any zooming done on the channel.
     */
    public void resetZoom() {

        this.sampleView.resetZoom();

        SwingUtilities.invokeLater(() -> {
            this.repaint();
            this.revalidate();
        });
    }

    /**
     * Adds the specified zoom listener to this {@link PlotPanel}.
     *
     * @param listener The zoom listener as a {@link ZoomListener}.
     */
    public void addZoomListener(ZoomListener listener) {
        this.zoomListeners.add(Objects.requireNonNull(listener, "The zoom listener must not be null."));
    }

    /**
     * Removes the specified zoom listener from this {@link PlotPanel}.
     *
     * @param listener The zoom listener as a {@link ZoomListener}.
     */
    public void removeZoomListener(ZoomListener listener) {
        this.zoomListeners.remove(Objects.requireNonNull(listener, "The zoom listener must not be null."));
    }


    /** === Main ------------------------------------------------------------- **/

    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ignored) {}


        JFrame testFrame = new JFrame("Test");
        testFrame.setPreferredSize(new Dimension(500, 500));
        testFrame.setLocationByPlatform(true);

        testFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        PlotPanel plot = new PlotPanel(true);
        plot.setAxisType(Axis.HORIZONTAL, AxisType.DYNAMIC_LABEL);

        testFrame.add(plot);

        testFrame.pack();
        testFrame.setVisible(true);

        Random random = new Random();

        while(true) {

            int nPoints = random.nextInt(100);

            Double[] x = new Double[nPoints];
            Double[] y = new Double[nPoints];

            for(int i = 0; i < nPoints; i++) {
                x[i] = Math.round(random.nextDouble() * 1e6 * 1e3) / 1e3;
                y[i] = (double) random.nextInt(2);
            }

            System.out.println("Generated sequences:\nX - " + List.of(x) + "\nY - " + List.of(y));

            plot.plot(x, y, MICRO_SECONDS, NO_UNIT);

            try {
                Thread.sleep(500);
            } catch (InterruptedException ignored) {}
        }
    }


    /** === Private Functions ------------------------------------------------ **/

    @Override
    protected void paintComponent(Graphics graphics) {

        super.paintComponent(graphics);

        this.calculateComponentSizes();

        this.paintSamples(graphics);
        this.paintAxes(graphics);
        if(this.gridEnabled) this.paintGrid(graphics);

        if(DEBUG) {

            graphics.setColor(DEBUG_COLOR);
            graphics.drawString("Plot Center", this.width/2, this.height/2);
            graphics.drawRect(0, 0, this.width, this.height);
        }

        this.placeSampleMarkers(graphics);
    }

    /**
     * Adds a mouse click listener to this plot panel, which will trigger a prompt for the user specifying data
     * about the sample nearest to the point where the user clicked.
     */
    private void addMouseClickListener() {

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {

                PopupTextPanel.removePopup(PlotPanel.this);

                int x = e.getX();
                int y = e.getY();

                if (PlotPanel.this.isPointOutsideOfPlot(e.getPoint())) {
                    return;
                }

                final double TIME_VALUE = PlotPanel.this.getTimeValue(x);

                Executors.newSingleThreadExecutor().submit(() -> {

                    Sample chosenSample = PlotPanel.this.sampleView.getClosestSample(TIME_VALUE);
                    if(chosenSample == null) {
                        return;
                    }

                    PlotPanel.this.addSampleMarker(new Point(x, PlotPanel.this.height - PlotPanel.this.bottomMargin));

                    Point absolutePoint = new Point(x, y);
                    SwingUtilities.convertPointToScreen(absolutePoint, PlotPanel.this);

                    PopupTextPanel.displayPopup(PlotPanel.this,
                            chosenSample.toString(),
                            PopupTextPanel.PopupType.INFO,
                            absolutePoint.x + MARKER_POPUP_OFFSET,
                            absolutePoint.y,
                            false, true,
                            PlotPanel.this::removeSampleMarkers);
                });
            }
        });
    }

    /**
     * Adds a mouse listener that will trigger all registered {@link ZoomListener}s for this plot panel whenever a user
     * drags across a number of samples.
     */
    private void addMouseZoomListener() {

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {

                PopupTextPanel.removePopup(PlotPanel.this);

                if(PlotPanel.this.isPointOutsideOfPlot(e.getPoint())) {
                    PlotPanel.this.dragStartPoint = null;
                    return;
                }

                PlotPanel.this.dragStartPoint = new Point(e.getX(), PlotPanel.this.height - PlotPanel.this.bottomMargin);
                PlotPanel.this.addSampleMarker(PlotPanel.this.dragStartPoint);
            }

            @Override
            public void mouseReleased(MouseEvent e) {

                PlotPanel.this.removeSampleMarkers();
                if(PlotPanel.this.isPointOutsideOfPlot(e.getPoint()) || PlotPanel.this.dragStartPoint == null) {
                    PlotPanel.this.dragStartPoint = null;
                    return;
                }

                final double START_TIME = PlotPanel.this.getTimeValue(dragStartPoint.x);
                final double END_TIME = PlotPanel.this.getTimeValue(e.getX());

                Executors.newSingleThreadExecutor().submit(() -> {

                    double startTime;
                    double endTime;

                    if (START_TIME < END_TIME) {
                        startTime = START_TIME;
                        endTime = END_TIME;
                    } else {
                        startTime = END_TIME;
                        endTime = START_TIME;
                    }

                    int startSampleIndex = PlotPanel.this.sampleView.getClosestSampleIndex(startTime);
                    int endSampleIndex = PlotPanel.this.sampleView.getClosestSampleIndex(endTime);

                    if (startSampleIndex != -1 && endSampleIndex != -1 && startSampleIndex != endSampleIndex) {

                        logger.info("Zoom requested between the samples: " + startSampleIndex + ":" + endSampleIndex);

                        PlotPanel.this.zoomListeners
                                    .forEach(listener -> listener.zoomRequested(startSampleIndex, endSampleIndex));
                    }
                });

                PlotPanel.this.dragStartPoint = null;
            }

            @Override
            public void mouseExited(MouseEvent e) {

                PlotPanel.this.dragStartPoint = null;

                PopupTextPanel.removePopup(PlotPanel.this);
                PlotPanel.this.removeSampleMarkers();
            }
        });

        this.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseDragged(MouseEvent e) {

                PlotPanel.this.removeSampleMarkers();

                if(PlotPanel.this.isPointOutsideOfPlot(e.getPoint()) || PlotPanel.this.dragStartPoint == null) {
                    PlotPanel.this.dragStartPoint = null;
                    return;
                }

                Point currentPoint = new Point(e.getX(), PlotPanel.this.height - PlotPanel.this.bottomMargin);
                PlotPanel.this.addSampleMarkers(List.of(PlotPanel.this.dragStartPoint, currentPoint));
            }
        });
    }

    /**
     * Adds component listeners for removing marker pop-up to this {@link PlotPanel}.
     */
    private void addComponentListeners() {

        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {this.removePopup(e);}

            @Override
            public void componentMoved(ComponentEvent e) {this.removePopup(e);}

            private void removePopup(AWTEvent e) {

                PopupTextPanel.removePopup(PlotPanel.this);

                Container parent = PlotPanel.this.getParent();
                if(parent != null) {
                    parent.dispatchEvent(e);
                }
            }
        });
    }

    /**
     * Calculates the widths, heights, gap sizes and margis for all components on the plot.
     */
    private void calculateComponentSizes() {

        this.width = this.getWidth();
        this.height = this.getHeight();

        this.leftMargin = calculateMargin(width, LEFT_MARGIN_P, LEFT_MARGIN_MIN, LEFT_MARGIN_MAX);
        this.rightMargin = calculateMargin(width, RIGHT_MARGIN_P, RIGHT_MARGIN_MIN, RIGHT_MARGIN_MAX);
        this.topMargin = calculateMargin(height, TOP_MARGIN_P, TOP_MARGIN_MIN, TOP_MARGIN_MAX);
        this.bottomMargin = calculateMargin(height, BOTTOM_MARGIN_P, BOTTOM_MARGIN_MIN, BOTTOM_MARGIN_MAX);

        this.plotWidth = this.width - this.leftMargin - this.rightMargin;
        this.plotHeight = this.height - this.topMargin - this.bottomMargin;

        this.nHorizontalMarkers = (int) Math.ceil(this.width * MARKERS_PER_PIXEL);
        this.nVerticalMarkers =  (int) Math.ceil(this.height * MARKERS_PER_PIXEL);

        this.nHorizontalGridDivisions = 2 * this.nHorizontalMarkers;
        this.nVerticalGridDivisions = 2 * this.nVerticalMarkers;
    }

    private int calculateMargin(int size, double marginPart, int minValue, int maxValue) {
        return (int) Math.max(Math.min(size * marginPart, maxValue), minValue);
    }

    /**
     * Paints the samples of this plot.
     *
     * @param graphics The {@link Graphics} object to be painted.
     */
    private void paintSamples(Graphics graphics) {

        this.sampleView.setSize(this.width - this.leftMargin - this.rightMargin, this.height - this.topMargin - this.bottomMargin);
        this.sampleView.setOffsets(this.leftMargin, this.topMargin);

        this.sampleView.displaySamples(graphics);
    }

    /**
     * Paints the grid lines of this plot.
     *
     * @param graphics The {@link Graphics} object to be painted.
     */
    private void paintGrid(Graphics graphics) {

        double horizontalGap = this.plotWidth/(double) this.nHorizontalGridDivisions;
        double verticalGap = this.plotHeight/(double) this.nVerticalGridDivisions;

        for(int i = 0; i < this.nHorizontalGridDivisions; i++) {

            boolean isFineLine = i % 2 == 0;
            this.addGridLine(graphics, this.leftMargin + i * horizontalGap, this.height - this.bottomMargin, Axis.VERTICAL, isFineLine);
        }

        for(int i = 0; i < this.nVerticalGridDivisions; i++) {

            boolean isFineLine = i % 2 == 0;
            this.addGridLine(graphics, this.leftMargin, this.height - this.bottomMargin - i * verticalGap, Axis.HORIZONTAL, isFineLine);
        }
    }

    /**
     * Adds a single grid line to the plot at the specified points.
     *
     * @param graphics The {@link Graphics} object to be painted.
     * @param x The starting x coordinate of the line.
     * @param y The starting y coordinate of the line.
     * @param axis The direction along which the grid line should be drawn as a {@link Axis}.
     * @param isFineLine If true the line will be less visible (i.e. lighter).
     */
    private void addGridLine(Graphics graphics, double x, double y, Axis axis, boolean isFineLine) {
        this.addLine(graphics, x, y, axis, isFineLine ? FINE_GRID_LINE_COLOR : REGULAR_GRID_LINE_COLOR);
    }

    /**
     * Draws a line of the specified color connecting the given two points.
     *
     * @param graphics The graphics to draw on as a {@link Graphics}.
     * @param x The starting x coordinate of the line.
     * @param y The starting y coordinate of the line.
     * @param axis The direction along which the grid line should be drawn as a {@link Axis}.
     * @param color The color of the line.
     */
    private void addLine(Graphics graphics, double x, double y, Axis axis, Color color) {

        double xStart, xEnd, yStart, yEnd;

        if(axis == Axis.VERTICAL) {
            xStart = x;
            xEnd = x;
            yStart = y;
            yEnd = y - this.plotHeight;
        } else {
            xStart = x;
            xEnd = x + this.plotWidth;
            yStart = y;
            yEnd = y;
        }

        Color original = graphics.getColor();

        graphics.setColor(color);
        graphics.drawLine((int) xStart, (int) yStart, (int) xEnd, (int) yEnd);
        graphics.setColor(original);
    }

    /**
     * Paints the axes for this plot.
     *
     * @param graphics The {@link Graphics} object to be painted.
     */
    private void paintAxes(Graphics graphics) {

        graphics.drawLine(this.leftMargin, this.height - this.bottomMargin, this.width - this.rightMargin, this.height - this.bottomMargin);
        graphics.drawLine(this.leftMargin, this.height - this.bottomMargin, this.leftMargin, this.topMargin);

        double horizontalGap = this.plotWidth/(double) this.nHorizontalMarkers;
        double verticalGap = this.plotHeight/(double) this.nVerticalMarkers;

        for(int i = 0; i < this.nHorizontalMarkers; i++) {
            this.addMarker(graphics, this.leftMargin + i * horizontalGap, this.height - this.bottomMargin, Axis.VERTICAL);
        }

        for(int i = 0; i < this.nVerticalMarkers; i++) {
            this.addMarker(graphics, this.leftMargin, this.height - this.bottomMargin - i * verticalGap, Axis.HORIZONTAL);
        }

        if(this.sampleView.getSampleCount() != 0) {

            Graphics2D rotatedGraphics = (Graphics2D) graphics;
            rotatedGraphics.setFont(PLOT_FONT);

            if(this.xAxisType == AxisType.DYNAMIC_LABEL) this.paintDynamicXAxisLabels(rotatedGraphics);
            else this.paintStaticXAxisLabels(rotatedGraphics);

            if(this.yAxisType == AxisType.DYNAMIC_LABEL) this.paintDynamicYAxisLabels(rotatedGraphics);
            else this.paintStaticYAxisLabels(rotatedGraphics);
        }
    }

    /**
     * Paints the dynamic axis labels onto the X axis.
     *
     * @param graphics The {@link Graphics2D} object to be painted.
     */
    private void paintDynamicXAxisLabels(Graphics2D graphics) {

        double horizontalGap = this.plotWidth/(double) this.nHorizontalMarkers;
        double xValueInterval = (this.sampleView.getMaxX() - this.sampleView.getMinX())/this.nHorizontalMarkers;

        for(int i = 0; i <= nHorizontalMarkers; i += MARKERS_PER_LABEL) {

            String label = round(this.sampleView.getMinX() + i * xValueInterval) + this.xUnit;

            int labelX = (int) (this.leftMargin + i * horizontalGap);
            int labelY = this.height - this.bottomMargin + AXIS_LABEL_V_OFFSET;

            if(i == nHorizontalMarkers) {
                labelX = this.width - this.rightMargin - graphics.getFontMetrics().stringWidth(label);
            }

            graphics.drawString(label, labelX, labelY);
        }
    }

    /**
     * Paints the static axis labels onto the X axis.
     *
     * @param graphics The {@link Graphics2D} object to be painted.
     */
    private void paintStaticXAxisLabels(Graphics2D graphics) {

        String minX = round(this.sampleView.getMinX()) + this.xUnit;
        String maxX = round(this.sampleView.getMaxX()) + this.xUnit;

        int maxXWidth = graphics.getFontMetrics().stringWidth(maxX);

        graphics.drawString(minX, this.leftMargin, this.height - this.bottomMargin + AXIS_LABEL_V_OFFSET);
        graphics.drawString(maxX, this.width - this.rightMargin - maxXWidth, this.height - this.bottomMargin + AXIS_LABEL_V_OFFSET);
    }

    /**
     * Paints the dynamic axis labels onto the Y axis.
     *
     * @param graphics The {@link Graphics2D} object to be painted.
     */
    private void paintDynamicYAxisLabels(Graphics2D graphics) {

        double verticalGap = this.plotHeight/(double) this.nVerticalMarkers;
        double yValueInterval = (this.sampleView.getMaxY() - this.sampleView.getMinY())/this.nVerticalMarkers;

        AffineTransform originalTransform = graphics.getTransform();
        graphics.rotate(Math.toRadians(-90));

        for(int i = 0; i <= nVerticalMarkers; i += MARKERS_PER_LABEL) {

            String label = round(this.sampleView.getMinY() + i * yValueInterval) + this.yUnit;

            int labelX = (int) (this.bottomMargin - this.height + i * verticalGap);
            int labelY = this.leftMargin - AXIS_LABEL_H_OFFSET;

            if(i == nVerticalMarkers) {
                labelX = -this.topMargin - graphics.getFontMetrics().stringWidth(label);
            }

            graphics.drawString(label, labelX, labelY);
        }

        graphics.setTransform(originalTransform);
    }

    /**
     * Paints the static axis labels onto the Y axis.
     *
     * @param graphics The {@link Graphics2D} object to be painted.
     */
    private void paintStaticYAxisLabels(Graphics2D graphics) {

        AffineTransform originalTransform = graphics.getTransform();
        graphics.rotate(Math.toRadians(-90));

        String minY = round(this.sampleView.getMinY()) + this.yUnit;
        String maxY = round(this.sampleView.getMaxY()) + this.yUnit;

        int maxYWidth = graphics.getFontMetrics().stringWidth(maxY);

        graphics.drawString(minY, this.bottomMargin - this.height, this.leftMargin - AXIS_LABEL_H_OFFSET);
        graphics.drawString(maxY, -this.topMargin - maxYWidth, this.leftMargin - AXIS_LABEL_H_OFFSET);

        graphics.setTransform(originalTransform);
    }

    /**
     * Adds a marker to the axes at the specified point.
     *
     * @param graphics The {@link Graphics} object to be painted.
     * @param x The starting x coordinate of the marker.
     * @param y The starting y coordinate of the marker.
     * @param axis The direction along which the marker should be drawn as a {@link Axis}.
     */
    private void addMarker(Graphics graphics, double x, double y, Axis axis) {

        double xStart, xEnd, yStart, yEnd;

        if(axis == Axis.VERTICAL) {
            xStart = x;
            xEnd = x;
            yStart = y;
            yEnd = y + MARKER_LENGTH;
        } else {
            xStart = x - MARKER_LENGTH;
            xEnd = x;
            yStart = y;
            yEnd = y;
        }

        graphics.drawLine((int) xStart, (int) yStart, (int) xEnd, (int) yEnd);
    }

    /**
     * Places a marker line at the currently marked point.
     */
    private void placeSampleMarkers(Graphics graphics) {

        for(Point markedPoint: this.markedPoints) {
            this.addLine(graphics, markedPoint.x, markedPoint.y, Axis.VERTICAL, MARKER_LINE_COLOR);
        }
    }

    /**
     * Adds a sample marker to this {@link PlotPanel} at the specified point.
     *
     * @param point The point at which the marker should be added as a {@link Point}.
     */
    private void addSampleMarker(Point point) {

        assert point != null: "The specified point must not be null.";

        this.markedPoints.add(point);
        SwingUtilities.invokeLater(() -> this.repaint(this.getVisibleRect()));
    }

    /**
     * Adds sample markers to all the points in the specified colleciton.
     *
     * @param points The collection of {@link Point} objects as a {@link Collection}.
     */
    private void addSampleMarkers(Collection<Point> points) {

        assert points != null: "The collection of points must not be null.";

        this.markedPoints.addAll(points);

        SwingUtilities.invokeLater(() -> this.repaint(this.getVisibleRect()));
    }

    /**
     * Removes the currently place sample marker (if one exists).
     */
    private void removeSampleMarkers() {

        this.markedPoints.clear();
        SwingUtilities.invokeLater(() -> this.repaint(this.getVisibleRect()));
    }

    /**
     * @param point The point to be checked as a {@link Point}.
     *
     * @return True if the specified point is outside of the plot of this {@link PlotPanel}, false otherwise.
     */
    private boolean isPointOutsideOfPlot(Point point) {

        int x = (int) point.getX();
        int y = (int) point.getY();

        return PlotPanel.this.sampleView.getSampleCount() == 0 ||
                x < PlotPanel.this.leftMargin || x > PlotPanel.this.width - PlotPanel.this.rightMargin ||
                y < PlotPanel.this.topMargin || y > PlotPanel.this.height - PlotPanel.this.bottomMargin;
    }

    /**
     * Returns the time value corresponding to the relative x coordinate of a point on this {@link PlotPanel}.
     *
     * @param x The x coordinate of the point of interest.
     *
     * @return The time value corresponding to the x coordinate.
     */
    private double getTimeValue(int x) {

        double xRange = PlotPanel.this.sampleView.getMaxX() - PlotPanel.this.sampleView.getMinX();

        return this.sampleView.getMinX() + (x - this.leftMargin) * xRange / this.plotWidth;
    }

    /**
     * Rounds the specified value to a pre-defined number of digits after the decimal point.
     *
     * @param value The number to be rounded as a double.
     *
     * @return The rounded number as a double.
     */
    private static double round(double value) {
        return BigDecimal.valueOf(value).setScale(NUMBERS_AFTER_DECIMAL, RoundingMode.HALF_EVEN).doubleValue();
    }

    /** === Instance Variables ----------------------------------------------- **/

    private int width, height, plotWidth, plotHeight;
    private int leftMargin, rightMargin, topMargin, bottomMargin;
    private int nHorizontalMarkers, nVerticalMarkers, nHorizontalGridDivisions, nVerticalGridDivisions;

    private AxisType xAxisType, yAxisType;

    private boolean gridEnabled;

    private String xUnit, yUnit;

    private Point dragStartPoint;

    private final List<Point> markedPoints;
    private final List<ZoomListener> zoomListeners;

    private final SampleView sampleView;
}
