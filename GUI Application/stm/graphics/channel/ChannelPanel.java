package channel;

import boxes.ConfirmationBox;
import boxes.FormBox;
import buttons.Button;
import data.DataValidator;
import listeners.ChannelModificationListener;
import listeners.ChannelReorderingListener;
import listeners.ChannelSelectionListener;
import listeners.ZoomListener;
import logging.ConfiguredLogger;
import styling.Styler;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static styling.GUIConstants.*;

/**
 * A class representing a JPanel holding the information of all the active channels.
 */
public class ChannelPanel extends JSplitPane {

    private static final Logger logger = ConfiguredLogger.getLogger("ChannelPanel");

    private static final int MIN_N_SAMPLES_TO_ZOOM = 10;


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a JPanel representing an Empty Channel Panel.
     */
    public ChannelPanel() {

        super(JSplitPane.HORIZONTAL_SPLIT);

        this.channels = new HashMap<>();
        this.orderedChannels = new ArrayList<>();
        this.channelModificationListeners = new ArrayList<>();
        this.channelSelectionListeners = new ArrayList<>();
        this.isZoomable = new AtomicBoolean();

        this.channelRemovalConfirmations = new ArrayList<>();
        this.resizeLock = new ReentrantLock();

        synchronized (this) {

            this.controlPane = Styler.style(this.createChannelControls(), Styler.Style.CHANNEL_PANEL);
            this.displayPane = Styler.style(this.createChannelDisplay(), Styler.Style.CHANNEL_PANEL);

            linkVerticalScrollbars(this.controlPane, this.displayPane);

            JPanel extendedDisplayPanel = this.createExtendedDisplayPanel(this.displayPane);

            this.setLeftComponent(this.controlPane);
            this.setRightComponent(extendedDisplayPanel);
            this.setContinuousLayout(true);
            this.setOneTouchExpandable(true);

            Styler.style(this, Styler.Style.CHANNEL_PANEL);

            this.displayPane.setPreferredSize(new Dimension(
                    SCREEN_WIDTH - CHANNEL_CONTROL_PANEL_WIDTH - ADDITIONAL_OPTIONS_PANEL_WIDTH,
                    SCREEN_HEIGHT - DEFAULT_TERMINAL_PANEL_HEIGHT - CONTROL_PANEL_TEXT_FIELD_HEIGHT));

            this.refreshChannels();
        }

        this.setZoomable();
    }

    /**
     * Adds the specified channel modification listener to this channel panel.
     *
     * @param listener The listener as a {@link ChannelModificationListener} object.
     */
    public void addChannelModificationListener(ChannelModificationListener listener) {
        this.channelModificationListeners.add(listener);
    }

    /**
     * Adds the specified channel selection listener to this channel panel.
     *
     * @param listener The listener as a {@link ChannelSelectionListener} object.
     */
    public void addChannelSelectionListener(ChannelSelectionListener listener) {
        this.channelSelectionListeners.add(listener);
    }

    /**
     * Adds a Channel with the specified name to this Channel Panel.
     * The names of channels must be unique.
     *
     * @param channelName Name of the new channel.
     */
    public void addChannel(int channelIndex, String channelName) {

        assert !this.channels.containsKey(channelName) : "The channel with the name " + channelName + "already exists.";

        SwingUtilities.invokeLater(() -> {

            Channel newChannel = Channel.getChannel(channelIndex, channelName);

            this.orderedChannels.add(newChannel);
            this.channels.put(channelName, newChannel);

            this.refreshChannels();

            for(ChannelModificationListener listener : this.channelModificationListeners) {
                Executors.newSingleThreadExecutor().submit((() -> listener.channelAdded(newChannel)));
            }

            newChannel.addRemoveButtonListener(actionEvent -> Executors.newSingleThreadExecutor().submit((() -> {
                this.removeChannel(newChannel.getChannelName());
            })));

            newChannel.addEditButtonListener(actionEvent -> Executors.newSingleThreadExecutor().submit((() -> {
                this.editChannel(newChannel.getChannelName());
            })));

            newChannel.addChannelSelectedListener(actionEvent -> {
                Executors.newSingleThreadExecutor().submit(() -> this.channelSelectionChanged(newChannel));
            });

            newChannel.addTriggerSelectionListener(newTrigger -> this.checkMultipleEdgeConditions());
            newChannel.addChannelReorderingListener(channelReorderingListener);

            newChannel.addZoomListener(this.zoomedFrameListener);

            newChannel.setSelected(true);
            this.channelSelectionChanged(newChannel);
        });
    }

    /**
     * Removes the channel with the specified name from this channel panel.
     *
     * @param channelName The name of the channel to be removed.
     */
    public void removeChannel(String channelName) {

        assert this.channels.containsKey(channelName) : "The channel " + channelName + " doesn't exist.";

        Channel channel = getChannel(channelName);

        if(!ConfirmationBox.confirm("Do you want to remove the channel \"" + channelName + "\"?", channelName)) {
            return;
        }

        synchronized (this.channelRemovalConfirmations) {
            for (Function<Channel, Boolean> confirmation : this.channelRemovalConfirmations) {
                if (!confirmation.apply(channel)) return;
            }
        }

        this.orderedChannels.remove(channel);
        this.channels.remove(channelName);

        Channel.releaseChannel(channel);

        SwingUtilities.invokeLater(() -> {

            this.refreshChannels();

            for(ChannelModificationListener listener : this.channelModificationListeners) {
                Executors.newSingleThreadExecutor().submit((() -> listener.channelRemoved(channel)));
            }
        });
    }

    /**
     * Adds a channel removal confirmation to this {@link ChannelPanel}, which will be executed before a channel is
     * made. If the specified {@link Function} returns with a false value, the channel removal will be cancelled.
     *
     * @param confirmation The confirmation as a boolean {@link Function}, accepting the {@link Channel} to be removed.
     */
    public void addChannelRemovalConfirmation(Function<Channel, Boolean> confirmation) {

        synchronized (this.channelRemovalConfirmations) {
            this.channelRemovalConfirmations.add(confirmation);
        }
    }

    /**
     * Edits the name of the specified channel.
     *
     * @param channelName The name of the channel to be edited.
     */
    public void editChannel(String channelName) {

        Channel channel = this.getChannel(channelName);

        assert channel != null : "The channel " + channelName + " doesn't exist.";

        final FormBox.FormEntry NEW_CHANNEL_ENTRY = new FormBox.FormEntry("New Channel Name:",
                this.channelNameValidatorSupplier.apply(channelName), channelName);

        Map<FormBox.FormEntry, Object> response = FormBox.createForm(List.of(NEW_CHANNEL_ENTRY), channel);

        if(response == null) return;

        channel.setChannelName((String) response.get(NEW_CHANNEL_ENTRY));

        this.channels.remove(channelName);
        this.channels.put(channel.getChannelName(), channel);

        SwingUtilities.invokeLater(() -> {

            this.refreshChannels();

            for(ChannelModificationListener listener : this.channelModificationListeners) {
                Executors.newSingleThreadExecutor().submit((() -> listener.channelEdited(channel)));
            }
        });
    }

    /**
     * Returns the Channel with the specified name or null if one doesn't exist.
     *
     * @param channelName Name of the Channel
     * @return The Channel object or null.
     */
    public Channel getChannel(String channelName) {return this.channels.getOrDefault(channelName, null);}

    /**
     * @return A list of all currently open channels as a {@link List<Channel>}.
     */
    public List<Channel> getChannels() {
        return this.channels.values().stream().sorted((channel1, channel2) -> {

            String channel1Name = channel1.getChannelName();
            String channel2Name = channel2.getChannelName();

            int compareValue = Integer.compare(channel1Name.length(), channel2Name.length());
            if (compareValue != 0) {
                return compareValue;
            } else {
                return String.CASE_INSENSITIVE_ORDER.compare(channel1Name, channel2Name);
            }
        }).collect(Collectors.toList());
    }

    /**
     * Creates a new Pop-up where the user can specify the parameters
     * of a new channel.
     */
    public void openChannelOptions() {{

        ChannelOptionsFrame.ChannelInfo newChannelInfo = ChannelOptionsFrame.open(this.channelNameValidatorSupplier.apply(null));

        if(newChannelInfo == null) return;

        this.addChannel(newChannelInfo.channelIndex(), newChannelInfo.channelName());
    }}

    /**
     * Enables/Disables all buttons that modify/add channels.
     *
     * @param isEnabled If true all buttons will be enabled, otherwise they will be disabled.
     */
    public void setEnabled(boolean isEnabled) {

        super.setEnabled(isEnabled);

        this.newChannelButton.setEnabled(isEnabled);
        this.channels.values().forEach(channel -> channel.setEnabled(isEnabled));
    }


    /** === Private Methods -------------------------------------------------- **/

    /**
     * Creates a Channel Control pane for a Channel Panel.
     *
     * @return The Channel Control pane as a JScrollPane
     */
    private JScrollPane createChannelControls() {

        this.channelControlPanel = new JPanel();
        this.channelControlPanel.setLayout(new BoxLayout(this.channelControlPanel, BoxLayout.Y_AXIS));

        JScrollPane channelControlPane = new JScrollPane(this.channelControlPanel, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        channelControlPane.setAlignmentY(Container.TOP_ALIGNMENT);

        for(MouseWheelListener wheelListener: channelControlPane.getMouseWheelListeners()) {
            channelControlPane.removeMouseWheelListener(wheelListener);
        }

        channelControlPane.addMouseWheelListener(this.scrollingWheelListener);
        channelControlPane.getVerticalScrollBar().addAdjustmentListener(e -> this.getChannels().forEach(Channel::hideTriggerSelectionWarning));

        return channelControlPane;
    }

    /**
     * Creates a Channel Display pane meant to Hold the plots of the samples.
     *
     * @return The Channel Display pane as a JScrollPane.
     */
    private JScrollPane createChannelDisplay() {

        this.channelDisplayPanel = new JPanel();
        this.channelDisplayPanel.setLayout(new BoxLayout(this.channelDisplayPanel, BoxLayout.Y_AXIS));

        JScrollPane channelDisplayPane = new JScrollPane(this.channelDisplayPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        channelDisplayPane.setAlignmentY(Container.TOP_ALIGNMENT);

        for(MouseWheelListener wheelListener: channelDisplayPane.getMouseWheelListeners()) {
            channelDisplayPane.removeMouseWheelListener(wheelListener);
        }

        channelDisplayPane.addMouseWheelListener(this.zoomingWheelListener);
        channelDisplayPane.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                ChannelPanel.this.resizeChannels(ChannelPanel.this.displayPanelWidth);
            }
        });

        return channelDisplayPane;
    }

    /**
     * Creates a {@link JPanel} from the specified channel display pane and adds a {@link JToolBar} along the right
     * side.
     *
     * @param displayPane The original display pane as a {@link JScrollPane} object.
     * @return The extended panel as a {@link JPanel} object.
     */
    private JPanel createExtendedDisplayPanel(JScrollPane displayPane) {

        JPanel extendedPanel = new JPanel(new BorderLayout());

        this.toolBar = new JToolBar(SwingConstants.VERTICAL);
        this.toolBar.setPreferredSize(new Dimension(CHANNEL_TOOL_BAR_WIDTH, this.toolBar.getPreferredSize().height));
        this.toolBar.setFloatable(false);

        this.resetZoomButton = new Button(RESET_ZOOM_BUTTON_ICONS);
        this.resetZoomButton.addActionListener(actionEvent -> this.resizeChannels(0));
        this.resetZoomButton.addActionListener(actionEvent -> this.getChannels().forEach(Channel::resetZoom));

        JPanel sliderPanel = new JPanel();
        sliderPanel.setLayout(new BoxLayout(sliderPanel, BoxLayout.X_AXIS));

        this.zoomSlider = new JSlider(JSlider.VERTICAL, 0, ZOOM_SLIDER_TICKS, 0);
        this.zoomSlider.addChangeListener(this.zoomSliderListener);

        sliderPanel.add(Box.createHorizontalGlue());
        sliderPanel.add(this.zoomSlider);
        sliderPanel.add(Box.createHorizontalGlue());

        this.toolBar.add(Styler.style(resetZoomButton, Styler.Style.CHANNEL_TOOLBAR));
        this.toolBar.addSeparator();
        this.toolBar.add(Styler.style(sliderPanel, Styler.Style.BORDERLESS));

        extendedPanel.add(displayPane, BorderLayout.CENTER);
        extendedPanel.add(Styler.style(this.toolBar, Styler.Style.CHANNEL_PANEL), BorderLayout.LINE_END);

        this.addChannelModificationListener(new ChannelModificationListener() {
            @Override
            public void channelAdded(Channel channel) {ChannelPanel.this.setZoomable();}

            @Override
            public void channelRemoved(Channel channel) {ChannelPanel.this.setZoomable();}
        });

        return extendedPanel;
    }

    /**
     * Links the vertical scroll bars of the specified JScrollPane objects.
     *
     * @param pane1 The first scroll bar.
     * @param pane2 The second scroll bar.
     */
    private static void linkVerticalScrollbars(JScrollPane pane1, JScrollPane pane2) {

        pane1.getVerticalScrollBar().addAdjustmentListener(adjustmentEvent -> {
            pane2.getVerticalScrollBar().setValue(pane1.getVerticalScrollBar().getValue());
        });

        pane2.getVerticalScrollBar().addAdjustmentListener(adjustmentEvent -> {
            pane1.getVerticalScrollBar().setValue(pane2.getVerticalScrollBar().getValue());
        });
    }

    /**
     * Creates a new channel button which will later be added to the channel control panel.
     */
    private void createNewChannelButton() {

        // Create the Control Panel component.
        this.newChannelControlComponent = new JPanel(new BorderLayout());
        this.newChannelControlComponent.setAlignmentX(LEFT_ALIGNMENT);

        this.newChannelButton = new Button(NEW_CHANNEL_BUTTON_ICONS);

        this.newChannelButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.newChannelButton.setAlignmentY(Component.CENTER_ALIGNMENT);

        this.newChannelButton.addActionListener(actionEvent -> Executors.newSingleThreadExecutor().submit((this::openChannelOptions)));

        this.newChannelControlComponent.add(this.newChannelButton, BorderLayout.CENTER);

        // Create the Display Panel component.
        this.newChannelDisplayComponent = new JPanel();
        this.newChannelDisplayComponent.setBorder(new TitledBorder("New Channel"));
        this.newChannelDisplayComponent.setAlignmentX(Component.LEFT_ALIGNMENT);

        Styler.style(this.newChannelControlComponent, Styler.Style.CHANNEL_PANEL);
        Styler.setSize(this.newChannelControlComponent, CHANNEL_CONTROL_PANEL_WIDTH, OPEN_CHANNEL_HEIGHT);
    }

    /**
     * Adds a button to the channel control panel, which can be used to add a new channel.
     */
    private void addNewChannelButton() {

        if(this.newChannelControlComponent == null) this.createNewChannelButton();

        this.channelControlPanel.add(this.newChannelControlComponent);
        this.channelDisplayPanel.add(this.newChannelDisplayComponent);
    }

    /**
     * Removes and replaces all the channels displayed on this channel panel to properly show any updates made to it.
     */
    private void refreshChannels() {

        synchronized (this) {

            this.getChannels().forEach(Channel::hideTriggerSelectionWarning);

            this.channelControlPanel.removeAll();
            this.channelDisplayPanel.removeAll();

            for(int i = 0; i < this.orderedChannels.size(); i++) {

                Channel channel = this.orderedChannels.get(i);

                this.channelControlPanel.add(channel.getControlPanel());
                this.channelDisplayPanel.add(channel.getDisplayPanel());

                channel.setDisplayWidth(this.displayPanelWidth);

                channel.getControlPanel().setAlignmentX(Component.LEFT_ALIGNMENT);
                channel.getDisplayPanel().setAlignmentX(Component.LEFT_ALIGNMENT);

                boolean isFirst = (i == 0);
                boolean isLast = (i == this.orderedChannels.size() - 1);

                channel.setReorderable(isFirst, isLast);
            }

            if (Channel.getUnusedChannelIndices().size() != 0) addNewChannelButton();

            SwingUtilities.invokeLater(() -> {

                this.channelControlPanel.repaint();
                this.channelControlPanel.revalidate();

                this.channelDisplayPanel.repaint();
                this.channelDisplayPanel.revalidate();
            });
        }
    }

    /**
     * Enables/Disables zooming based on the number of channels currently present on the panel.
     */
    private void setZoomable() {

        this.isZoomable.set(this.getChannels().size() > 0);
        this.resetZoomButton.setEnabled(this.isZoomable.get());

        if(!this.isZoomable.get()) this.resizeChannels(0);

        this.zoomSlider.setEnabled(this.isZoomable.get());
    }

    /**
     * @return The currently available width for the display part of the channels.
     */
    private int getAvailableDisplayWidth() {

        int availableWidth = this.rightComponent.getWidth() - this.getDividerSize() -
                Math.max(this.toolBar.getWidth(), CHANNEL_TOOL_BAR_WIDTH);

        if(this.displayPane.getVerticalScrollBar().isVisible()) {
            availableWidth -= this.displayPane.getVerticalScrollBar().getWidth();
        }

        return availableWidth;
    }

    /**
     * Resizes the display part of all channels to the specified width if the new size fits within the resizing bounds.
     *
     * @param newWidth The new width for the display part of the channels.
     */
    private void resizeChannels(int newWidth) {

        if(!this.resizeLock.tryLock()) {
            return;
        }

        try {
            int availableWidth = this.getAvailableDisplayWidth();

            JScrollBar horizontalScrollBar = this.displayPane.getHorizontalScrollBar();
            double scrollValuePart = (double) horizontalScrollBar.getValue() / this.displayPanelWidth;

            if (this.isZoomable.get()) {
                this.displayPanelWidth = Math.max(availableWidth, Math.min(MAX_DISPLAY_PANEL_WIDTH, newWidth));
            } else {
                this.displayPanelWidth = availableWidth;
            }

            this.displayPane.getHorizontalScrollBar().setValue((int) Math.round(scrollValuePart * this.displayPanelWidth));

            synchronized (this) {
                Styler.setSize(this.newChannelDisplayComponent, this.displayPanelWidth, OPEN_CHANNEL_HEIGHT);
                for (Channel channel : this.channels.values()) channel.setDisplayWidth(this.displayPanelWidth);
            }

            this.adjustZoomSlider((this.displayPanelWidth - availableWidth) * ZOOM_SLIDER_TICKS / MAX_DISPLAY_PANEL_WIDTH);

        } finally {
            this.resizeLock.unlock();
        }
    }

    /**
     * Adjusts the zoom slider value to the specified amount.
     *
     * @param sliderValue The new value for the slider.
     */
    private void adjustZoomSlider(int sliderValue) {

        this.zoomSlider.removeChangeListener(this.zoomSliderListener);
        this.zoomSlider.setValue(sliderValue);
        this.zoomSlider.addChangeListener(this.zoomSliderListener);
    }

    /**
     * Invoked whenever a channel is selected/deselected.
     *
     * @param channel The channel which was selected/deselected as a {@link Channel} object.
     */
    private void channelSelectionChanged(Channel channel) {

        for(ChannelSelectionListener selectionListener: this.channelSelectionListeners) {

            if(channel.isSelected()) selectionListener.channelSelected(channel);
            else selectionListener.channelDeselected(channel);
        }

        this.checkMultipleEdgeConditions();
    }

    /**
     * Checks all selected channels currently present on this {@link ChannelPanel} for multiple simultaneous edge
     * conditions and displays a warning to the user if more than one is present.
     */
    private synchronized void checkMultipleEdgeConditions() {

        List<Channel> edgeConditionChannels = this.getChannels().stream().filter(Channel::isSelected)
                .filter(channel -> channel.getTrigger().isEdgeCondition()).collect(Collectors.toList());

        this.getChannels().forEach(Channel::hideTriggerSelectionWarning);

        if(edgeConditionChannels.size() > 1) {
            edgeConditionChannels.forEach(channel -> channel.displayTriggerSelectionWarning("Simultaneous Edge Conditions have been selected."));
        }
    }


    /** === Instance Variables ----------------------------------------------- **/

    private JPanel channelControlPanel;
    private JPanel channelDisplayPanel;

    private final JScrollPane controlPane;
    private final JScrollPane displayPane;
    private final List<Channel> orderedChannels;
    private final Map<String, Channel> channels;

    private Button newChannelButton;
    private Button resetZoomButton;
    private JPanel newChannelDisplayComponent;
    private JPanel newChannelControlComponent;

    private JToolBar toolBar;
    private JSlider zoomSlider;

    private AtomicBoolean isZoomable;

    private int displayPanelWidth;

    private final List<ChannelModificationListener> channelModificationListeners;
    private final List<ChannelSelectionListener> channelSelectionListeners;

    private final List<Function<Channel, Boolean>> channelRemovalConfirmations;

    private final Lock resizeLock;

    /**
     * Methods for this listener are invoked whenever channel reordering is requested from the user.
     */
    private final ChannelReorderingListener channelReorderingListener = new ChannelReorderingListener() {
        @Override
        public void channelReorderedUp(Channel channel) {

            synchronized (ChannelPanel.this.orderedChannels) {
                int index = ChannelPanel.this.orderedChannels.indexOf(channel);

                if (index > 0) {
                    ChannelPanel.this.orderedChannels.remove(channel);
                    ChannelPanel.this.orderedChannels.add(index - 1, channel);
                }
            }

            ChannelPanel.this.refreshChannels();
        }

        @Override
        public void channelReorderedDown(Channel channel) {

            synchronized (ChannelPanel.this.orderedChannels) {
                int index = ChannelPanel.this.orderedChannels.indexOf(channel);

                if (index != -1 && index != ChannelPanel.this.orderedChannels.size() - 1) {
                    ChannelPanel.this.orderedChannels.remove(channel);
                    ChannelPanel.this.orderedChannels.add(index + 1, channel);
                }
            }

            ChannelPanel.this.refreshChannels();
        }
    };

    /**
     * The {@link DataValidator} for validation the user specified channel name during channel creation/modification.
     */
    private final Function<String, DataValidator> channelNameValidatorSupplier = currentName -> newName -> {

        newName = newName.trim();

        if(newName.equals("")) return "A channel name must be specified.";
        else if(!newName.equals(currentName) && this.getChannel(newName) != null) {
            return "A channel with the name " + newName + " already exists.";
        }
        else return null;
    };

    /**
     * Listener for scrolling via the scrolling wheel when the mouse is hovering over the control panel.
     */
    private final MouseWheelListener scrollingWheelListener = mouseWheelEvent -> SwingUtilities.invokeLater(() ->{

        JScrollBar scrollBar = ((JScrollPane) mouseWheelEvent.getSource()).getVerticalScrollBar();
        scrollBar.setValue((int) (scrollBar.getValue() + SCROLL_WEIGHT * mouseWheelEvent.getPreciseWheelRotation()));
    });

    /**
     * Listener for zooming via the scrolling wheel.
     */
    private final MouseWheelListener zoomingWheelListener = mouseWheelEvent -> SwingUtilities.invokeLater(() -> {

        if(!this.isZoomable.get()) return;

        int currWidth = this.displayPanelWidth;
        currWidth -= ZOOM_PER_TICK * mouseWheelEvent.getPreciseWheelRotation();

        this.resizeChannels(currWidth);
    });

    /**
     * Listener for the zooming slider.
     */
    private final ChangeListener zoomSliderListener = changeEvent -> {

        int newWidth = this.zoomSlider.getValue() * MAX_DISPLAY_PANEL_WIDTH / ZOOM_SLIDER_TICKS
                + this.getAvailableDisplayWidth();

        this.resizeChannels(newWidth);
    };

    private final ZoomListener zoomedFrameListener = (startSampleIndex, endSampleIndex) -> {

        if(!this.isEnabled() || Math.abs(endSampleIndex - startSampleIndex) < MIN_N_SAMPLES_TO_ZOOM) {
            return;
        }

        logger.info("Zoom requested for the samples " + startSampleIndex + ", " + endSampleIndex);

        this.getChannels().stream().filter(channel -> !channel.isCleared())
                .forEach(channel -> channel.zoom(startSampleIndex, endSampleIndex));
    };
}
