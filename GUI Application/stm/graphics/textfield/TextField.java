package textfield;

import data.DataValidator;
import popups.PopupTextPanel;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TextField extends JTextField {

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a text field with the specified number of columns.
     *
     * @param nColumns The number of initially visible character columns.
     */
    public TextField(int nColumns) {this(nColumns, data -> null);}

    /**
     * Creates a text field with the specified data validator.
     *
     * @param dataValidator An object implementing the {@link DataValidator} interface.
     */
    public TextField(DataValidator dataValidator) {this(0, dataValidator);}

    /**
     * Creates a text field with the specified column count and data validator.
     *
     * @param dataValidator An object implementing the {@link DataValidator} interface.
     * @param nColumns The number of initially visible character columns.
     */
    public TextField(int nColumns, DataValidator dataValidator) {

        super(nColumns);

        this.dataValidator = dataValidator;

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {PopupTextPanel.removePopup(TextField.this);}
        });
        this.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {PopupTextPanel.removePopup(TextField.this);}
        });
        this.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {PopupTextPanel.removePopup(TextField.this);}

            @Override
            public void removeUpdate(DocumentEvent e) {PopupTextPanel.removePopup(TextField.this);}

            @Override
            public void changedUpdate(DocumentEvent e) {PopupTextPanel.removePopup(TextField.this);}
        });
    }

    /**
     * Validates the currently specified data in the text field and displays an error message
     * if the data is invalid.
     *
     * @return True if the currently present data in the text field is valid.
     */
    public boolean validateData() {
        return this.validateData(true);
    }

    /**
     * Validates the currently specified data in the text field.
     *
     * @param displayError If true the error message will be displayed in the content area of the tooltip.
     * @return True if the currently present data in the text field is valid.
     */
    public boolean validateData(boolean displayError) {

        String validationResult = this.dataValidator.on(this.getText());

        if(validationResult == null) return true;
        else if(displayError) displayError(validationResult);

        return false;
    }

    /**
     * Displays an error message within the text field temporarily and then clears the text field contents.
     *
     * @param errorMessage The error message to be displayed.
     */
    public void displayError(String errorMessage) {
        PopupTextPanel.displayPopup(this, errorMessage, PopupTextPanel.PopupType.ERROR, PopupTextPanel.PopupLocation.BELOW, true);
    }

    /** === Instance Variables ----------------------------------------------- **/

    private final DataValidator dataValidator;
}
