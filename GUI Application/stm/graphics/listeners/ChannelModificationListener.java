package listeners;

import channel.Channel;

public interface ChannelModificationListener {

    /**
     * Invoked whenever a channel is added to a channel panel.
     *
     * @param channel The channel that was added as a {@link Channel} object.
     */
    default void channelAdded(Channel channel) {}

    /**
     * Invoked whenever a channel is removed from a channel panel.
     *
     * @param channel The channel that was removed as a {@link Channel} object.
     */
    default void channelRemoved(Channel channel) {}

    /**
     * Invoked whenever a channel present on a channel panel was modified.
     *
     * @param channel The modified channel as a {@link Channel} object.
     */
    default void channelEdited(Channel channel) {}
}
