package listeners;

import channel.Channel;

public interface ChannelReorderingListener {

    /**
     * Invoked whenever the user presses the "reorder up" button on a channel.
     *
     * @param channel The channel that the user wishes to move up, as a {@link Channel} object.
     */
    default void channelReorderedUp(Channel channel) {}

    /**
     * Invoked whenever the user presses the "reorder down" button on a channel.
     *
     * @param channel The channel that the user wishes to move down, as a {@link Channel} object.
     */
    default void channelReorderedDown(Channel channel) {}
}
