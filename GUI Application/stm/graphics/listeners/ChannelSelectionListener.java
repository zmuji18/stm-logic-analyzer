package listeners;

import channel.Channel;

public interface ChannelSelectionListener {

    /**
     * Invoked whenever a channel is selected.
     *
     * @param channel The channel which was selected.
     */
    default void channelSelected(Channel channel) {}

    /**
     * Invoked whenever a channel is deselected.
     *
     * @param channel The channel which was deselected.
     */
    default void channelDeselected(Channel channel) {}
}
