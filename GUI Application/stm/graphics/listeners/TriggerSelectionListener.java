package listeners;

import configuration.ChannelConfiguration;

public interface TriggerSelectionListener {

    /**
     * Invoked whenever a trigger button is pressed on a channel.
     *
     * @param newTrigger The new trigger that was set as a result of the button press.
     */
    void triggerSelected(ChannelConfiguration.Trigger newTrigger);
}
