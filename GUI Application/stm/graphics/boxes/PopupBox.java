package boxes;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public abstract class PopupBox extends JFrame {

    /** === Interface Functions ---------------------------------------------- **/

    /**
     *  Creates a blank pop-up box with the specified title, layout direction and margins.
     *
     * @param title The title of the pop-up.
     * @param axis The axis of the {@link BoxLayout} this box uses.
     * @param insets The insets to be used at the edges for this pop-up.
     */
    public PopupBox(String title, int axis, Insets insets) {

        super(title);

        this.mainPanel = new Box(axis);

        this.setLayout();
        this.setMargins(insets);

        this.setResizable(false);

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }


    /**
     * Adds a component to the main settings panel.
     *
     * @param component The component to be added as a {@link Component} object.
     * @return The component that was added.
     */
    @Override
    public Component add(Component component) {return this.mainPanel.add(component);}


    /** === Private Methods -------------------------------------------------- **/

    /**
     * Sets the layout of this {@link PopupBox}.
     */
    private void setLayout() {

        super.setLayout(new BorderLayout());
        super.add(this.mainPanel, BorderLayout.CENTER);
    }

    /**
     * Sets the margins for this {@link PopupBox}.
     *
     * @param insets The margins as an {@link Insets} object.
     */
    private void setMargins(Insets insets) {
        this.mainPanel.setBorder(new EmptyBorder(insets));
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final Box mainPanel;
}
