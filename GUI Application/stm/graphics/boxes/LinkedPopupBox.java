package boxes;

import styling.Styler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class LinkedPopupBox extends PopupBox {


    public static final Object NO_RESPONSE = new Object();

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a pop-up for the user and waits for their response. The current thread will be blocked until
     * the user responds or closes the window.
     *
     * @param link The object this pop-up is linked to. If this method is called with the same link before the previous
     *             window is closed, a new one will not be created and the previous pop-up will be brought to the
     *             front of the screen.
     *
     * @param type The type of the pop-up window.
     * @param arguments The arguments for the class of {@link LinkedPopupBox} that will be created.
     *
     * @return The user's response or an empty response if this method is called a second time before the previous window is closed.
     */
    protected static Object open(Object link, Class<? extends LinkedPopupBox> type,  Object... arguments) {

        openLock.lock();

        if(openedBoxes.containsKey(link)) {

            Styler.centerPopUp(openedBoxes.get(link));
            openedBoxes.get(link).toFront();

            openLock.unlock();

            return false;
        }
        else {

            LinkedPopupBox box = createBoxByType(type, arguments);

            box.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosed(WindowEvent e) {box.setNoResponse();}
            });

            Styler.centerPopUp(box);
            box.toFront();

            openedBoxes.put(link, box);
            openLock.unlock();

            Object response = box.getResponse();

            openLock.lock();
            box.dispose();
            openedBoxes.remove(link);
            openLock.unlock();

            return response;
        }
    }

    /**
     * Sets the response object that will be returned to any thread waiting for this box to close. Calling this function
     * will immediately close this pop-up.
     *
     * @param response The response object.
     */
    public final void setResponse(Object response) {

        this.response = response;
        this.responseLatch.countDown();
    }

    /** === Private Methods -------------------------------------------------- **/

    /**
     * Creates a blank pop-up box with the specified title, layout direction and margins.
     *
     * @param title  The title of the pop-up.
     * @param axis   The axis of the {@link BoxLayout} this box uses.
     * @param insets The insets to be used at the edges for this pop-up.
     */
    protected LinkedPopupBox(String title, int axis, Insets insets) {

        super(title, axis, insets);

        this.responseLatch = new CountDownLatch(1);
    }

    /**
     * Blocks the current thread until a user has submitted a response.
     *
     * @return The user's response.
     */
    private Object getResponse() {

        try {
            this.responseLatch.await();
        } catch (InterruptedException ignored) {}

        return this.response;
    }

    /**
     * Sets a singleton object as a response representing an event where the user chose to close the window instead of
     * submitting a response.
     */
    private void setNoResponse() {
        this.setResponse(NO_RESPONSE);
    }

    /**
     * Creates a {@link LinkedPopupBox} object based on the specified type and arguments.
     *
     * @param type The type of the {@link LinkedPopupBox} that should be created.
     * @param arguments The arguments for the constructor of the specified type.
     *
     * @return A {@link LinkedPopupBox} of the specified type.
     */
    private static LinkedPopupBox createBoxByType(Class<? extends LinkedPopupBox> type, Object... arguments) {

        if (ConfirmationBox.class.equals(type)) {

            if(arguments.length != 1 || !(arguments[0] instanceof String)) {
                throw new RuntimeException("The expected arguments for a ConfirmationBox are (String,).");
            }

            return new ConfirmationBox((String) arguments[0]);
        } else if (FormBox.class.equals(type)) {

            if(arguments.length != 1 || !(arguments[0] instanceof Collection)) {
                throw new RuntimeException("The expected arguments for a FormBox are (Collection,).");
            }

            return new FormBox((Collection<FormBox.FormEntry>) arguments[0]);

        } else {
            throw new RuntimeException("The specified type must be a sub-class of a LinkedPopupBox.");
        }
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final CountDownLatch responseLatch;
    private Object response;

    /** === Static Variables ------------------------------------------------- **/

    private static final Lock openLock;
    private static final Map<Object, LinkedPopupBox> openedBoxes;

    static {
        openLock = new ReentrantLock();
        openedBoxes = new HashMap<>();
    }
}
