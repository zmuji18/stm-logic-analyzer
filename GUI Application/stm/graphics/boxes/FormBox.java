package boxes;

import data.DataValidator;
import styling.Styler;
import textfield.TextField;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static styling.GUIConstants.*;

public class FormBox extends LinkedPopupBox {

    public static record FormEntry(String fieldName, DataValidator dataValidator, Object defaultValue) {

        public FormEntry {
            Objects.requireNonNull(fieldName, "The field name must be specified.");
            Objects.requireNonNull(dataValidator, "The data validator must be specified");
        }
    }

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a {@link FormBox} from the specified fields.
     *
     * @param fields The fields that the form box should containt as a {@link Map} object.
     * @param link The object the new form box is linked to.
     *
     * @return The user's response as a map.
     */
    public static Map<FormEntry, Object> createForm(Collection<FormEntry> fields, Object link) {

        Object response = open(link, FormBox.class, fields);

        if(response == NO_RESPONSE) {
            return null;
        } else {
            return (Map<FormEntry, Object>) response;
        }
    }


    /** === Private Methods -------------------------------------------------- **/

    /**
     * Creates a form box from the specified fields.
     *
     * @param fields The fields as a {@link Map} containing mappings of field names to their respective
     *               {@link DataValidator} objects.
     */
     protected FormBox(Collection<FormEntry> fields) {

        super("Form", BoxLayout.Y_AXIS, FORM_BOX_MARGINS);

        this.textFields = new HashMap<>();

        this.addFields(fields);
        this.add(Box.createRigidArea(new Dimension(GENERIC_COMPONENT_GAP, GENERIC_COMPONENT_GAP)));
        this.add(Box.createVerticalGlue());
        this.addControlButtons();

        this.setResizable(false);

        this.setMinimumSize(new Dimension(FORM_POPUP_WIDTH, FORM_POPUP_HEIGHT));

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        this.pack();
        this.setVisible(true);

        Styler.centerPopUp(this);
     }

    /**
     * Adds the specified fields to this {@link FormBox} object.
     *
     * @param fields The fields to be added to the form box as a {@link Collection} of {@link FormEntry}s.
     */
    private void addFields(Collection<FormEntry> fields) {

        JPanel formPanel = new JPanel();
        formPanel.setLayout(new BoxLayout(formPanel, BoxLayout.Y_AXIS));

        for(FormEntry entry: fields) {

            JPanel fieldPanel = new JPanel();
            fieldPanel.setLayout(new BoxLayout(fieldPanel, BoxLayout.X_AXIS));

            TextField textField = new TextField(GENERIC_TEXT_FIELD_COLUMNS, entry.dataValidator);
            textField.setText(entry.defaultValue == null ? "" : entry.defaultValue.toString());
            textField.setCaretPosition(textField.getText().length());
            this.textFields.put(entry, textField);

            fieldPanel.add(Styler.style(new JLabel(entry.fieldName), Styler.Style.POP_UP));
            fieldPanel.add(Box.createRigidArea(new Dimension(GENERIC_COMPONENT_GAP, GENERIC_COMPONENT_GAP)));
            fieldPanel.add(Styler.style(textField, Styler.Style.FORM_POP_UP));
            fieldPanel.add(Box.createHorizontalGlue());

            textField.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    if(e.getKeyCode() == KeyEvent.VK_ENTER) FormBox.this.collectResponse();
                    else if(e.getKeyCode() == KeyEvent.VK_ESCAPE) FormBox.this.dispose();
                }
            });

            formPanel.add(fieldPanel);
        }

        this.add(formPanel);
    }

    /**
     * Adds control buttons for the user to this {@link FormBox}.
     */
    private void addControlButtons() {

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

        JButton applyButton = new JButton("Apply");
        Styler.style(applyButton);

        JButton cancelButton = new JButton("Cancel");
        Styler.style(cancelButton);

        applyButton.addActionListener(actionEvent -> this.collectResponse());
        cancelButton.addActionListener(actionEvent -> this.dispose());

        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(applyButton);
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(cancelButton);
        buttonPanel.add(Box.createHorizontalGlue());

        this.add(buttonPanel);
    }

    /**
     * Validates every field in this {@link FormBox} according to the user specified {@link DataValidator} objects.
     *
     * @return True if all fields contain valid data, false otherwise.
     */
    private boolean validateResponse() {

        for(TextField textField: this.textFields.values()) {
            if(!textField.validateData()) return false;
        }

        return true;
    }

    /**
     * Validates every field and returns the user response to the thread waiting for it.
     */
    private void collectResponse() {

        if(!this.validateResponse()) return;

        super.setResponse(this.textFields.entrySet().stream().map(entry -> Map.entry(entry.getKey(), entry.getValue().getText()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final Map<FormEntry, TextField> textFields;
}
