package terminal;

import styling.Styler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;

/**
 * Class: TerminalPanel
 * Date Created: 21.08.21 19:04
 *
 * === Description ============================================= *
 * TODO Fill in description.
 */
public class TerminalPanel extends JPanel{


    /** === Constant Definitions --------------------------------------------- **/
    // TODO Fill this in


    /** === Interface Functions ---------------------------------------------- **/

    public TerminalPanel(){

        super();

        this.setLayout(new BorderLayout());

        this.add(getHeader(), BorderLayout.PAGE_START);
        this.add(getTerminalOutput(), BorderLayout.CENTER);
        this.add(getTerminalInput(), BorderLayout.PAGE_END);
    }


    /** === Static Interface Functions --------------------------------------- **/
    // TODO Fill ths in


    /** === Main ------------------------------------------------------------- **/

    public static void main(String [] args){

        try{
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch(Exception ignored) {}

        TerminalPanel terminal = new TerminalPanel();
        JFrame terminalFrame = new JFrame();
        terminalFrame.add(terminal);

        terminalFrame.pack();
        terminalFrame.setVisible(true);
    }


    /** === Private Functions ------------------------------------------------ **/

    private JPanel getHeader() {

        JPanel headerPanel = new JPanel();
        headerPanel.setAlignmentX(LEFT_ALIGNMENT);
        headerPanel.setLayout(new BorderLayout());

        Styler.style(headerPanel);

        JLabel terminalLabel = new JLabel("Terminal ");
        terminalLabel.setAlignmentX(LEFT_ALIGNMENT);

        headerPanel.add(terminalLabel, BorderLayout.LINE_START);
        Styler.style(terminalLabel);

        return headerPanel;
    }

    private JPanel getTerminalInput(){

        JPanel terminalInputPanel = new JPanel();
        terminalInputPanel.setAlignmentX(LEFT_ALIGNMENT);
        terminalInputPanel.setLayout(new BorderLayout());

        Styler.style(terminalInputPanel);

        terminalInputPanel.add(getInputTextFields(), BorderLayout.CENTER);

        return terminalInputPanel;
    }

    private JScrollPane getTerminalOutput(){

        JPanel terminalOutputPanel = new JPanel();
        terminalOutputPanel.setAlignmentX(LEFT_ALIGNMENT);
        terminalOutputPanel.setLayout(new BorderLayout());

        Styler.style(terminalOutputPanel);

        terminalOutputPanel.add(getOutputTextArea(), BorderLayout.CENTER);

        return new JScrollPane(terminalOutputPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    }

    private JPanel getInputTextFields() {

        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new BorderLayout());

        inputText = new JTextField();

        inputText.addKeyListener(inputListener);

        inputText.setEditable(true);
        inputPanel.add(inputText);

        return inputPanel;
    }

    private JPanel getOutputTextArea() {

        JPanel outputPanel = new JPanel();
        outputPanel.setLayout(new BorderLayout());

        outputText = new JTextArea();

        outputText.setAlignmentX(LEFT_ALIGNMENT);
        outputText.setEditable(false);

        outputPanel.add(outputText);

        return outputPanel;
    }

    private void getTerminalOutput(String command){

        try {

            Process process = Runtime.getRuntime().exec(command);
            process.waitFor();

            int numOfBytes = process.getInputStream().available();
            byte [] inputBytes = new byte [numOfBytes];
            process.getInputStream().read(inputBytes);
            String output = new String(inputBytes);

            printLine(output);

        } catch (IOException | InterruptedException | IllegalArgumentException ex) {

            String message = ex.getMessage();
            printLine(message + "\n");
        }
    }

    private void printLine (String text){

        SwingUtilities.invokeLater(() -> {
            if(outputText.getText().equals("")) {
                outputText.setText(outputText.getText() + text);
            }else {
                outputText.setText(outputText.getText() + "\n" + text);
            }
        });
    }


    /** === Instance Variables ----------------------------------------------- **/

    private JTextField inputText;
    private JTextArea outputText;

    private final KeyAdapter inputListener = new KeyAdapter() {
        @Override
        public void keyPressed(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_ENTER){

                SwingUtilities.invokeLater(() -> {

                    String command = inputText.getText();
                    printLine(command);

                    inputText.setText("");

                    new Thread(() -> getTerminalOutput(command)).start();

                });
            }
        }
    };
    
    
    /** === Static Variables ------------------------------------------------- **/
    // TODO Fill this in
}