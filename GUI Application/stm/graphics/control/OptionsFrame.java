package control;

import buttons.Button;
import com.fazecast.jSerialComm.SerialPort;
import communication.STMController;
import configuration.PortConfiguration;
import styling.Styler;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import static data.DataConstants.STM32_PORT_NAME;
import static styling.GUIConstants.REFRESH_BUTTON_ICONS;

public class OptionsFrame extends JFrame {

    /** === Constant Definitions --------------------------------------------- **/

    public static final int POP_UP_WIDTH = 500;
    public static final int POP_UP_HEIGHT = 600;

    public static final int SERIAL_PORT_BOX_WIDTH = 200;
    public static final int COMBO_BOX_HEIGHT = 50;

    public static final int SERIAL_PORT_SETTINGS_HEIGHT = 300;

    public static final int TOP_MARGIN = 10;
    public static final int LEFT_MARGIN = 15;
    public static final int BOTTOM_MARGIN = 20;
    public static final int RIGHT_MARGIN = 15;
    public static final int COMPONENT_H_GAP = 15;
    public static final int COMPONENT_V_GAP = 10;


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Opens an Options Frame for the specified {@link ControlPanel} object. If one is
     * already opened it will be moved to the center of the screen.
     *
     * @param parentPanel The parent control panel.
     */
    public static void open(ControlPanel parentPanel) {

        openLock.lock();
        if (openedFrame == null) {

            SwingUtilities.invokeLater(() -> {

                openedFrame = new OptionsFrame(parentPanel);
                openLock.unlock();
            });
        } else {

            SwingUtilities.invokeLater(() -> {

                Styler.centerPopUp(openedFrame);
                openedFrame.toFront();
                openLock.unlock();
            });
        }
    }

    /**
     * Adds a component to the main settings panel.
     *
     * @param component The component to be added as a {@link Component} object.
     * @return The component that was added.
     */
    @Override
    public Component add(Component component) {return this.mainPanel.add(component);}


    /** === Private Methods -------------------------------------------------- **/

    /**
     * Closes the options window.
     */
    private void close() {

        openLock.lock();
        openedFrame = null;
        openLock.unlock();
    }

    /**
     * Refreshes the available serial port combo box with the ports of the currently connected and visible devices.
     */
    private void refreshAvailablePorts() {

        Map<String, String> availablePorts = getAvailablePortMap();

        openLock.lock();

        String selectedItem = (String) this.portComboBox.getSelectedItem();

        this.portComboBox.removeAllItems();
        availablePorts.keySet().forEach(this.portComboBox::addItem);

        if(selectedItem == null || !availablePorts.containsKey(selectedItem)) {

            availablePorts.keySet().stream().filter(portName -> portName.contains(STM32_PORT_NAME)).findFirst()
                    .ifPresent(defaultPortName -> this.portComboBox.setSelectedItem(defaultPortName));

        } else this.portComboBox.setSelectedItem(selectedItem);

        this.portComboBox.revalidate();

        openLock.unlock();
    }

    /**
     * Updates the port configuration for the {@link STMController}.
     */
    private void updateControllerPortConfiguration() {

        openLock.lock();

        this.parentPanel.getController().setPortConfiguration(new PortConfiguration(
                getAvailablePortMap().get((String) this.portComboBox.getSelectedItem())
        ));

        openLock.unlock();
    }

    /**
     * Sets the layout for this {@link OptionsFrame}.
     */
    private void setLayout() {

        super.setLayout(new BorderLayout());
        super.add(this.mainPanel, BorderLayout.CENTER);

        this.mainPanel.setLayout(new BoxLayout(this.mainPanel, BoxLayout.Y_AXIS));
    }

    /**
     * Adds padding to the edges of the options frame.
     */
    private void setMargins() {
        this.mainPanel.setBorder(new EmptyBorder(TOP_MARGIN, LEFT_MARGIN, BOTTOM_MARGIN, RIGHT_MARGIN));
    }

    /**
     * Adds GUI components related to serial port settings.
     */
    private void addSerialPortSettings() {

        JPanel serialPortSettingsPanel = new JPanel();

        serialPortSettingsPanel.setLayout(new BoxLayout(serialPortSettingsPanel, BoxLayout.Y_AXIS));
        serialPortSettingsPanel.setBorder(new TitledBorder("Serial Port Settings"));

        Styler.setSize(serialPortSettingsPanel, POP_UP_WIDTH, SERIAL_PORT_SETTINGS_HEIGHT);

        // Serial Port Selector: Label and Combo Box.
        JPanel portSelectorPanel = new JPanel();
        portSelectorPanel.setLayout(new BoxLayout(portSelectorPanel, BoxLayout.X_AXIS));

        Button refreshButton = new Button(REFRESH_BUTTON_ICONS);
        refreshButton.setContentAreaFilled(false);
        refreshButton.addActionListener(actionEvent -> Executors.newSingleThreadExecutor().submit((this::refreshAvailablePorts)));

        this.portComboBox = new JComboBox<>();
        Styler.setSize(this.portComboBox, SERIAL_PORT_BOX_WIDTH, COMBO_BOX_HEIGHT);

        portSelectorPanel.add(Box.createRigidArea(new Dimension(COMPONENT_H_GAP, COMPONENT_H_GAP)));

        portSelectorPanel.add(new JLabel("Serial Port:"));
        portSelectorPanel.add(Box.createHorizontalGlue());
        portSelectorPanel.add(refreshButton);
        portSelectorPanel.add(this.portComboBox);

        portSelectorPanel.add(Box.createRigidArea(new Dimension(COMPONENT_H_GAP, COMPONENT_H_GAP)));

        // Add everything to Serial Port Settings Panel.
        serialPortSettingsPanel.add(portSelectorPanel);

        this.add(serialPortSettingsPanel);
    }

    /**
     * Adds control buttons to the bottom part of the options frame.
     */
    private void addControlButtons() {

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));

        JButton cancelButton = new JButton("Cancel");
        JButton applyButton = new JButton("Apply");

        cancelButton.addActionListener(actionEvent -> this.dispose());

        applyButton.addActionListener(actionEvent -> {

            this.updateControllerPortConfiguration();
            this.dispose();
        });

        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(applyButton);
        buttonPanel.add(Box.createRigidArea(new Dimension(COMPONENT_H_GAP, COMPONENT_H_GAP)));
        buttonPanel.add(cancelButton);
        buttonPanel.add(Box.createRigidArea(new Dimension(COMPONENT_H_GAP, COMPONENT_H_GAP)));

        this.add(Box.createVerticalGlue());
        this.add(buttonPanel);
    }

    /**
     * Creates an {@link OptionsFrame} object linked to the specified {@link ControlPanel}.
     *
     * @param parentPanel The parent {@link ControlPanel} this options frame is linked to.
     */
    private OptionsFrame(ControlPanel parentPanel) {

        super("Settings");

        this.parentPanel = parentPanel;
        this.mainPanel = new JPanel();

        this.setLayout();
        this.setMargins();

        this.addSerialPortSettings();
        this.addControlButtons();

        Styler.setSize(this, POP_UP_WIDTH, POP_UP_HEIGHT);
        this.setResizable(false);

        Styler.centerPopUp(this);

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                OptionsFrame.this.close();
            }
        });

        this.pack();
        this.setVisible(true);

        this.refreshAvailablePorts();
    }

    /**
     * @return A map of available Serial ports to their system names as a {@link Map<String, String>}.
     */
    private static Map<String, String> getAvailablePortMap() {

        return Arrays.stream(SerialPort.getCommPorts())
                .map(serialPort -> {

                    String descriptiveName = serialPort.getDescriptivePortName();
                    String systemName = serialPort.getSystemPortName();

                    return Map.entry(descriptiveName + " - " + systemName, systemName);

                }).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final ControlPanel parentPanel;
    private final JPanel mainPanel;

    private JComboBox<String> portComboBox;


    /** === Static Variables ------------------------------------------------- **/

    private static OptionsFrame openedFrame;
    private static final Lock openLock;

    static {
        openLock = new ReentrantLock();
    }
}
