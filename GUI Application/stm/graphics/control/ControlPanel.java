package control;

import boxes.ConfirmationBox;
import boxes.NotificationBox;
import buttons.Button;
import channel.Channel;
import channel.ChannelPanel;
import communication.STMController;
import configuration.STMConfiguration;
import data.DataValidator;
import data.SampleDataModel;
import decoder.DecoderPanel;
import decoder.DecoderViewPane;
import decoder.DecodingFrame;
import listeners.ChannelModificationListener;
import listeners.ChannelSelectionListener;
import logging.ConfiguredLogger;
import org.jetbrains.annotations.Nullable;
import styling.Styler;
import textfield.TextField;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static data.DataConstants.*;
import static styling.GUIConstants.*;

/**
 * Class: ControlPanel
 * Date Created: 04.07.21
 *
 * === Description --------------------------------------------= *
 * TODO Fill this in.
 */
public class ControlPanel extends JPanel {

    private static final Logger logger = ConfiguredLogger.getLogger("ControlPanel");

    private static final int LOW_PRIORITY = 10;
    private static final int HIGH_PRIORITY = 0;

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a {@link ControlPanel} representing the main control panel of the GUI. The control
     * panel is linked to other GUI components.
     *
     * @param channelPanel The channel panel as a {@link ChannelPanel} object.
     * @param decoderViewPane The decoder pane as a {@link DecoderViewPane} object.
     */
    public ControlPanel(ChannelPanel channelPanel, DecoderViewPane decoderViewPane) {

        super();

        assert channelPanel != null: "The linked channel panel must be non-null.";
        assert decoderViewPane != null: "The linked decoder panel must be non-null.";

        this.dataModel = SampleDataModel.createInstance();
        this.controller = STMController.createInstance(this.dataModel);

        this.channelPanel = channelPanel;
        this.decoderViewPane = decoderViewPane;

        this.refreshTimer = null;
        this.isSampling = new AtomicBoolean(false);

        this.setLayout();

        this.createSelectAllChannelsCheckBox();
        this.createSamplingParameterControls();
        this.createControlButtons();
        this.addDecoderViewCallbacks();

        this.addSamplingTasks();
        this.enableConfiguringMode();

        Styler.style(this, Styler.Style.CONTROL_PANEL);
    }

    /**
     * @return The currently selected sampling rate as a double value in Samples/Second.
     */
    public double getSelectedSamplingRate() {
        return SAMPLING_RATES.get((String) this.samplingRate.getSelectedItem()).doubleValue();
    }

    /**
     * @return The selected sampling duration as a double value.
     */
    public double getSelectedSamplingDuration() {

        assert this.samplingDuration.validateData(): "The sampling duration data is not valid.";
        return Double.parseDouble(this.samplingDuration.getText()) * this.getSelectedSamplingDurationUnit();
    }

    /**
     * @return The user selected unit of measure for the sampling duration of the logic analyzer.
     */
    public double getSelectedSamplingDurationUnit() {
        return SAMPLING_DURATION_UNITS.get((String) this.samplingDurationUnitBox.getSelectedItem()).doubleValue();
    }

    /**
     * @return Returns the selected sampling memory as an integer value.
     */
    public int getSelectedSamplingMemory() {

        assert this.memory.validateData(): "The sampling duration data is not valid.";
        return (int) (Double.parseDouble(this.memory.getText()) * this.getSelectedSamplingMemoryUnit());
    }

    /**
     * @return The user selected unit of measure for the sampling memory of the logic analyzer.
     */
    public int getSelectedSamplingMemoryUnit() {
        return SAMPLING_MEMORY_UNITS.get((String) this.memoryUnitBox.getSelectedItem());
    }

    /**
     * @return The selected sampling level as a single encoded byte.
     */
    public byte getSelectedLogicLevel() {
        return (byte) (int) (LOGIC_LEVELS.get((String) this.logicLevel.getSelectedItem()).doubleValue() / 2 / STM_REFERENCE_VOLTAGE * 0xff);
    }

    /**
     * @return True if valid values are chosen for all sampling parameters.
     */
    public boolean validateParameters() {
        return this.samplingDuration.validateData() && this.memory.validateData();
    }

    /**
     * Disables all components related to sampling configurations and enables the stop button.
     */
    public void enableSamplingMode() {

        List.of(
                this.startButton,
                this.settingsButton,
                this.samplingRate,
                this.samplingDuration,
                this.memory,
                this.logicLevel,
                this.channelPanel,
                this.decoderViewPane
        ).forEach(component -> component.setEnabled(false));

        this.stopButton.setEnabled(true);
    }

    /**
     * Enables all configuration components and disables the stop button.
     */
    public void enableConfiguringMode() {

        List.of(
                this.startButton,
                this.settingsButton,
                this.samplingRate,
                this.samplingDuration,
                this.memory,
                this.logicLevel,
                this.channelPanel,
                this.decoderViewPane
        ).forEach(component -> component.setEnabled(true));

        this.stopButton.setEnabled(false);
    }

    /**
     * @return The snapshot of the {@link SampleDataModel} used by this {@link ControlPanel}, or null if the Data Model
     * is not initialized.
     */
    public @Nullable SampleDataModel.Snapshot getModelSnapshot() {

        assert !this.isSampling.get(): "Model Snapshot requested during sampling.";

        return this.dataModel.isInitialized() ? this.dataModel.getSnapshot() : null;
    }


    /** === Private Methods -------------------------------------------------- **/

    /**
     * @return The {@link STMController} associated with this {@link ControlPanel}.
     */
    final STMController getController() {return this.controller;}

    /**
     * Sends a request to the {@link STMController} instance to startTime sampling,
     *
     * @return True if the request has successfully been sent.
     */
    private boolean startSampling(STMConfiguration stmConfiguration) {

        if(!this.isSampling.compareAndSet(false, true)) return false;

        this.controller.setSTMConfiguration(stmConfiguration);

        boolean success = this.controller.startSampling();
        if(!success) this.isSampling.set(false);

        return success;
    }

    /**
     * Notifies the {@link STMController} to stop sampling and sets the GUI to configuring mode.
     */
    private void stopSampling() {

        assert this.isSampling.get() : "The GUI is not in sampling mode.";

        SwingUtilities.invokeLater(this::enableConfiguringMode);

        this.controller.stopSampling();

        this.stopChannelRefresh();
        this.refreshChannels();

        this.isSampling.set(false);
    }

    /**
     * @return The sampling configuration currently specified by the user as an {@link STMConfiguration} object.
     */
    private STMConfiguration getSTMConfiguration() {

        List<Channel> selectedChannels = this.getSelectedChannels();

        if(!NotificationBox.check(selectedChannels.size() != 0, "No channels have been selected for sampling.")) {

            this.isSampling.set(false);
            return null;
        }

        if(!this.validateParameters()) {

            this.isSampling.set(false);
            return null;
        }

        STMConfiguration stmConfiguration = new STMConfiguration();

        stmConfiguration.setSamplingRate(this.getSelectedSamplingRate());
        stmConfiguration.setSamplingMemory(this.getSelectedSamplingMemory());
        stmConfiguration.setLogicLevel(this.getSelectedLogicLevel());

        selectedChannels.forEach(channel -> {
            stmConfiguration.addChannelConfiguration(channel.getConfiguration());
        });

        return stmConfiguration;
    }

    /**
     * @return A list of all selected channels.
     */
    private List<Channel> getSelectedChannels() {
        return this.channelPanel.getChannels().stream().filter(Channel::isSelected).collect(Collectors.toList());
    }

    /**
     * Sets a box layout for the GUI.
     */
    private void setLayout() {
        super.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
    }

    /**
     * Creates a checkbox at the top left corner, which will select all channel for listening when checked.
     */
    private void createSelectAllChannelsCheckBox() {

        JCheckBox channelCheckBox = new JCheckBox(" Select All Channels");
        channelCheckBox.setEnabled(false);

        channelCheckBox.addActionListener(actionEvent -> {

            boolean isSelected = channelCheckBox.isSelected();

            for(Channel channel: this.channelPanel.getChannels()) channel.setSelected(isSelected);
        });

        this.channelPanel.addChannelModificationListener(new ChannelModificationListener() {
            @Override
            public void channelAdded(Channel channel) {this.updateCheckBox();}

            @Override
            public void channelRemoved(Channel channel) {this.updateCheckBox();}

            /**
             * Enables/Disables the "Select All Channels" Checkbox based on the number of channels currently present
             * on the panel.
             */
            private void updateCheckBox() {

                int nChannels = ControlPanel.this.channelPanel.getChannels().size();

                if(nChannels == 0) {
                    channelCheckBox.setEnabled(false);
                    channelCheckBox.setSelected(false);
                } else {
                    channelCheckBox.setEnabled(true);
                }
            }
        });

        this.channelPanel.addChannelSelectionListener(new ChannelSelectionListener() {
            @Override
            public void channelSelected(Channel channel) {
                updateCheckBox();
                ControlPanel.this.refreshSamplingParameters();
            }

            @Override
            public void channelDeselected(Channel channel) {
                updateCheckBox();
                ControlPanel.this.refreshSamplingParameters();
            }

            private void updateCheckBox() {

                List<Channel> channels = ControlPanel.this.channelPanel.getChannels();

                int nChannels = channels.size();
                int nSelectedChannels = channels.stream().filter(Channel::isSelected).toList().size();

                channelCheckBox.setSelected(nChannels == nSelectedChannels);
            }
        });

        this.add(Box.createRigidArea(new Dimension(SMALL_COMPONENT_GAP, SMALL_COMPONENT_GAP)));
        this.add(Styler.style(channelCheckBox, Styler.Style.CONTROL_PANEL));

        Styler.addSeparator(this, JSeparator.VERTICAL, true);
    }

    /**
     * Adds GUI components for sampling parameter options to the control panel.
     */
    private void createSamplingParameterControls() {

        // Sampling Rate: Add Combo Box
        String[] samplingRates = SAMPLING_RATES.keySet().toArray(new String[0]);
        Arrays.sort(samplingRates, (string1, string2) -> {

            assert string1 != null && string2 != null: "All sampling rate strings must be non-null";
            return Double.compare(SAMPLING_RATES.get(string1).doubleValue(), SAMPLING_RATES.get(string2).doubleValue());
        });

        this.samplingRate = new JComboBox<>(samplingRates);
        this.samplingRate.addActionListener(actionEvent -> SwingUtilities.invokeLater(() -> {
            this.samplingDuration.setText(this.samplingDuration.getText());
        }));

        // Sampling Duration: Add Text Field
        String[] samplingDurationUnits = SAMPLING_DURATION_UNITS.keySet().toArray(new String[0]);
        Arrays.sort(samplingDurationUnits, (string1, string2) -> {

            assert string1 != null && string2 != null: "All sampling duration unit strings must be non-null";
            return -Double.compare(SAMPLING_DURATION_UNITS.get(string1).doubleValue(), SAMPLING_DURATION_UNITS.get(string2).doubleValue());
        });

        this.samplingDuration = new TextField(GENERIC_TEXT_FIELD_COLUMNS, this.samplingDurationValidator);
        this.samplingDurationUnitBox = new JComboBox<>(samplingDurationUnits);
        this.samplingDurationUnitBox.addActionListener(actionEvent -> SwingUtilities.invokeLater(() -> {
            this.samplingDuration.setText(this.samplingDuration.getText());
        }));

        // Sampling Memory: Add Text Field
        String[] samplingMemoryUnits = SAMPLING_MEMORY_UNITS.keySet().toArray(new String[0]);
        Arrays.sort(samplingMemoryUnits, (string1, string2) -> {

            assert string1 != null && string2 != null: "All sampling duration unit strings must be non-null";
            return Double.compare(SAMPLING_MEMORY_UNITS.get(string1).doubleValue(), SAMPLING_MEMORY_UNITS.get(string2).doubleValue());
        });

        this.memory = new TextField(GENERIC_TEXT_FIELD_COLUMNS, this.samplingMemoryValidator);
        this.memoryUnitBox = new JComboBox<>(samplingMemoryUnits);
        this.memoryUnitBox.addActionListener(actionEvent -> SwingUtilities.invokeLater(() -> {
            this.samplingDuration.setText(this.samplingDuration.getText());
        }));

        this.linkSamplingDurationAndMemory();

        // Logic Level: Add Combo Box
        String[] logicLevels = LOGIC_LEVELS.keySet().toArray(new String[0]);
        Arrays.sort(logicLevels);

        this.logicLevel = new JComboBox<>(logicLevels);

        this.channelPanel.addChannelModificationListener(new ChannelModificationListener() {
            @Override
            public void channelAdded(Channel channel) {
                ControlPanel.this.refreshSamplingParameters();
            }

            @Override
            public void channelRemoved(Channel channel) {
                ControlPanel.this.refreshSamplingParameters();
            }
        });


        this.add(Styler.style(new JLabel("Sampling Rate: "), Styler.Style.CONTROL_PANEL));
        this.add(Styler.style(samplingRate, Styler.Style.CONTROL_PANEL));
        Styler.addGap(this, MEDIUM_COMPONENT_GAP);

        this.add(Styler.style(new JLabel("Sampling Duration: "), Styler.Style.CONTROL_PANEL));
        this.add(Styler.style(this.samplingDuration, Styler.Style.CONTROL_PANEL));
        this.add(Styler.style(this.samplingDurationUnitBox, Styler.Style.MEASURE_UNIT));
        Styler.addGap(this, MEDIUM_COMPONENT_GAP);

        this.add(Styler.style(new JLabel("Sampling Memory: "), Styler.Style.CONTROL_PANEL));
        this.add(Styler.style(this.memory, Styler.Style.CONTROL_PANEL));
        this.add(Styler.style(this.memoryUnitBox, Styler.Style.MEASURE_UNIT));
        Styler.addGap(this, MEDIUM_COMPONENT_GAP);

        this.add(Styler.style(new JLabel("Logic Level: "), Styler.Style.CONTROL_PANEL));
        this.add(Styler.style(this.logicLevel, Styler.Style.CONTROL_PANEL));
        Styler.addGap(this, MEDIUM_COMPONENT_GAP);

        this.add(Box.createHorizontalGlue());

        Styler.addSeparator(this, JSeparator.VERTICAL, true);

        Styler.setSize(this.samplingRate, SAMPLING_RATE_FIELD_WIDTH, CONTROL_PANEL_TEXT_FIELD_HEIGHT);
        Styler.setSize(this.samplingDuration, SAMPLING_DURATION_FIELD_WIDTH, CONTROL_PANEL_TEXT_FIELD_HEIGHT);
        Styler.setSize(this.memory, MEMORY_FIELD_WIDTH, CONTROL_PANEL_TEXT_FIELD_HEIGHT);
        Styler.setSize(this.logicLevel, LOGIC_LEVEL_FIELD_WIDTH, CONTROL_PANEL_TEXT_FIELD_HEIGHT);
    }

    /**
     * Adds Control Buttons to the control panel.
     */
    private void createControlButtons() {

        this.startButton = new Button(START_BUTTON_ICONS);
        this.stopButton = new Button(STOP_BUTTON_ICONS);
        this.settingsButton = new Button(SETTINGS_BUTTON_ICONS);

        this.startButton.addActionListener(actionEvent -> Executors.newSingleThreadExecutor().submit((() -> {

            if(!this.closeDecodingFrames()) {
                return;
            }

            STMConfiguration stmConfiguration = this.getSTMConfiguration();
            if(stmConfiguration == null) return;

            this.enableSamplingMode();
            this.stopButton.setEnabled(false);

            if(this.startSampling(stmConfiguration)) {

                this.stopButton.setEnabled(true);
                this.scheduleChannelRefresh();

            } else this.enableConfiguringMode();

        })));

        this.stopButton.addActionListener(actionEvent -> Executors.newSingleThreadExecutor().submit((this::stopSampling)));

        this.settingsButton.addActionListener(actionEvent -> OptionsFrame.open(this));

        this.add(this.startButton);
        this.add(this.stopButton);
        this.add(this.settingsButton);
        this.add(Box.createHorizontalGlue());

        Styler.addSeparator(this, JSeparator.VERTICAL, true);

        this.add(Box.createHorizontalGlue());

        Styler.style(this.startButton, Styler.Style.CONTROL_PANEL);
        Styler.style(this.stopButton, Styler.Style.CONTROL_PANEL);
        Styler.style(this.settingsButton, Styler.Style.CONTROL_PANEL);
    }

    /**
     * Adds callbacks to the {@link ChannelPanel} to refresh the {@link DecoderViewPane} whenever the channel contents
     * are modified. Also adds a callback to the {@link STMController} to trigger a refresh whenever sampling finishes.
     */
    private void addDecoderViewCallbacks() {

        this.channelPanel.addChannelModificationListener(new ChannelModificationListener() {

            @Override
            public void channelAdded(Channel channel) {ControlPanel.this.decoderViewPane.refreshDecoders();}

            @Override
            public void channelRemoved(Channel channel) {ControlPanel.this.decoderViewPane.refreshDecoders();}

            @Override
            public void channelEdited(Channel channel) {ControlPanel.this.decoderViewPane.refreshDecoders();}
        });

        this.controller.addSamplingCompletionTask(this.decoderViewPane::refreshDecoders, LOW_PRIORITY);
        this.controller.addSamplingErrorTask(this.decoderViewPane::refreshDecoders, LOW_PRIORITY);

        this.channelPanel.addChannelRemovalConfirmation(channel -> {

            String dependentDecoders = this.decoderViewPane.getUsedDecoders().stream()
                    .filter(decoder -> this.decoderViewPane.getDecoderStatus(decoder) != DecoderPanel.DecoderStatus.CHANNEL_MISSING)
                    .filter(decoder -> decoder.getUsedChannelIndices().contains(channel.getChannelIndex()))
                    .map(this.decoderViewPane::getDecoderName).map("\"%s\""::formatted)
                    .collect(Collectors.joining(", "));

            if(dependentDecoders.isEmpty()) {
                return true;
            }

            return ConfirmationBox.confirm("Removing the channel \"" + channel.getChannelName() + "\" will " +
                    "invalidate the following decoders: " + dependentDecoders + "\n" +
                    "Do you want to remove it anyways?", this);
        });
    }

    /**
     * If any decoding frames are open, calling this method will prompt the user and close all active decoding frames upon
     * confirmation.
     *
     * @return True if no decoding frame was open or the user confirmed they want to close the frames, false otherwise.
     */
    private boolean closeDecodingFrames() {

        if(DecodingFrame.isFrameOpen()) {
            if(!ConfirmationBox.confirm("Resampling will close all active decoding windows. Do you want to continue?", this)) {
                return false;
            }

            DecodingFrame.closeAllFrames();
        }
        return true;
    }

    /**
     * Adds tasks to the shared {@link STMController} instance.
     */
    private void addSamplingTasks() {

        this.controller.addSamplingErrorTask(this::stopSampling, HIGH_PRIORITY);
        this.controller.addSamplingCompletionTask(this::stopSampling, HIGH_PRIORITY);
    }

    /**
     * Refreshes all selected channels with the latest sample information.
     */
    private void refreshChannels() {

        SampleDataModel.Snapshot finalSnapshot = this.dataModel.getFinalSnapshot();

        try {
            this.refreshSemaphore.acquire();
        } catch (InterruptedException ignored) {}

        this.refreshChannels(finalSnapshot);

        logger.info("Final refresh finished.");
        this.refreshSemaphore.release();

        this.disableProgressBars(finalSnapshot.getChannelIndices());
    }

    /**
     * Refreshes all eligible channels with the latest sample information.
     *
     * @param snapshot The sample information as a {@link SampleDataModel.Snapshot}.
     */
    private void refreshChannels(SampleDataModel.Snapshot snapshot) {

        snapshot.getChannelIndices().forEach(channelIndex -> {
            Channel.getChannel(channelIndex).displaySamples(snapshot.getSampleTime(), snapshot.getSampleValues(channelIndex));
        });
    }

    /**
     * Disables the progress bars for the channels with the specified indices.
     *
     * @param channelIndices A {@link List} of channel indices for which the progress bars should be disabled.
     */
    private void disableProgressBars(List<Integer> channelIndices) {

        channelIndices.stream().map(Channel::getChannel).forEach(Channel::disableSamplingProgress);
        logger.info("Channel progress bars disabled for channels: " + channelIndices);
    }

    /**
     * Schedules a {@link Timer} to refresh the plots of all selected channels every fixed timer interval.
     */
    private void scheduleChannelRefresh() {


        List<Channel> selectedChannels = this.getSelectedChannels();
        this.channelPanel.getChannels().forEach(channel -> channel.clearDisplay(selectedChannels.contains(channel)));

        selectedChannels.forEach(channel -> {

            channel.setSamplingProgressLimit(this.dataModel.getTotalSamples());
            channel.enableSamplingProgress();
        });

        this.refreshSemaphore = new Semaphore(1);
        SampleDataModel dataModel = this.dataModel;

        this.refreshTimer = new Timer(PLOT_REFRESH_RATE, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {

                Thread.currentThread().setName("Timer Triggered Refresh Thread.");

                if (!ControlPanel.this.refreshSemaphore.tryAcquire()) return;
                logger.fine("Timer triggered refresh started.");

                if (dataModel.getSampleCount() <= lastNSamples) {

                    logger.fine("No new data. Refresh aborted.");
                    ControlPanel.this.refreshSemaphore.release();
                    return;
                }

                SampleDataModel.Snapshot dataSnapshot = dataModel.getSnapshot();
                this.lastNSamples = dataSnapshot.getSampleCount();

                ControlPanel.this.refreshChannels(dataSnapshot);

                logger.fine("Timer triggered refresh complete.");

                ControlPanel.this.refreshSemaphore.release();
            }

            private int lastNSamples;
        });

        this.refreshTimer.start();
    }

    /**
     * Stops the channel refresh timer.
     */
    private void stopChannelRefresh() {

        assert this.refreshTimer != null : "The refresh timer has not been set up.";
        this.refreshTimer.stop();
    }

    /**
     * Refreshes the user specified sampling parameters.
     */
    private void refreshSamplingParameters() {
        SwingUtilities.invokeLater(() -> {
            this.samplingDuration.setText(this.samplingDuration.getText());
            this.memory.setText(this.memory.getText());
        });
    }

    /**
     * @return The sampling memory (in bytes) corresponding to the current sample rate, duration and number of
     * selected channels.
     */
    private int calculateSamplingMemory() {

        int requiredBits = (int) Math.ceil(this.getSelectedSamplingRate() * this.getSelectedSamplingDuration() * this.getSelectedChannels().size());
        return (requiredBits - 1) / Byte.SIZE + 1;
    }

    /**
     * @return The sampling duration (in Samples/Second) corresponding to the current sample rate, memory and number
     * of selected channels.
     */
    private double calculateSamplingDuration() {

        int specifiedBits = this.getSelectedSamplingMemory() * Byte.SIZE;
        return (double) (specifiedBits / this.getSelectedChannels().size()) / this.getSelectedSamplingRate();
    }

    /**
     * Links the text fields where the user can specify sampling duration and memory. The value for one can always
     * can be determined from the other.
     */
    private void linkSamplingDurationAndMemory() {

        final AtomicBoolean linking = new AtomicBoolean(false);

        this.samplingDuration.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {this.changedUpdate(e);}

            @Override
            public void removeUpdate(DocumentEvent e) {this.changedUpdate(e);}

            @Override
            public void changedUpdate(DocumentEvent e) {

                if(!linking.compareAndSet(false, true)) return;

                SwingUtilities.invokeLater(() -> {

                    if(ControlPanel.this.getSelectedChannels().size() > 0 && samplingDuration.validateData(false)) {

                        int calculatedMemory = ControlPanel.this.calculateSamplingMemory();
                        int memoryUnit = ControlPanel.this.getSelectedSamplingMemoryUnit();

                        double memory = (double) calculatedMemory / memoryUnit;
                        memory = new BigDecimal(memory).setScale(MAXIMUM_N_DECIMAL_POINTS, RoundingMode.HALF_UP).doubleValue();

                        ControlPanel.this.memory.setText(String.valueOf(memory));
                    }
                    linking.set(false);
                });
            }
        });

        this.memory.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {this.changedUpdate(e);}

            @Override
            public void removeUpdate(DocumentEvent e) {this.changedUpdate(e);}

            @Override
            public void changedUpdate(DocumentEvent e) {

                if(!linking.compareAndSet(false, true)) return;

                SwingUtilities.invokeLater(() -> {

                    if(ControlPanel.this.getSelectedChannels().size() > 0 && memory.validateData(false)) {

                        double calculatedSamplingDuration = ControlPanel.this.calculateSamplingDuration();
                        double samplingDurationUnit = ControlPanel.this.getSelectedSamplingDurationUnit();

                        double samplingDuration = calculatedSamplingDuration / samplingDurationUnit;
                        samplingDuration = new BigDecimal(samplingDuration).setScale(MAXIMUM_N_DECIMAL_POINTS, RoundingMode.HALF_UP).doubleValue();

                        ControlPanel.this.samplingDuration.setText(String.valueOf(samplingDuration));
                    }
                    linking.set(false);
                });
            }
        });
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final STMController controller;
    private final SampleDataModel dataModel;

    private final ChannelPanel channelPanel;
    private final DecoderViewPane decoderViewPane;

    private TextField samplingDuration;
    private TextField memory;

    private JComboBox <String> samplingRate;
    private JComboBox <String> samplingDurationUnitBox;
    private JComboBox <String> memoryUnitBox;
    private JComboBox <String> logicLevel;

    private Button startButton;
    private Button stopButton;
    private Button settingsButton;

    private final AtomicBoolean isSampling;
    private Timer refreshTimer;

    private Semaphore refreshSemaphore;

    /**
     * A {@link DataValidator} used for validating the user specified sampling duration value.
     */
    private final DataValidator samplingDurationValidator = data -> {

        double value;

        try {
            value = Double.parseDouble(data) * this.getSelectedSamplingDurationUnit();}
        catch (NumberFormatException e) {
            return "Input must be a floating point number.";
        }

        int bitsPerCycle = this.getSelectedChannels().size();
        int requiredBits = (int) Math.ceil(this.getSelectedSamplingRate() * value * bitsPerCycle);

        if(value <= 0) {
            return "The sampling duration must be a positive number.";
        } else if((requiredBits - 1) / Byte.SIZE + 1 > MAX_MEMORY) {
            return "Sampling for the specified duration will exceed the maximum memory limit of " + MAX_MEMORY + " MB.";
        } else {
            return null;
        }
    };

    /**
     * A {@link DataValidator} used for validating the user specified sampling memory value.
     */
    private final DataValidator samplingMemoryValidator = data -> {

        int value;

        try {

            value = (int) (Double.parseDouble(data) * this.getSelectedSamplingMemoryUnit());
            if(value != Double.parseDouble(data) * this.getSelectedSamplingMemoryUnit()) {
                throw new NumberFormatException();
            }
        } catch (NumberFormatException e) {
            return "Input must be an integer value.";
        }

        if(value <= 0) {
            return "The sampling memory must be a positive number.";
        } else if(value > MAX_MEMORY) {
            return "The specified memory will exceed the maximum limit of " + MAX_MEMORY + " MB.";
        } else {
            return null;
        }
    };
}
