package decoder;

import channel.Channel;
import data.DataValidator;
import decoder.panel.I2CPanel;
import decoder.panel.ParallelEncoderPanel;
import decoder.panel.ProtocolPanel;
import decoder.panel.SPIPanel;
import logging.ConfiguredLogger;
import org.jetbrains.annotations.Nullable;
import protocols.protocol.ProtocolType;
import styling.Styler;
import textfield.TextField;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import static data.DataConstants.PROTOCOLS;
import static styling.GUIConstants.GENERIC_COMPONENT_GAP;
import static styling.GUIConstants.GENERIC_TEXT_FIELD_COLUMNS;

public class DecoderOptionsFrame extends JFrame {

    private static final Logger logger = ConfiguredLogger.getLogger("DecoderFrame");

    private static final int PROTOCOL_PANE_WIDTH = 400;
    private static final int PROTOCOL_PANE_HEIGHT = 500;

    private static final int FRAME_WIDTH = 500;
    private static final int FRAME_HEIGHT = 600;


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Opens a new Decoder Options Menu and waits for a user response. If the menu is already opened, it will be
     * brought to the front of the screen and the method will return immediately with an empty response.
     *
     * @param availableChannels The currently available channels to be used as a {@link List}.
     * @param decoderNameValidator The {@link DataValidator} object used for validating the name of the decoder.
     *
     * @return The user chosen decoder as a {@link DecoderInfo} record or null if there is no response.
     */
    @Nullable
    public static DecoderInfo openDecoderOptions(List<Channel> availableChannels, DataValidator decoderNameValidator){

        try {
            openLock.lock();

            if (openedFrame != null) {

                openedFrame.toFront();
                Styler.centerPopUp(openedFrame);
                return null;
            }

            openedFrame = new DecoderOptionsFrame(availableChannels, decoderNameValidator);
        } finally {
            openLock.unlock();
        }

        DecoderInfo decoderInfo = openedFrame.getResponse();
        openedFrame.dispose();

        openedFrame = null;

        return decoderInfo;
    }

    /**
     * Updates the list of available channels for this {@link DecoderOptionsFrame}.
     *
     * @param availableChannels The {@link List} of currently available channels.
     */
    public static void updateAvailableChannels(List<Channel> availableChannels) {

        try {
            openLock.lock();

            if (openedFrame != null) {
                logger.log(Level.FINE, "Currently no decoder options are opened.");
                openedFrame.setAvailableChannels(availableChannels);
            }
        } finally {
            openLock.unlock();
        }
    }

    /**
     * Updates the channel selectors in this {@link DecoderOptionsFrame} with the specified channel information.
     *
     * @param availableChannels A {@link List} of currently available channels.
     */
    public void setAvailableChannels(List<Channel> availableChannels) {

        this.availableChannels = availableChannels;
        this.refreshProtocolPanel((ProtocolType) Objects.requireNonNull(this.protocolComboBox.getSelectedItem()));
    }


    /** === Private Methods -------------------------------------------------- **/

    /**
     * Creates a new Decoder frame with the specified channel information
     *
     * @param availableChannels The currently available channels to be used as a {@link List}.
     * @param decoderNameValidator The {@link DataValidator} object used for validating the name of the decoder.
     */
    private DecoderOptionsFrame(List<Channel> availableChannels, DataValidator decoderNameValidator) {

        super("Decoder Options");

        this.availableChannels = availableChannels;
        this.responseLatch = new CountDownLatch(1);
        this.decoderNameValidator = decoderNameValidator;
        this.cachedProtocolPanels = new HashMap<>();

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        this.addComponents();
        this.refreshProtocolPanel(ProtocolType.PARALLEL_ENCODER);

        this.setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
        this.setResizable(false);

        this.pack();

        Styler.centerPopUp(this);
        this.setVisible(true);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                DecoderOptionsFrame.this.setNullResponse();
            }
        });
    }

    /**
     * Blocks the current thread and waits for the user response.
     *
     * @return The user chosen decoder as a {@link Decoder} or null if the response was invalid.
     */
    private @Nullable DecoderInfo getResponse() {

        try{
            this.responseLatch.await();
        } catch(InterruptedException ignored){}

        if(this.protocolPanel == null) {

            logger.info("A decoder was not  generated.");
            return null;
        }

        Decoder decoder = this.protocolPanel.getProtocolDecoder();
        logger.info("Generated decoder: " + decoder);

        assert this.decoderNameField.validateData():
                "The decoder has an invalid name: " + this.decoderNameField.getText();

        return new DecoderInfo(decoder, this.decoderNameField.getText());
    }

    /**
     * Adds GUI components to this {@link DecoderOptionsFrame}.
     */
    private void addComponents() {

        Box mainPanel = Box.createVerticalBox();

        //Add field for specifying the decoder name
        mainPanel.add(createNameBox());

        //Add ComboBox for choosing protocol
        mainPanel.add(createProtocolSelectorBox());

        //Add channel selector and protocol options panel
        mainPanel.add(createProtocolPane());

        //Add 'cancel' and 'add' buttons
        mainPanel.add(Box.createVerticalGlue());
        mainPanel.add(createButtonBox());

        this.add(mainPanel);
    }

    /**
     * @return The panel where the name of the new decoder will be specified, as a {@link Box}.
     */
    private Box createNameBox() {

        Box nameBox = Box.createHorizontalBox();

        this.decoderNameField = new TextField(GENERIC_TEXT_FIELD_COLUMNS, this.decoderNameValidator);

        nameBox.add(Box.createRigidArea(new Dimension(GENERIC_COMPONENT_GAP, GENERIC_COMPONENT_GAP)));
        nameBox.add(Styler.style(new JLabel("Decoder Name:")));
        nameBox.add(Box.createHorizontalGlue());
        nameBox.add(Styler.style(this.decoderNameField, Styler.Style.LONG));
        nameBox.add(Box.createRigidArea(new Dimension(GENERIC_COMPONENT_GAP, GENERIC_COMPONENT_GAP)));

        return nameBox;
    }

    /**
     * @return The panel with the protocol selector {@link JComboBox} as a {@link Box}.
     */
    private Box createProtocolSelectorBox(){

        Box protocolBox = Box.createHorizontalBox();

        this.protocolComboBox = new JComboBox<>(PROTOCOLS);
        this.protocolComboBox.setOpaque(true);
        this.protocolComboBox.addActionListener(this.protocolBoxListener);

        protocolBox.add(Box.createRigidArea(new Dimension(GENERIC_COMPONENT_GAP, GENERIC_COMPONENT_GAP)));
        protocolBox.add(Styler.style(new JLabel("Protocol:")));
        protocolBox.add(Box.createHorizontalGlue());
        protocolBox.add(Styler.style(this.protocolComboBox, Styler.Style.LONG));
        protocolBox.add(Box.createRigidArea(new Dimension(GENERIC_COMPONENT_GAP, GENERIC_COMPONENT_GAP)));

        return protocolBox;
    }

    /**
     * @return A panel for the Add and Cancel buttons, as a {@link Box}.
     */
    private Box createButtonBox() {

        Box buttonBox = Box.createHorizontalBox();

        JButton cancelButton = new JButton("Cancel");
        this.addButton = new JButton("Add");

        this.addButton.setEnabled(false);

        buttonBox.add(Box.createHorizontalGlue());
        buttonBox.add(cancelButton);
        buttonBox.add(Box.createRigidArea(new Dimension(GENERIC_COMPONENT_GAP, GENERIC_COMPONENT_GAP)));
        buttonBox.add(this.addButton);
        buttonBox.add(Box.createRigidArea(new Dimension(GENERIC_COMPONENT_GAP, GENERIC_COMPONENT_GAP)));

        cancelButton.addActionListener(actionEvent -> this.setNullResponse());
        this.addButton.addActionListener(actionEvent -> this.setResponse());

        return buttonBox;
    }

    /**
     * @return A {@link JScrollPane} where the protocol related settings will be visible.
     */
    private JScrollPane createProtocolPane() {

        this.protocolPanePanel = new JPanel();
        this.protocolPanePanel.setLayout(new BoxLayout(this.protocolPanePanel, BoxLayout.Y_AXIS));

        JScrollPane protocolPane = new JScrollPane(this.protocolPanePanel);

        protocolPane.setPreferredSize(new Dimension(PROTOCOL_PANE_WIDTH, PROTOCOL_PANE_HEIGHT));
        Styler.style(this.protocolPanePanel);

        return protocolPane;
    }

    /**
     * Refreshes the protocol panel with protocol decoder options for the specified protocol type.
     *
     * @param protocol The protocol type as a {@link ProtocolType}.
     */
    private void refreshProtocolPanel(ProtocolType protocol) {

        Channel[] availableChannels = this.availableChannels.toArray(Channel[]::new);

        switch (protocol){

            case PARALLEL_ENCODER -> cachedProtocolPanels.putIfAbsent(protocol, new ParallelEncoderPanel(availableChannels));
            case SPI -> cachedProtocolPanels.putIfAbsent(protocol, new SPIPanel(availableChannels));
            case I2C -> cachedProtocolPanels.putIfAbsent(protocol, new I2CPanel(availableChannels));

            default -> throw new IllegalStateException("Unexpected value: " + protocol);
        }

        this.protocolPanel = this.cachedProtocolPanels.get(protocol);
        this.protocolPanel.display(availableChannels);

        this.protocolPanePanel.removeAll();
        this.protocolPanePanel.add(this.protocolPanel.getProtocolOptionsBox());
        this.protocolPanePanel.add(this.protocolPanel.getChannelSelectorBox());

        this.addButton.setEnabled(this.protocolPanel.isInitialized());

        this.protocolComboBox.removeActionListener(this.protocolBoxListener);
        this.protocolComboBox.setSelectedItem(protocol);
        this.protocolComboBox.addActionListener(this.protocolBoxListener);

        SwingUtilities.invokeLater(()->{
            this.protocolPanePanel.revalidate();
            this.protocolPanePanel.repaint();
        });
    }

    /**
     * Sets the user specified settings as a response.
     */
    private void setResponse() {

        if(this.protocolPanel.validateChannelMapping() && this.decoderNameField.validateData()) {

            logger.info("A valid decoder has been chosen.");
            this.responseLatch.countDown();
        }
    }

    /**
     * Sets a null response for the awaiting thread.
     */
    private void setNullResponse() {

        this.protocolPanel = null;
        this.responseLatch.countDown();
    }


    /** === Instance Variables ----------------------------------------------- **/

    private List<Channel> availableChannels;

    private final Map<ProtocolType, ProtocolPanel> cachedProtocolPanels;

    private final DataValidator decoderNameValidator;

    private JPanel protocolPanePanel;
    private TextField decoderNameField;
    private JComboBox<ProtocolType> protocolComboBox;
    private ProtocolPanel protocolPanel;
    private JButton addButton;

    private final CountDownLatch responseLatch;

    ActionListener protocolBoxListener = actionEvent -> SwingUtilities.invokeLater(() -> {
        refreshProtocolPanel((ProtocolType) Objects.requireNonNull(this.protocolComboBox.getSelectedItem()));
    });


    /** === Static Variables ------------------------------------------------- **/

    private static DecoderOptionsFrame openedFrame;
    private static final Lock openLock;

    static {
        openLock = new ReentrantLock();
    }
}
