package decoder;

import com.formdev.flatlaf.FlatDarculaLaf;
import data.SampleDataModel;
import decoder.data.message.DecodedMessage;
import logging.ConfiguredLogger;
import popups.PopupTextPanel;
import styling.Styler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static styling.GUIConstants.SMALL_COMPONENT_GAP;

public class DecodingFrame extends JFrame {

    private static final Logger logger = ConfiguredLogger.getLogger("SPIDecoder");

    private static final int DEFAULT_FRAME_WIDTH = 1000;
    private static final int DEFAULT_FRAME_HEIGHT = 500;

    private static final String EDITOR_CONTENT_TYPE = "text/html";
    private static final String INITIAL_DATA_TEXT =
            """
            <h2>Decoded data content will be displayed here...</h2>
            """;

    private static final String SEPARATOR = "<hr>";


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Opens a decoding frame for the specified decoder. If a frame is already opened then it is
     * updated with the specified snapshot. If the snapshot is the same, the frame is simply brought to the
     * front of the screen.
     *
     * @param decoder The decoder as a {@link Decoder}.
     * @param snapshot The snapshot of the Data Model to be used as a {@link SampleDataModel.Snapshot}.
     */
    public static void open(Decoder decoder, SampleDataModel.Snapshot snapshot) {

        synchronized (decodingFrames) {

            if(!decodingFrames.containsKey(decoder)) {
                decodingFrames.put(decoder, new DecodingFrame(decoder, snapshot));
            }

            DecodingFrame decodingFrame = decodingFrames.get(decoder);

            decodingFrame.updateSnapshot(snapshot);
            decodingFrame.toFront();
        }
    }

    /**
     * @return True if at least one {@link DecodingFrame} is currently active.
     */
    public static boolean isFrameOpen() {
        return !decodingFrames.isEmpty();
    }

    /**
     * Immediately closes all active {@link DecodingFrame}s.
     */
    public static void closeAllFrames() {

        synchronized (decodingFrames) {

            decodingFrames.values().forEach(DecodingFrame::dispose);
            decodingFrames.clear();
        }
    }

    /**
     * Closes the specified decoding frame if it is active.
     *
     * @param frame The decoding frame to be closed as a {@link DecodingFrame}.
     */
    public static void closeFrame(DecodingFrame frame) {

        assert frame != null : "The specified frame must not be null.";

        if(frame.isVisible()) {
            frame.dispose();
        }

        synchronized (decodingFrames) {
            decodingFrames.remove(frame.decoder);
        }
    }


    /** === Main ------------------------------------------------------------- **/

    public static void main(String[] args) {

        FlatDarculaLaf.setup();

        try {
            UIManager.setLookAndFeel(new FlatDarculaLaf());
        } catch (Exception ignored) {}

        new DecodingFrame(null, null);
    }


    /** === Private Methods -------------------------------------------------- **/

    /**
     * Updates this {@link DecodingFrame} with the specifies data snapshot.
     *
     * @param snapshot The data snapshot as a {@link SampleDataModel.Snapshot}.
     */
    private void updateSnapshot(SampleDataModel.Snapshot snapshot) {

        if(snapshot == this.snapshot) return;

        this.snapshot = snapshot;
        this.refreshControls();
    }

    /**
     * Creates a {@link DecodingFrame} from the specified decoder and data snapshot.
     *
     * @param decoder The decoder as a {@link Decoder}.
     * @param snapshot The data snapshot as a {@link SampleDataModel.Snapshot}.
     */
    private DecodingFrame(Decoder decoder, SampleDataModel.Snapshot snapshot) {

        super();

        this.decoder = decoder;
        this.snapshot = snapshot;

        this.mainPanel = new JPanel();

        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        this.addComponents();

        this.setPreferredSize(new Dimension(DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT));

        this.pack();

        Styler.centerPopUp(this);
        this.setVisible(true);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                closeFrame(DecodingFrame.this);
            }
        });
    }

    /**
     * Adds the decoded data text area and the controls to this {@link DecodingFrame}.
     */
    private void addComponents() {

        this.mainPanel.setLayout(new BorderLayout());

        this.mainPanel.add(this.createDataArea(), BorderLayout.CENTER);
        this.mainPanel.add(this.createControls(), BorderLayout.PAGE_END);

        this.getContentPane().add(mainPanel);

        this.clearDataArea();
    }

    /**
     * @return The {@link Box} containing the {@link JTextPane} with the decoded data information.
     */
    private Box createDataArea() {

        Box dataBox = Box.createVerticalBox();

        this.dataEditor = new JEditorPane();
        this.dataEditor.setContentType(EDITOR_CONTENT_TYPE);
        this.dataEditor.setEditable(false);
        this.dataEditor.setDoubleBuffered(true);
        this.dataEditor.setText(INITIAL_DATA_TEXT);

        JScrollPane dataScrollPane = new JScrollPane(Styler.style(this.dataEditor));
        dataScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        dataScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        dataScrollPane.setBorder(BorderFactory.createEtchedBorder());

        dataBox.add(dataScrollPane);

        return dataBox;
    }

    /**
     * @return The {@link Box} containing controls for decoding
     */
    private Box createControls() {

        Box controlBox = Box.createHorizontalBox();

        controlBox.add(this.createSampleSelectorsBox());
        controlBox.add(this.createButtonBox());
        controlBox.add(Box.createHorizontalGlue());

        return controlBox;
    }

    /**
     * @return The {@link Box} containing the sample selectors for the start and end sample.
     */
    private Box createSampleSelectorsBox() {

        Box sampleSelectorsBox = Box.createHorizontalBox();

        this.startSampleBox = SampleSelectorBox.from(this.snapshot);
        this.endSampleBox = SampleSelectorBox.from(this.snapshot);

        this.startSampleBox.addActionListener(this.sampleSelectorLinker);
        this.endSampleBox.addActionListener(this.sampleSelectorLinker);

        this.startSampleBox.linkSampleSelectorBox(this.endSampleBox);
        this.endSampleBox.linkSampleSelectorBox(this.startSampleBox);

        Box labelBox = Box.createVerticalBox();
        labelBox.add(Box.createVerticalGlue());
        labelBox.add(Styler.style(new JLabel("Start Sample")));
        labelBox.add(Box.createVerticalGlue());
        labelBox.add(Styler.style(new JLabel("End Sample")));
        labelBox.add(Box.createVerticalGlue());

        Box selectorBox = Box.createVerticalBox();
        selectorBox.add(this.startSampleBox);
        selectorBox.add(this.endSampleBox);

        sampleSelectorsBox.add(Box.createRigidArea(new Dimension(SMALL_COMPONENT_GAP, SMALL_COMPONENT_GAP)));
        sampleSelectorsBox.add(Box.createHorizontalGlue());
        sampleSelectorsBox.add(labelBox);
        sampleSelectorsBox.add(Box.createRigidArea(new Dimension(SMALL_COMPONENT_GAP, SMALL_COMPONENT_GAP)));
        sampleSelectorsBox.add(selectorBox);
        sampleSelectorsBox.add(Box.createHorizontalGlue());

        return sampleSelectorsBox;
    }

    /**
     * @return The {@link Box} containing all control buttons used by this {@link DecodingFrame}.
     */
    private Box createButtonBox() {

        Box buttonBox = Box.createHorizontalBox();

        this.decodeButton = new JButton("Decode");
        this.clearButton = new JButton("Clear");
        this.closeButton = new JButton("Close");

        this.decodeButton.addActionListener(actionEvent -> Executors.newSingleThreadExecutor().submit(this::decode));
        this.clearButton.addActionListener(actionEvent -> SwingUtilities.invokeLater(this::clearDataArea));
        this.closeButton.addActionListener(actionEvent -> closeFrame(this));

        buttonBox.add(Box.createHorizontalGlue());
        buttonBox.add(this.decodeButton);
        buttonBox.add(Box.createRigidArea(new Dimension(SMALL_COMPONENT_GAP, SMALL_COMPONENT_GAP)));
        buttonBox.add(this.clearButton);
        buttonBox.add(Box.createRigidArea(new Dimension(SMALL_COMPONENT_GAP, SMALL_COMPONENT_GAP)));
        buttonBox.add(this.closeButton);
        buttonBox.add(Box.createHorizontalGlue());

        return buttonBox;
    }

    /**
     * Updates the start and end sample selectors with the current {@link SampleDataModel.Snapshot}.
     */
    private void refreshControls() {
        this.startSampleBox.updateSnapshot(this.snapshot);
        this.endSampleBox.updateSnapshot(this.snapshot);
    }

    /**
     * Decodes the currently selected samples and displays the decoded data in the data area of
     * this {@link DecodingFrame}.
     */
    private synchronized void decode() {

        try {

            if (!this.startSampleBox.validateData() && this.endSampleBox.validateData()) {
                return;
            }

            List.of(this.startSampleBox, this.endSampleBox, this.decodeButton, this.clearButton, this.closeButton)
                    .forEach(component -> component.setEnabled(false));

            Map<Integer, Byte[]> sampleValues = this.decoder.getUsedChannelIndices().stream()
                    .map(channelIndex -> Map.entry(channelIndex, this.snapshot.getSampleValues(channelIndex)))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

            double startTime = this.startSampleBox.getValue();
            double endTime = this.endSampleBox.getValue();

            int startIndex = (int) Math.floor(startTime / this.snapshot.getSamplingPeriod());
            int endIndex = (int) Math.floor(endTime / this.snapshot.getSamplingPeriod());

            List<DecodedMessage> decodedMessages =
                    this.decoder.decode(this.snapshot.getSampleTime(), sampleValues, startIndex, endIndex);

            this.decodedInfo += SEPARATOR +
                    decodedMessages.stream().map(DecodedMessage::toHTMLString).collect(Collectors.joining("<br>"));

            this.dataEditor.getEditorKit().createDefaultDocument();
            this.dataEditor.setText(this.decodedInfo);

            logger.info("Decoded message:\n"
                    + decodedMessages.stream().map(DecodedMessage::toString).collect(Collectors.joining("\n")));

        } catch (Throwable e) {

            PopupTextPanel.displayPopup(this.decodeButton,
                    "An error occurred while decoding the specified samples.",
                    PopupTextPanel.PopupType.ERROR,
                    PopupTextPanel.PopupLocation.RIGHT,
                    false);

            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);

            e.printStackTrace(printWriter);

            logger.severe("An error occurred while decoding the samples.\nException Trace: " + stringWriter);

        } finally {
            List.of(this.startSampleBox, this.endSampleBox, this.decodeButton, this.clearButton, this.closeButton)
                    .forEach(component -> component.setEnabled(true));
        }
    }

    /**
     * Clears all text present in the text area for displaying data.
     */
    private void clearDataArea() {

        this.dataEditor.getEditorKit().createDefaultDocument();
        this.dataEditor.setText(INITIAL_DATA_TEXT);
        this.decodedInfo = "";
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final JPanel mainPanel;
    private final Decoder decoder;

    private String decodedInfo;

    private SampleDataModel.Snapshot snapshot;

    private JEditorPane dataEditor;

    private SampleSelectorBox startSampleBox;
    private SampleSelectorBox endSampleBox;

    private JButton decodeButton;
    private JButton clearButton;
    private JButton closeButton;

    /**
     * Links the sample selector boxes so that their values always form a well-formed range.
     */
    private final ActionListener sampleSelectorLinker = actionEvent -> {

        List.of(this.startSampleBox, this.endSampleBox).forEach(selector -> {
            selector.removeActionListener(this.sampleSelectorLinker);
        });

        if(actionEvent.getSource() == this.startSampleBox && this.startSampleBox.validateData() &&
                this.startSampleBox.getValue() > this.endSampleBox.getValue()) {
            this.endSampleBox.setValue(this.startSampleBox.getValue());
        } else if(actionEvent.getSource() == this.endSampleBox && this.endSampleBox.validateData() &&
                this.endSampleBox.getValue() < this.startSampleBox.getValue()) {
            this.startSampleBox.setValue(this.endSampleBox.getValue());
        }

        List.of(this.startSampleBox, this.endSampleBox).forEach(selector -> {
            selector.addActionListener(this.sampleSelectorLinker);
        });
    };


    /** === Static Variables ------------------------------------------------- **/

    private static final HashMap<Decoder, DecodingFrame> decodingFrames;

    static {
        decodingFrames = new HashMap<>();
    }
}
