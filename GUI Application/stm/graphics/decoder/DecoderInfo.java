package decoder;

import java.util.Objects;

public record DecoderInfo(Decoder decoder, String decoderName)  {

    /**
     * Holds information about a newly created decoder.
     *
     * @param decoder The decoder as a {@link Decoder}.
     * @param decoderName The name of the decoder as a {@link String}.
     */
    public DecoderInfo {
        Objects.requireNonNull(decoder, "The Decoder must be non-null.");
        Objects.requireNonNull(decoderName, "The Name for the decoder must be non-null.");
    }
}
