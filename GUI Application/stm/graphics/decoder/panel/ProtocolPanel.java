package decoder.panel;

import boxes.FieldBox;
import channel.Channel;
import decoder.Decoder;
import org.jetbrains.annotations.NotNull;
import protocols.protocol.ProtocolType;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class ProtocolPanel {

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a protocol decoder from the current user specified protocol options.
     *
     * @return The protocol decoder as a {@link Decoder}.
     */
    public final @NotNull Decoder getProtocolDecoder() {

        assert this.validateChannelMapping(): "The current channel mapping is invalid.";
        return this.createProtocolDecoder();
    }

    /**
     * @return The {@link FieldBox} containing fields for the protocol options.
     */
    public final FieldBox getProtocolOptionsBox(){
        return this.optionsBox;
    }

    /**
     * @return The {@link FieldBox} containing fields for the channel mappings.
     */
    public final FieldBox getChannelSelectorBox(){
        return this.channelSelectorBox;
    }

    /**
     * Checks that all channel mappings are one-to-one.
     *
     * @return True if the channel mappings are valid, false otherwise.
     */
    public boolean validateChannelMapping(){

        Set <String> usedChannels = new HashSet<>();
        Map <String, Object> selectedChannels = this.channelSelectorBox.getValues();

        for(String channel: selectedChannels.keySet()){

            assert selectedChannels.get(channel) != null : "Null channel mapping found for channel " + channel + ".";

            Channel selectedChannel = (Channel) selectedChannels.get(channel);
            if(usedChannels.contains(selectedChannel.getChannelName())){

                this.channelSelectorBox.displayError(channel,"This channel is already selected.");
                return false;
            }
            usedChannels.add(selectedChannel.getChannelName());
        }
        return true;
    }

    /**
     * Displays or updates the protocol panel with the specfied channel information.
     *
     * @param availableChannels An array of the currently available {@link Channel}s.
     */
    public final void display(Channel[] availableChannels) {

        this.optionsBox.clearAllFields();
        this.channelSelectorBox.clearAllFields();

        if(availableChannels.length < this.getMinimumChannelCount()) {

            String errorMessage = "This protocol requires at least " + this.getMinimumChannelCount() + " available channels.";
            this.channelSelectorBox.addText(errorMessage);

            this.isInitialized = false;
        } else {
            this.initializeChannelSelectorBox(this.channelSelectorBox, availableChannels);
            this.initializeProtocolOptionsBox(this.optionsBox, availableChannels);

            this.isInitialized = true;
        }
    }

    /**
     * @return True if the protocol panel was successfully initialized.
     */
    public final boolean isInitialized() {
        return this.isInitialized;
    }


    /** === Private Methods -------------------------------------------------- **/

    /**
     * @return The protocol type associated with this protocol decoder as a {@link ProtocolType}.
     */
    protected abstract ProtocolType getProtocolType();

    /**
     * Initializes the protocol options box with the current channel data.
     *
     * @param optionsBox The {@link FieldBox} which should be initialized with all the option fields.
     * @param availableChannels An array of the currently available {@link Channel}s.
     */
    protected abstract void initializeProtocolOptionsBox(FieldBox optionsBox, Channel[] availableChannels);

    /**
     * Initializes the channel mapping box with the current channel data.
     *
     * @param channelSelectorBox The {@link FieldBox} which should be initialized with all the channel mapping fields.
     * @param availableChannels An array of the currently available {@link Channel}s.
     */
    protected abstract void initializeChannelSelectorBox(FieldBox channelSelectorBox, Channel[] availableChannels);

    /**
     * @return The minimum number of channels that should be available for the protocol options to be
     * properly initialized.
     */
    protected abstract int getMinimumChannelCount();

    /**
     * Creates a {@link ProtocolPanel} from the specified channel information.
     *
     * @param availableChannels An array of the currently available {@link Channel}s.
     */
    protected ProtocolPanel(Channel[] availableChannels) {

        this.optionsBox = FieldBox.createEmptyBox("Protocol Options");
        this.channelSelectorBox = FieldBox.createEmptyBox(null);

        this.display(availableChannels);
    }

    /**
     * @return A {@link Decoder} from the currently specified user options.
     */
    private @NotNull Decoder createProtocolDecoder() {

        Map<String, Integer> channelMappings = this.getChannelSelectorBox().getValues().entrySet().stream()
                .map(entry -> Map.entry(entry.getKey(), ((Channel) entry.getValue()).getChannelIndex()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        return Decoder.from(this.getProtocolType(), channelMappings, this.getProtocolOptionsBox().getValues());
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final FieldBox optionsBox;
    private final FieldBox channelSelectorBox;
    private boolean isInitialized;
}
