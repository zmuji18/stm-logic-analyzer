package decoder.panel;

import boxes.FieldBox;
import channel.Channel;
import protocols.protocol.ProtocolType;

public class UARTPanel extends ProtocolPanel{

    protected UARTPanel(Channel[] availableChannels) {
        super(availableChannels);
    }

    @Override
    protected ProtocolType getProtocolType() {return ProtocolType.UART;}

    @Override
    protected void initializeProtocolOptionsBox(FieldBox optionsBox, Channel[] availableChannels) {

    }

    @Override
    protected void initializeChannelSelectorBox(FieldBox channelSelectorBox, Channel[] availableChannels) {

    }

    @Override
    protected int getMinimumChannelCount() {
        return 0;
    }
}
