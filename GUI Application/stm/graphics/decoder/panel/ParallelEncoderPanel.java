package decoder.panel;

import boxes.FieldBox;
import channel.Channel;
import protocols.options.ParallelEncoderOptions;
import protocols.protocol.ParallelEncoder;
import protocols.protocol.ProtocolType;

import java.util.stream.IntStream;

public class ParallelEncoderPanel extends ProtocolPanel {

    private static final int MIN_N_CHANNELS = 2;

    private static final String FIXED_CHANNELS = "Fixed Channel(s)";
    private static final String BIT_CHANNELS = "Bit Channel(s)";

    @Override
    protected ProtocolType getProtocolType() {return ProtocolType.PARALLEL_ENCODER;}

    @Override
    protected int getMinimumChannelCount() {return MIN_N_CHANNELS;}

    @Override
    protected void initializeProtocolOptionsBox(FieldBox optionsBox, Channel[] availableChannels){

        String dataChannelCountOption = ParallelEncoderOptions.getDataChannelCountOption();

        optionsBox.addField(dataChannelCountOption,
                IntStream.range(1, availableChannels.length).boxed().toArray());

        optionsBox.addActionListener(dataChannelCountOption, actionEvent -> {

            Integer numberOfDataChannels = (Integer) optionsBox.getSelectedValue(dataChannelCountOption);

            if(numberOfDataChannels == null){
                return;
            }

            FieldBox bitChannelBox = this.getChannelSelectorBox().getBox(BIT_CHANNELS).clearAllFields();

            for(int i = 0; i < numberOfDataChannels; i++){

                bitChannelBox.addField(ParallelEncoder.getDataChannelName(i), availableChannels);
            }
        });

        optionsBox.setSelectedValue(dataChannelCountOption, 1);
    }

    @Override
    protected void initializeChannelSelectorBox(FieldBox channelSelectorBox, Channel[] availableChannels) {

        channelSelectorBox.addBox(FIXED_CHANNELS).addField(ParallelEncoder.getClockChannelName(), availableChannels);
        channelSelectorBox.addBox(BIT_CHANNELS);
    }

    public ParallelEncoderPanel(Channel[] availableChannels){super(availableChannels);}
}
