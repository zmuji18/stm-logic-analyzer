package decoder.panel;

import boxes.FieldBox;
import channel.Channel;
import protocols.options.SPIOptions;
import protocols.protocol.ProtocolType;
import protocols.protocol.SPI;

import java.util.stream.IntStream;

import static protocols.options.SPIOptions.BITS_PER_PACKET_VALUES;

public class SPIPanel extends ProtocolPanel{

    private static final int MIN_N_CHANNELS = 4;
    private static final String FIXED_CHANNELS = "Fixed Channel(s)";
    private static final String SLAVE_SELECT_CHANNEL = "Slave Select Channel(s)";

    public SPIPanel(Channel[] availableChannels) {
        super(availableChannels);
    }

    @Override
    protected ProtocolType getProtocolType() {return ProtocolType.SPI;}

    @Override
    protected int getMinimumChannelCount() {
        return MIN_N_CHANNELS;
    }

    @Override
    protected void initializeProtocolOptionsBox(FieldBox optionsBox, Channel[] availableChannels) {

        String ssChannelCountOption = SPIOptions.getSSChannelCountOption();

        optionsBox.addField(SPIOptions.getClockPhaseOption(), SPIOptions.ClockPhase.values());
        optionsBox.addField(SPIOptions.getClockPolarityOption(), SPIOptions.ClockPolarity.values());
        optionsBox.addField(SPIOptions.getBitOrderOption(), SPIOptions.BitOrder.values());
        optionsBox.addField(SPIOptions.getBitsPerPacketOption(), BITS_PER_PACKET_VALUES);
        optionsBox.addField(ssChannelCountOption,
                IntStream.rangeClosed(1, availableChannels.length - MIN_N_CHANNELS + 1).boxed().toArray());

        optionsBox.addActionListener(ssChannelCountOption, actionEvent -> {

            Integer numberOfSSChannels = (Integer) optionsBox.getSelectedValue(ssChannelCountOption);

            if(numberOfSSChannels == null){
                return;
            }

            FieldBox bitChannelBox = this.getChannelSelectorBox().getBox(SLAVE_SELECT_CHANNEL).clearAllFields();

            for(int i = 0; i < numberOfSSChannels; i++){

                bitChannelBox.addField(SPI.getSlaveSelectChannelName(i), availableChannels);
            }
        });

        optionsBox.setSelectedValue(ssChannelCountOption, 1);
    }

    @Override
    protected void initializeChannelSelectorBox(FieldBox channelSelectorBox, Channel[] availableChannels) {

        channelSelectorBox.addBox(FIXED_CHANNELS)
                .addField(SPI.getClockChannelName(), availableChannels)
                .addField(SPI.getMOSIChannelName(), availableChannels)
                .addField(SPI.getMISOChannelName(), availableChannels);

        channelSelectorBox.addBox(SLAVE_SELECT_CHANNEL);
    }
}
