package decoder.panel;

import boxes.FieldBox;
import channel.Channel;
import protocols.options.I2COptions;
import protocols.protocol.I2C;
import protocols.protocol.ProtocolType;

public class I2CPanel extends ProtocolPanel{

    private static final int MIN_N_CHANNELS = 2;
    private static final String FIXED_CHANNELS = "Fixed Channel(s)";

    public I2CPanel(Channel[] availableChannels) {
        super(availableChannels);
    }

    @Override
    protected ProtocolType getProtocolType() {return ProtocolType.I2C;}

    @Override
    protected int getMinimumChannelCount() {
        return MIN_N_CHANNELS;
    }

    @Override
    protected void initializeProtocolOptionsBox(FieldBox optionsBox, Channel[] availableChannels) {
        optionsBox.addField(I2COptions.getAddressBitCountOption(), I2COptions.getAddressBitOptionValues());
    }

    @Override
    protected void initializeChannelSelectorBox(FieldBox channelSelectorBox, Channel[] availableChannels) {

        channelSelectorBox.addBox(FIXED_CHANNELS)
                .addField(I2C.getClockChannelName(), availableChannels)
                .addField(I2C.getDataChannelName(), availableChannels);
    }
}
