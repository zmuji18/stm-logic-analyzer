package decoder;

import boxes.ConfirmationBox;
import channel.Channel;
import data.DataValidator;
import data.SampleDataModel;
import logging.ConfiguredLogger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import styling.Styler;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;

import static styling.GUIConstants.ADDITIONAL_OPTIONS_PANEL_HEIGHT;
import static styling.GUIConstants.ADDITIONAL_OPTIONS_PANEL_WIDTH;

public class DecoderViewPane extends JScrollPane {

    private static final int MAX_N_DECODERS = 20;

    private static final Logger logger = ConfiguredLogger.getLogger("DecoderViewPane");


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a decoder pane from the specified channel supplier.
     *
     * @param availableChannelsSupplier A {@link Supplier} to retrieve information on the currently available channels.
     * @param snapshotSupplier  A {@link Supplier} to retrieve the sample information on the current channels.
     */
    public DecoderViewPane(Supplier<List<Channel>> availableChannelsSupplier,
                           Supplier<SampleDataModel.Snapshot> snapshotSupplier){

        super();

        this.mainPanel = new JPanel();
        this.decoders = new LinkedHashMap<>();

        this.availableChannelsSupplier = availableChannelsSupplier;
        this.snapshotSupplier = snapshotSupplier;

        this.setPanelLayout();
        this.setPanelBorder();

        this.setMaximumSize(new Dimension(ADDITIONAL_OPTIONS_PANEL_WIDTH, ADDITIONAL_OPTIONS_PANEL_HEIGHT));
        this.refreshDecoderPane();
    }

    /**
     * Updates channel and sample information for all active decoders and update the opened decoder options frame
     * (if any) with the currently available channels.
     */
    public synchronized void refreshDecoders() {

        List<Channel> availableChannels = this.availableChannelsSupplier.get();
        List<Integer> availableChannelIndices = availableChannels.stream().map(Channel::getChannelIndex).toList();

        SampleDataModel.Snapshot snapshot = this.snapshotSupplier.get();
        List<Integer> selectedChannelIndices = (snapshot == null) ? List.of() : snapshot.getChannelIndices();

        DecoderOptionsFrame.updateAvailableChannels(availableChannels);

        for (DecoderPanel decoderPanel : this.decoders.values()) {

            List<Integer> usedChannelIndices = decoderPanel.getDecoder().getUsedChannelIndices();

            if (!availableChannelIndices.containsAll(usedChannelIndices)) {
                decoderPanel.setStatus(DecoderPanel.DecoderStatus.CHANNEL_MISSING);
            } else if (!selectedChannelIndices.containsAll(usedChannelIndices)) {
                decoderPanel.setStatus(DecoderPanel.DecoderStatus.EXPIRED);
            } else if (decoderPanel.getStatus() != DecoderPanel.DecoderStatus.DISABLED) {
                decoderPanel.setStatus(DecoderPanel.DecoderStatus.ACTIVE);
            }

            decoderPanel.updateSnapshot(snapshot);
        }
    }

    /**
     * @return All {@link Decoder}s present on this {@link DecoderViewPane} as a {@link List}.
     */
    public List<Decoder> getUsedDecoders() {return this.decoders.keySet().stream().toList();}

    /**
     * @return A {@link List} of the names of all decoders present in this {@link DecoderViewPane}.
     */
    public List<String> getUsedDecoderNames() {return this.decoders.values().stream().map(DecoderPanel::getName).toList();}

    /**
     * @param decoder The decoder as a {@link Decoder}.
     *
     * @return The name of the {@link DecoderPanel} containing the specified decoder as a {@link String}.
     */
    public @NotNull String getDecoderName(Decoder decoder) {

        assert this.decoders.containsKey(decoder): "The specified decoder does not exist or was already removed.";

        return this.decoders.get(decoder).getName();
    }

    /**
     * Returns the current status of the specified decoder.
     *
     * @param decoder The decoder as a {@link Decoder}.
     *
     * @return The status of the decoder as a {@link DecoderPanel.DecoderStatus}.
     */
    public DecoderPanel.DecoderStatus getDecoderStatus(Decoder decoder) {

        assert this.decoders.containsKey(decoder): "The specified decoder does not exist or was already removed.";

        return this.decoders.get(decoder).getStatus();
    }

    /**
     * Disabled/Enables this {@link DecoderViewPane} and all decoders that it contains.
     *
     * @param enabled If true this component will be enabled, otherwise it will be disabled.
     */
    @Override
    public void setEnabled(boolean enabled) {

        super.setEnabled(enabled);

        DecoderPanel.DecoderStatus newStatus =
                enabled ? DecoderPanel.DecoderStatus.ACTIVE : DecoderPanel.DecoderStatus.DISABLED;

        this.decoders.values().stream()
                .filter(decoderPanel -> decoderPanel.getStatus() == DecoderPanel.DecoderStatus.ACTIVE ||
                        decoderPanel.getStatus() == DecoderPanel.DecoderStatus.DISABLED)
                .forEach(decoderPanel -> decoderPanel.setStatus(newStatus));

        this.newDecoderButton.setEnabled(enabled);
    }


    /** === Private Methods -------------------------------------------------- **/

    /**
     * @param decoder The decoder as a {@link Decoder}
     *
     * @return The {@link DecoderPanel} associated with the specified decoder, or null if one doesn't exist.
     */
    private @Nullable DecoderPanel getDecoderPanel(Decoder decoder) {
        return this.decoders.get(decoder);
    }

    /**
     * Creates a {@link JPanel} with a button used for creating a new {@link Decoder}.
     */
    private void createNewDecoderButton() {

        this.newDecoderButton = new JButton("New Decoder");
        this.newDecoderButton.addActionListener(actionEvent -> {
            Executors.newSingleThreadExecutor().submit(this::addDecoder);
        });

        this.newDecoderButton.setAlignmentX(CENTER_ALIGNMENT);

        Styler.style(this.newDecoderButton);
    }

    /**
     * Adds the "New Decoder" button to this {@link DecoderViewPane}.
     */
    private void addNewDecoderButton() {

        if(this.newDecoderButton == null) this.createNewDecoderButton();

        Styler.addSeparator(this.mainPanel, JSeparator.HORIZONTAL, false);
        this.mainPanel.add(this.newDecoderButton);
    }

    /**
     * Prompts the user with a new Decoder Options frame and adds a new {@link Decoder} to this {@link DecoderViewPane}
     * if the user chooses a non-null response (i.e. doesn't cancel or close the window).
     */
    private void addDecoder() {

        DecoderInfo newDecoderInfo =
                DecoderOptionsFrame.openDecoderOptions(this.availableChannelsSupplier.get(), this.decoderNameValidator);

        if(newDecoderInfo == null) return;

        assert !this.getUsedDecoderNames().contains(newDecoderInfo.decoderName()) :
                "A Decoder with the name " + newDecoderInfo.decoderName() + " already exists.";

        synchronized (this.decoders) {

            DecoderPanel newDecoderPanel =
                    DecoderPanel.from(newDecoderInfo.decoder(), newDecoderInfo.decoderName(), this.snapshotSupplier.get());

            newDecoderPanel.addRemoveButtonListener(actionEvent -> Executors.newSingleThreadExecutor().submit(() -> {
                this.removeDecoder(newDecoderPanel.getDecoder());
            }));

            this.decoders.put(newDecoderInfo.decoder(), newDecoderPanel);
        }

        this.refreshDecoders();
        this.refreshDecoderPane();
    }

    /**
     * Removes the decoder with the specified name from this {@link DecoderViewPane}.
     *
     * @param decoder The decoder to be removed.
     */
    private void removeDecoder(Decoder decoder) {

        DecoderPanel panel = this.getDecoderPanel(decoder);

        if(panel == null) {
            logger.log(Level.WARNING, "Attempted to remove a non-existing decoder.");
            return;
        }

        if(!ConfirmationBox.confirm("Do you want to remove the decoder \"" + panel.getName() + "\"?", this)) {
            return;
        }

        synchronized (this.decoders) {
            this.decoders.remove(decoder);
        }

        this.refreshDecoderPane();
    }

    /**
     * Sets the layout for this {@link DecoderViewPane}.
     */
    private void setPanelLayout() {

        this.setViewportView(this.mainPanel);
        this.mainPanel.setLayout(new BoxLayout(this.mainPanel, BoxLayout.Y_AXIS));
    }

    /**
     * Sets the border for this {@link DecoderViewPane}.
     */
    private void setPanelBorder() {

        this.setBorder(new TitledBorder("Bus Analyzers"));
        this.setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_ALWAYS);
        this.setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_ALWAYS);
    }

    /**
     * Refreshes this {@link DecoderViewPane} with the latest decoder information.
     */
    private void refreshDecoderPane() {

        this.mainPanel.removeAll();

        synchronized (this.decoders) {

            for (DecoderPanel decoderPanel : this.decoders.values()) {
                this.mainPanel.add(decoderPanel);
            }

            if(this.decoders.size() < MAX_N_DECODERS) {
                this.addNewDecoderButton();
            }
        }

        SwingUtilities.invokeLater(() -> {
            this.repaint();
            this.revalidate();
        });
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final Supplier<List<Channel>> availableChannelsSupplier;
    private final Supplier<SampleDataModel.Snapshot> snapshotSupplier;

    private final JPanel mainPanel;
    private JButton newDecoderButton;

    private final Map<Decoder, DecoderPanel> decoders;

    private final DataValidator decoderNameValidator = data -> {

        String decoderName = data.trim();

        if(decoderName.equals("")) return "A decoder name must be specified.";
        else if(this.getUsedDecoderNames().contains(decoderName)) {
            return "A decoder with the name " + decoderName + " already exists.";
        }
        else return null;
    };
}
