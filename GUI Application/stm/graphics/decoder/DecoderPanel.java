package decoder;

import boxes.NotificationBox;
import buttons.Button;
import channel.Channel;
import data.SampleDataModel;
import styling.Styler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static styling.GUIConstants.*;

public class DecoderPanel extends Box {

    private static final int DECODER_PANEL_WIDTH = ADDITIONAL_OPTIONS_PANEL_WIDTH;
    private static final int DECODER_PANEL_HEIGHT = 140;

    private static final Color ACTIVE_COLOR = new Color(6, 182, 23, 218);
    private static final Color EXPIRED_COLOR = new Color(52, 231, 184, 218);
    private static final Color CHANNEL_MISSING_COLOR = new Color(239, 3, 3, 218);
    private static final Color DISABLED_COLOR = new Color(128, 126, 126, 218);

    /** === Enum Definitions ------------------------------------------------- **/

    public enum DecoderStatus {
        ACTIVE("Active", ACTIVE_COLOR),
        EXPIRED("Samples Expired", EXPIRED_COLOR),
        CHANNEL_MISSING("Channels Missing", CHANNEL_MISSING_COLOR),
        DISABLED("Disabled", DISABLED_COLOR);

        DecoderStatus(String value, Color color) {
            this.value = value;
            this.color = color;
        }

        private final String value;
        private final Color color;
    }


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a simple {@link JPanel} for the specified decoder.
     *
     * @param decoder The {@link Decoder} to be associated with this decoder panel.
     * @param decoderName The name of the decoder as a {@link String}.
     * @param snapshot  A {@link data.SampleDataModel.Snapshot} to retrieve the sample information on the current channels.
     *
     * @return The newly created decoder panel as a {@link DecoderPanel}
     */
    public static DecoderPanel from(Decoder decoder,
                                    String decoderName,
                                    SampleDataModel.Snapshot snapshot) {

        return new DecoderPanel(decoder, decoderName, snapshot);
    }

    /**
     * @return The current status of this decoder as a {@link DecoderStatus}.
     */
    public DecoderStatus getStatus() {return this.status;}

    /**
     * @return The name of this {@link DecoderPanel} as a {@link String}.
     */
    public String getName() {return this.decoderName;}

    /**
     * @return The {@link Decoder} associated with this {@link DecoderPanel}.
     */
    public Decoder getDecoder() {return this.decoder;}

    /**
     * Sets the status of this decoder.
     *
     * @param status The status of the decoder as a {@link DecoderStatus}.
     */
    public void setStatus(DecoderStatus status) {

        this.status = status;
        this.statusLabel.setText(this.status.value);
        this.statusLabel.setForeground(this.status.color);

        this.decodeButton.setEnabled(this.status == DecoderStatus.ACTIVE);
        this.removeButton.setEnabled(this.status != DecoderStatus.DISABLED);

        SwingUtilities.invokeLater(() -> {
            this.repaint();
            this.revalidate();
        });
    }

    /**
     * Updates the sample data snapshot used by the {@link Decoder} in this {@link DecoderPanel}.
     *
     * @param newSnapshot A {@link data.SampleDataModel.Snapshot} to retrieve the sample information on the current channels.
     */
    public void updateSnapshot(SampleDataModel.Snapshot newSnapshot) {
        this.snapshot = newSnapshot;
    }

    /**
     * Adds the specified action listener to the remove button of this {@link DecoderPanel}.
     *
     * @param listener The action listener as an {@link ActionListener}.
     */
    public void addRemoveButtonListener(ActionListener listener) {
        this.removeButton.addActionListener(listener);
    }


    /** === Private Methods -------------------------------------------------- **/

    /**
     * Constructs a new {@link DecoderPanel} from the specified parameters.
     *
     * @param decoder The {@link Decoder} to be associated with this decoder panel.
     * @param decoderName The name of the decoder as a {@link String}.
     * @param snapshot  A {@link data.SampleDataModel.Snapshot} to retrieve the sample information on the current channels.
     */
    private DecoderPanel(Decoder decoder, String decoderName, SampleDataModel.Snapshot snapshot) {

        super(BoxLayout.Y_AXIS);

        this.decoder = decoder;
        this.decoderName = decoderName;
        this.snapshot = snapshot;
        this.status = DecoderStatus.ACTIVE;

        this.addComponents();

        this.setBorder(BorderFactory.createEtchedBorder());
        this.setMaximumSize(new Dimension(DECODER_PANEL_WIDTH, DECODER_PANEL_HEIGHT));

        this.setStatus(DecoderStatus.ACTIVE);
    }

    /**
     * Adds all necessary components to this {@link DecoderPanel}.
     */
    private void addComponents() {

        this.add(this.createHeader());
        this.add(Box.createVerticalGlue());
        this.add(this.createInfoBox());
        this.add(Box.createVerticalGlue());
        this.add(this.createDecodeButton());
        this.add(Box.createRigidArea(new Dimension(SMALL_COMPONENT_GAP, SMALL_COMPONENT_GAP)));
    }

    /**
     * @return A {@link Box} containing the name of this {@link DecoderPanel} and the close button.
     */
    private Box createHeader() {

        Box headerBox = Box.createHorizontalBox();

        headerBox.add(Box.createRigidArea(new Dimension(SMALL_COMPONENT_GAP, SMALL_COMPONENT_GAP)));
        headerBox.add(Styler.style(new JLabel(this.getWrappedDecoderName())));
        headerBox.add(Box.createHorizontalGlue());
        headerBox.add(Styler.style(this.infoButton = new Button(INFO_BUTTON_ICONS), Styler.Style.TRANSPARENT));
        headerBox.add(Styler.style(this.removeButton = new Button(CLOSE_BUTTON_ICONS), Styler.Style.TRANSPARENT));

        this.infoButton.addActionListener(actionEvent -> this.displayDecoderInfo());

        return Styler.style(headerBox);
    }

    /**
     * @return A {@link Box} containing information about this {@link DecoderPanel}.
     */
    private Box createInfoBox() {

        Box infoBox = Box.createVerticalBox();
        infoBox.setBorder(BorderFactory.createEtchedBorder());

        Box protocolBox = Box.createHorizontalBox();
        protocolBox.add(Box.createRigidArea(new Dimension(SMALL_COMPONENT_GAP, SMALL_COMPONENT_GAP)));
        protocolBox.add(Styler.style(new JLabel("Protocol:")));
        protocolBox.add(Box.createHorizontalGlue());
        protocolBox.add(Styler.style(new JLabel(this.decoder.getProtocol().getProtocol().toString())));
        protocolBox.add(Box.createRigidArea(new Dimension(SMALL_COMPONENT_GAP, SMALL_COMPONENT_GAP)));

        Box statusBox = Box.createHorizontalBox();
        statusBox.add(Box.createRigidArea(new Dimension(SMALL_COMPONENT_GAP, SMALL_COMPONENT_GAP)));
        statusBox.add(Styler.style(new JLabel("Status:")));
        statusBox.add(Box.createHorizontalGlue());
        statusBox.add(Styler.style(this.statusLabel = new JLabel(this.status.value)));
        statusBox.add(Box.createRigidArea(new Dimension(SMALL_COMPONENT_GAP, SMALL_COMPONENT_GAP)));

        infoBox.add(protocolBox);
        infoBox.add(statusBox);

        return infoBox;
    }

    /**
     * @return A {@link Box} containing the decode button.
     */
    private Box createDecodeButton() {

        Box buttonBox = Box.createHorizontalBox();

        this.decodeButton = new JButton("Decode");
        this.decodeButton.addActionListener(actionEvent -> Executors.newSingleThreadExecutor().submit(this::decode));

        buttonBox.add(Box.createHorizontalGlue());
        buttonBox.add(this.decodeButton);
        buttonBox.add(Box.createHorizontalGlue());

        return buttonBox;
    }

    /**
     * @return The HTML wrapped version of this {@link DecoderPanel}s name.
     */
    private String getWrappedDecoderName() {
        return "<html>" + this.decoderName + "</html>";
    }

    /**
     * Creates a {@link boxes.PopupBox} displaying the mapping information of this decoder.
     */
    private void displayDecoderInfo() {

        final String HEADER_STRING = "<h2 style='text-align:center;'>%s</h2>".formatted(this.getName()) +
                """
                <table style="text-align:center">
                    <tr>
                        <th>Channel Name</th>
                        <th>Channel Index</th>
                    </tr>
                    """;

        final String ROW_FORMAT = """
                <tr>
                    <td style="text-align:center;">%s</td>
                    <td style="text-align:center;">%s</td>
                </tr>
                """;

        final String TRAILER_STRING = "</table>";

        NotificationBox.display(HEADER_STRING +
                this.getDecoder().getChannelMapping().entrySet().stream()
                .map(entry -> ROW_FORMAT.formatted(entry.getKey(), Channel.getChannel(entry.getValue())))
                .collect(Collectors.joining("")) + TRAILER_STRING);
    }

    /**
     * Decodes samples from the {@link data.SampleDataModel.Snapshot} provided by this objects snapshot supplier.
     */
    private void decode() {

        assert this.getStatus() == DecoderStatus.ACTIVE: "The decoder is currently not active.";
        assert this.snapshot != null: "A non-null snapshot must be set for the decoder " + this.getName() + ".";

        if(!this.snapshot.getChannelIndices().containsAll(this.decoder.getUsedChannelIndices())) {
            throw new RuntimeException("Channels missing for the decoder \"" + this.getName() + "\"." +
                    "Required: " + this.decoder.getUsedChannelIndices() + "\n" +
                    "Available: " + this.snapshot.getChannelIndices() + ".");
        }

        DecodingFrame.open(this.decoder, this.snapshot);
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final Decoder decoder;
    private final String decoderName;

    private DecoderStatus status;
    private SampleDataModel.Snapshot snapshot;

    private Button infoButton;
    private Button removeButton;
    private JLabel statusLabel;
    private JButton decodeButton;
}
