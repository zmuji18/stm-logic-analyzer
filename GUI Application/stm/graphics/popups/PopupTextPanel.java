package popups;

import buttons.Button;
import buttons.LinkedButtonPanel;
import org.jetbrains.annotations.Nullable;
import styling.Styler;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

import static styling.GUIConstants.*;

public class PopupTextPanel extends JPanel {

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Enumerator to hold the various types of pop-up styles available.
     */
    public enum PopupType {
        INFO,
        WARNING,
        ERROR
    }

    public enum PopupLocation {
        LEFT,
        ABOVE,
        RIGHT,
        BELOW
    }

    /**
     * Creates and displays a pop-up with the specified text. The pop-up is linked to the given container. Any active
     * pop-up for the linked component will be automatically removed if a new one is created.
     *
     * @param parent The {@link JComponent} the pop-up is linked to.
     * @param text The text displayed on the pop-up.
     * @param type The type of the pop-up as a {@link PopupType} object. The style of the pop-up will vary based on type.
     * @param x The X coordinate of the top-left corner of the pop-up.
     * @param y The Y coordinate of the top-left corner of the pop-up.
     */
    public static void displayPopup(JComponent parent, String text, PopupType type, int x, int y) {
        displayPopup(parent, text, type, x, y, false, false, null);
    }

    /**
     * Creates and displays a pop-up with the specified text. The pop-up is linked to the given container. Any active
     * pop-up for the linked component will be automatically removed if a new one is created.
     *
     * @param parent The {@link JComponent} the pop-up is linked to.
     * @param text The text displayed on the pop-up.
     * @param type The type of the pop-up as a {@link PopupType} object. The style of the pop-up will vary based on type.
     * @param location The relative location of the pop-up as a {@link PopupLocation} enum.
     * @param highlightParent If true the parent component will be highlighted.
     */
    public static void displayPopup(JComponent parent, String text, PopupType type, PopupLocation location, boolean highlightParent) {

        Point parentLocation = parent.getLocationOnScreen();
        Point popupPoint;

        switch (location) {
            case LEFT -> popupPoint = new Point(parentLocation.x - POPUP_PANEL_WIDTH, parentLocation.y);
            case ABOVE -> popupPoint = new Point(parentLocation.x, parentLocation.y - POPUP_PANEL_HEIGHT);
            case RIGHT -> popupPoint = new Point(parentLocation.x + parent.getWidth(), parentLocation.y);
            case BELOW -> popupPoint = new Point(parentLocation.x, parentLocation.y + parent.getHeight());
            default -> throw new RuntimeException(location + " is not a valid pop-up location.");
        }

        displayPopup(parent, text, type, popupPoint.x, popupPoint.y, highlightParent, false, null);
    }

    /**
     * Creates and displays a pop-up with the specified text. The pop-up is linked to the given container. Any active
     * pop-up for the linked component will be automatically removed if a new one is created.
     *
     * @param parent The {@link JComponent} the pop-up is linked to.
     * @param text The text displayed on the pop-up.
     * @param type The type of the pop-up as a {@link PopupType} object. The style of the pop-up will vary based on type.
     * @param x The X coordinate of the top-left corner of the pop-up.
     * @param y The Y coordinate of the top-left corner of the pop-up.
     * @param highlightParent If true the parent component will be highlighted.
     */
    public static void displayPopup(JComponent parent, String text, PopupType type, int x, int y, boolean highlightParent) {
        displayPopup(parent, text, type, x, y, highlightParent, false, null);
    }

    /**
     * Creates and displays a pop-up with the specified text. The pop-up is linked to the given container. Any active
     * pop-up for the linked component will be automatically removed if a new one is created.
     *
     * @param parent The {@link JComponent} the pop-up is linked to.
     * @param text The text displayed on the pop-up.
     * @param type The type of the pop-up as a {@link PopupType} object. The style of the pop-up will vary based on type.
     * @param x The X coordinate of the top-left corner of the pop-up.
     * @param y The Y coordinate of the top-left corner of the pop-up.
     * @param highlightParent If true the parent component will be highlighted.
     * @param ignoreViewPort If true the visibility of the component will not be checked.
     * @param callback A routine to be executed when the pop-up is removed from the screen as a {@link Runnable}.
     */
    public static void displayPopup(JComponent parent, String text, PopupType type, int x, int y,
                                    boolean highlightParent, boolean ignoreViewPort, @Nullable Runnable callback) {

        synchronized (parentTimers) {
            if (parentTimers.containsKey(parent)) {
                parentTimers.get(parent).stop();
                parentTimers.remove(parent);
            }
        }

        synchronized (parentCallbacks) {

            if(parentCallbacks.containsKey(parent) && parentCallbacks.get(parent) != null) {
                parentCallbacks.get(parent).run();
            }
            parentCallbacks.put(parent, callback);
        }

        PopupTextPanel popupText = new PopupTextPanel(parent, text, type);
        Popup newPopup = PopupFactory.getSharedInstance().getPopup(parent, popupText, x, y);

        synchronized (popupMap) {

            if (popupMap.containsKey(parent)) {
                popupMap.get(parent).hide();
            }

            revertBorder(parent);

            if(ignoreViewPort || inViewPort(parent)) {
                popupMap.put(parent, newPopup);
                newPopup.show();
            }

            if(highlightParent) {
                originalBorders.put(parent, parent.getBorder());
                addHighlightBorder(parent, type);
            }
        }
    }

    /**
     * Removes any active pop-ups linked to the specified parent
     *
     * @param parent The {@link JComponent} the pop-up is linked to.
     */
    public static void removePopup(JComponent parent) {

        synchronized (popupMap) {

            if (popupMap.containsKey(parent)) {
                popupMap.get(parent).hide();
                popupMap.remove(parent);
            }

            if(originalBorders.containsKey(parent)) {
                parent.setBorder(originalBorders.get(parent));
                originalBorders.remove(parent);
            }
        }

        synchronized (parentTimers) {
            if (parentTimers.containsKey(parent)) {
                parentTimers.get(parent).stop();
                parentTimers.remove(parent);
            }
        }

        synchronized (parentCallbacks) {
            if(parentCallbacks.containsKey(parent) && parentCallbacks.get(parent) != null) {
                parentCallbacks.get(parent).run();
            }
            parentCallbacks.remove(parent);
        }
    }


    /** === Private Methods -------------------------------------------------- **/

    /**
     * Creates a new {@link PopupTextPanel} object with the specified text and styling.
     *
     * @param parent The {@link Component} the pop-up is linked to.
     * @param text The text to be displayed on the panel.
     * @param type The type of the pop-up box. The style of the pop-up will vary based on this value.
     */
    private PopupTextPanel(JComponent parent, String text, PopupType type) {

        super();

        this.setStyle();
        this.addHeader(parent, type);
        this.setTimer(parent, type);
        this.addRootPaneListeners(parent);
        this.displayText(text);

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent e) {
                if(!PopupTextPanel.this.getVisibleRect().contains(e.getPoint())) {
                    removePopup(parent);
                }
            }
        });
    }

    /**
     * Sets the panel style for the {@link PopupTextPanel}.
     */
    private void setStyle() {

        Styler.style(this, Styler.Style.POP_UP_TEXT);

        this.setLayout(new BorderLayout());
        this.setBorder(new CompoundBorder(this.getBorder(), new EmptyBorder(POP_UP_PANEL_MARGINS)));

        this.setOpaque(false);

        this.setMinimumSize(new Dimension(POPUP_PANEL_WIDTH, POPUP_PANEL_HEIGHT));
    }

    /**
     * Adds a header and a small close button to the top of the pop-up.
     *
     * @param parent The {@link Component} the pop-up is linked to.
     * @param type The type of the pop-up box. The style of the pop-up will vary based on this value.
     */
    private void addHeader(JComponent parent, PopupType type) {

        JPanel headerPanel = new JPanel();
        headerPanel.setLayout(new BoxLayout(headerPanel, BoxLayout.X_AXIS));

        Button closeButton = new Button(CLOSE_BUTTON_ICONS);
        closeButton.addActionListener(actionEvent -> removePopup(parent));
        closeButton.setFocusable(false);

        String htmlColorStyle;
        String headerText;
        switch (type) {
            case INFO -> {
                htmlColorStyle = "color:rgb(54,168,255)";
                headerText = "Information";
            }
            case WARNING -> {
                htmlColorStyle = "color:yellow";
                headerText = "Warning";
            }
            case ERROR -> {
                htmlColorStyle = "color:red";
                headerText = "Error";
            }
            default -> throw new RuntimeException(type + " is not a valid pop-up type.");
        }

        headerPanel.add(Styler.style(new JLabel("<html><h2 style=\"" + htmlColorStyle + "\">" + headerText + "</h2></html>"), Styler.Style.POP_UP));
        headerPanel.add(Box.createHorizontalGlue());
        headerPanel.add(Styler.style(closeButton, Styler.Style.POP_UP_TEXT));

        this.add(headerPanel, BorderLayout.PAGE_START);
    }

    /**
     * Sets an auto-close timer for this {@link PopupTextPanel}.
     *
     * @param parent The {@link Component} the pop-up is linked to.
     * @param type The type of the pop-up. Different types will have a different delay.
     */
    private void setTimer(JComponent parent, PopupType type) {

        int delay;

        switch (type) {
            case INFO -> delay = INFO_POPUP_DELAY;
            case WARNING -> delay = WARNING_POPUP_DELAY;
            case ERROR -> delay = ERROR_POPUP_DELAY;
            default -> throw new RuntimeException(type + " is not a valid pop-up type.");
        }

        Timer autoCloseTimer = new Timer(delay, actionEvent -> removePopup(parent));

        synchronized (parentTimers) {

            autoCloseTimer.setRepeats(false);
            autoCloseTimer.start();

            parentTimers.put(parent, autoCloseTimer);
        }
    }

    /**
     * Adds listeners to the root pane of this {@link LinkedButtonPanel}.
     */
    private void addRootPaneListeners(JComponent parent) {

        Component root = SwingUtilities.getRoot(parent);

        synchronized (registeredWindows) {

            if (root != null && !registeredWindows.contains(root)) {

                JFrame rootFrame = (JFrame) SwingUtilities.getRoot(parent);

                rootFrame.addWindowListener(popUpWindowAdapterSupplier.apply(parent));
                rootFrame.addComponentListener(popUpComponentAdapterSupplier.apply(parent));

                registeredWindows.add(parent);
            }
        }
    }

    /**
     * Displays the specified text on this pop-up panel.
     *
     * @param text The text to be displayed on the panel.
     */
    private void displayText(String text) {

        this.add(Styler.style(new JLabel("<html><p>" + text + "</p></html>"), Styler.Style.POP_UP), BorderLayout.CENTER);
    }

    /**
     * Adds a highlight border to the specified {@link JComponent} for the given pop-up type.
     *
     * @param parent The parent component to which the border should be added.
     * @param type The type of the pop-up box. The style of the border will vary based on this value.
     */
    private static void addHighlightBorder(JComponent parent, PopupType type) {

        Color highlightBorderColor;

        switch (type) {
            case INFO -> highlightBorderColor = INFO_BORDER_COLOR;
            case WARNING -> highlightBorderColor = WARNING_BORDER_COLOR;
            case ERROR -> highlightBorderColor = ERROR_BORDER_COLOR;
            default -> throw new RuntimeException(type + " is not a valid pop-up type.");
        }

        Border highlightBorder = BorderFactory.createLineBorder(highlightBorderColor, POP_UP_BORDER_WIDTH, true);
        Border parentBorder = parent.getBorder();

        if(parentBorder != null) {

            Insets originalInsets = parentBorder.getBorderInsets(parent);
            Border fillerBorder = BorderFactory.createEmptyBorder(
                    originalInsets.top - POP_UP_BORDER_WIDTH,
                    originalInsets.left - POP_UP_BORDER_WIDTH,
                    originalInsets.bottom - POP_UP_BORDER_WIDTH,
                    originalInsets.right - POP_UP_BORDER_WIDTH
            );

            highlightBorder = new CompoundBorder(highlightBorder, fillerBorder);
        }

        parent.setBorder(highlightBorder);

        SwingUtilities.invokeLater(() -> {

            parent.repaint();
            parent.revalidate();
        });
    }

    /**
     * Reverts the highlighted border for the specified parent component.
     *
     * @param parent The parent component for which the border should be reverted to the original.
     */
    private static void revertBorder(JComponent parent) {

        if(originalBorders.containsKey(parent)) {
            parent.setBorder(originalBorders.get(parent));
            originalBorders.remove(parent);
        }

        SwingUtilities.invokeLater(() -> {

            parent.repaint();
            parent.revalidate();
        });
    }

    /**
     * Checks if the specified component is at least partially in the viewport of a {@link JScrollPane}.
     *
     * @param component The component to be checked.
     * @return True if the component is not in a {@link JScrollPane} or the component is inside the viewport of all
     *         of it's parent panes.
     */
    private static boolean inViewPort(JComponent component) {

        Container parent = component.getParent();

        while(parent != null) {

            if(parent instanceof JScrollPane) {
                if(!((JScrollPane) parent).getViewport().getVisibleRect().intersects(component.getVisibleRect())) return false;
            }

            parent = parent.getParent();
        }

        return true;
    }


    /** === Static Variables ------------------------------------------------- **/

    private static final Map<JComponent, Popup> popupMap;
    private static final Map<JComponent, Border> originalBorders;
    private static final Map<JComponent, Timer> parentTimers;
    private static final Map<JComponent, Runnable> parentCallbacks;
    private static final Set<Component> registeredWindows;

    static {
        popupMap = new HashMap<>();
        originalBorders = new HashMap<>();
        parentTimers = new HashMap<>();
        parentCallbacks = new HashMap<>();
        registeredWindows = new HashSet<>();
    }

    private static final Function<JComponent, WindowAdapter> popUpWindowAdapterSupplier = component -> new WindowAdapter() {
        @Override
        public void windowClosed(WindowEvent e) {PopupTextPanel.removePopup(component);}

        @Override
        public void windowIconified(WindowEvent e) {PopupTextPanel.removePopup(component);}

        @Override
        public void windowLostFocus(WindowEvent e) {PopupTextPanel.removePopup(component);}

        @Override
        public void windowDeactivated(WindowEvent e) {PopupTextPanel.removePopup(component);}
    };

    private static final Function<JComponent, ComponentAdapter> popUpComponentAdapterSupplier = component -> new ComponentAdapter() {
        @Override
        public void componentResized(ComponentEvent e) {PopupTextPanel.removePopup(component);}

        @Override
        public void componentMoved(ComponentEvent e) {PopupTextPanel.removePopup(component);}

        @Override
        public void componentShown(ComponentEvent e) {PopupTextPanel.removePopup(component);}

        @Override
        public void componentHidden(ComponentEvent e) {PopupTextPanel.removePopup(component);}
    };
}
