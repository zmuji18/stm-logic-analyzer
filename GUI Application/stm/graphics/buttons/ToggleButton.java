package buttons;

import logging.ConfiguredLogger;

import java.util.logging.Logger;

/**
 * Class: ToggleButton
 * Date Created: 06.07.21 16:14
 *
 * === Description ============================================= *
 * This class represents a Toggleable {@link Button} with distinct Icons for every state.
 */
public class ToggleButton extends Button {

    private static final Logger logger = ConfiguredLogger.getLogger("ToggleButton");


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a toggleable button from the specified icon collections.
     *
     * @param toggledOffIcons The collection of icons to be used when the button is "off" as a {@link ButtonIconRecord}.
     * @param toggledOnIcons The collection of icons to be used when the button is "on" as a {@link ButtonIconRecord}.
     */
    public ToggleButton(ButtonIconRecord toggledOffIcons, ButtonIconRecord toggledOnIcons) {

        super(toggledOffIcons);

        this.turnedOffIcons = toggledOffIcons;
        this.turnedOnIcons = toggledOnIcons;
        this.toggled = false;

        this.addActionListener(actionEvent -> {

            if(!ToggleButton.this.isEnabled()) return;
            ToggleButton.this.toggle();
        });

        this.setFocusable(false);
    }

    /**
     * @return True if this {@link ToggleButton} is toggled on.
     */
    public boolean isToggled() {return this.toggled;}

    /**
     * Changes the state of the {@link ToggleButton}.
     */
    public void toggle() {

        this.toggled = !this.toggled;

        ButtonIconRecord icons = this.toggled ? this.turnedOnIcons : this.turnedOffIcons;

        super.setIcons(icons);
    }

    /**
     * Sets this button to be in the specified state.
     *
     * @param toggled If true the button will be toggled on, otherwise it will be toggled off.
     */
    public void setToggled(boolean toggled) {
        if(this.toggled ^ toggled) this.toggle();
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final ButtonIconRecord turnedOffIcons;
    private final ButtonIconRecord turnedOnIcons;
    private boolean toggled;
}
