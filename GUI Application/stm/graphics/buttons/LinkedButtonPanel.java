package buttons;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;

import static styling.GUIConstants.START_BUTTON_ICONS;
import static styling.GUIConstants.STOP_BUTTON_ICONS;

/**
 * Class: LinkedButtonPanel
 * Date Created: 08.07.21 17:22
 *
 * === Description ============================================= *
 * This class represents a {@link JPanel} of interlinked buttons from which only
 * one can be on at a time.
 */
public class LinkedButtonPanel extends JPanel {

    public static final int X_AXIS = BoxLayout.X_AXIS;
    public static final int Y_AXIS = BoxLayout.Y_AXIS;
 
 
    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a panel with interlinked buttons, from which only one can be toggled on at a time.
     *
     * @param buttons The array of {@link ToggleButton} objects.
     * @param axis The axis along which these buttons should be aligned.
     */
    public LinkedButtonPanel(ToggleButton[] buttons, int axis) {this(buttons, axis, 0);}

    /**
     * Creates a panel with interlinked buttons, from which only one can be toggled on at a time.
     *
     * @param buttons The array of {@link ToggleButton} objects.
     * @param axis The axis along which these buttons should be aligned.
     * @param componentGap The gap between the buttons
     */
    public LinkedButtonPanel(ToggleButton[] buttons, int axis, int componentGap) {

        super();
        this.setLayout(new BoxLayout(this, axis));

        this.buttons = new ToggleButton[buttons.length];
        System.arraycopy(buttons, 0, this.buttons, 0, buttons.length);

        this.addButtons(axis, componentGap);
        this.setEnabled(true);
    }

    /**
     * Enables/Disables this linked button panel.
     *
     * @param isEnabled If true the button panel will be enabled, otherwise it will be disabled.
     */
    @Override
    public void setEnabled(boolean isEnabled) {

        super.setEnabled(isEnabled);

        this.isEnabled = isEnabled;
        for(ToggleButton button: this.buttons) button.setEnabled(isEnabled);
    }

    /**
     * @return All buttons contained in this {@link LinkedButtonPanel} as a {@link List}.
     */
    public List<ToggleButton> getButtons() {return List.of(this.buttons);}


    /** === Main ------------------------------------------------------------- **/

    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ignored) {}

        JFrame frame = new JFrame("Test");

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setVisible(true);

        ToggleButton[] test_buttons = new ToggleButton[4];

        for(int i = 0; i < 4; i++) test_buttons[i] = new ToggleButton(START_BUTTON_ICONS, STOP_BUTTON_ICONS);

        frame.add(new LinkedButtonPanel(test_buttons, LinkedButtonPanel.X_AXIS));
    }


    /** === Private Functions ------------------------------------------------ **/

    /**
     * Adds all linked buttons to this {@link LinkedButtonPanel}.
     */
    private void addButtons(int axis, int componentGap) {

        ActionListener linkingListener = actionEvent -> {

            if(!LinkedButtonPanel.this.isEnabled) return;

            for(ToggleButton button : LinkedButtonPanel.this.buttons) {
                if(button != actionEvent.getSource()) button.setToggled(false);
            }
        };

        for(int i = 0; i < this.buttons.length; i++) {

            ToggleButton button = this.buttons[i];

            button.addActionListener(linkingListener);
            this.add(button);

            if(i != this.buttons.length - 1) {
                this.add(Box.createRigidArea(axis == X_AXIS ?
                        new Dimension(componentGap, 0) : new Dimension(0, componentGap)));
            }
        }
    }


    /** === Instance Variables ----------------------------------------------- **/

    private boolean isEnabled;
    private final ToggleButton[] buttons;
}
