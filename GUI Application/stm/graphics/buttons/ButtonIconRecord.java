package buttons;

import javax.swing.*;
import java.util.Objects;

public record ButtonIconRecord(Icon defaultIcon, Icon rolloverIcon, Icon pressedIcon, Icon disabledIcon) {

    /**
     * Creates a new {@link ButtonIconRecord} used for initializing buttons with customized icons.
     *
     * @param defaultIcon The icon that is visible when there's no interaction with the button.
     * @param rolloverIcon The icon that is visible when the mouse is hovering over the button.
     * @param pressedIcon The icon that is visible when the mouse is clicked on the button.
     * @param disabledIcon The icon that is visible when the button is disabled.
     */
    public ButtonIconRecord {
        Objects.requireNonNull(defaultIcon, "The default icon in a ButtonIconRecord cannot be null.");
        Objects.requireNonNull(rolloverIcon, "The rollover icon in a ButtonIconRecord cannot be null.");
        Objects.requireNonNull(pressedIcon, "The pressed icon in a ButtonIconRecord cannot be null.");
        Objects.requireNonNull(disabledIcon, "The disabled icon in a ButtonIconRecord cannot be null.");
    }
}
