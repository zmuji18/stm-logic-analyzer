package styling;

import buttons.ButtonIconRecord;
import image.IconLoader;

import java.awt.*;
import java.io.File;
import java.io.IOException;

public final class GUIConstants {

    private static final String ASSETS_DIR = "stm/Assets";

    /** === Size Constants -------------------------------------------------------- **/

    public static final int SCREEN_WIDTH = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();
    public static final int SCREEN_HEIGHT = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();

    public static final int GENERIC_TEXT_FIELD_COLUMNS = 10;
    public static final int GENERIC_TEXT_FIELD_WIDTH = 100;
    public static final int GENERIC_TEXT_FIELD_HEIGHT = 35;

    public static final int SMALL_COMBO_BOX_WIDTH = 45;
    public static final int GENERIC_COMBO_BOX_WIDTH = 90;
    public static final int LARGE_COMBO_BOX_WIDTH = 160;
    public static final int GENERIC_COMBO_BOX_HEIGHT = 35;

    public static final int GENERIC_BUTTON_WIDTH = 70;
    public static final int GENERIC_BUTTON_HEIGHT = 35;

    public static final int SAMPLING_RATE_FIELD_WIDTH = 130;
    public static final int SAMPLING_DURATION_FIELD_WIDTH = 110;
    public static final int MEMORY_FIELD_WIDTH = 110;
    public static final int LOGIC_LEVEL_FIELD_WIDTH = 110;
    public static final int CONTROL_PANEL_TEXT_FIELD_HEIGHT = 40;

    public static final int CHANNEL_TOOL_BAR_WIDTH = 35;

    public static final int ADDITIONAL_OPTIONS_PANEL_WIDTH = 300;
    public static final int ADDITIONAL_OPTIONS_PANEL_HEIGHT = 3000;

    public static final int ERROR_PANEL_WIDTH = 250;
    public static final int ERROR_PANEL_HEIGHT = 300;

    public static final int POPUP_PANEL_WIDTH = 150;
    public static final int POPUP_PANEL_HEIGHT = 50;

    public static final int POP_UP_BORDER_WIDTH = 2;
    public static final int GENERIC_BORDER_WIDTH = 2;

    public static final int CONFIRMATION_POPUP_WIDTH = 250;
    public static final int CONFIRMATION_POPUP_HEIGHT = 200;

    public static final int FORM_POPUP_WIDTH = 250;
    public static final int FORM_POPUP_HEIGHT = 200;

    public static final int SMALL_COMPONENT_GAP = 10;
    public static final int MEDIUM_COMPONENT_GAP = 25;
    public static final int GENERIC_COMPONENT_GAP = 40;
    public static final int LARGE_COMPONENT_GAP = 60;

    public static final int CHANNEL_HEADER_GAP = 25;
    public static final int CHANNEL_TRIGGER_MARGIN = 50;

    public static final int CHANNEL_HEADER_HEIGHT = 50;
    public static final int CHANNEL_BODY_HEIGHT = 225;
    public static final int OPEN_CHANNEL_HEIGHT = CHANNEL_HEADER_HEIGHT + CHANNEL_BODY_HEIGHT;

    public static final int SAMPLING_PROGRESS_BAR_HEIGHT = 30;
    public static final int SAMPLING_PROGRESS_BAR_WIDTH = 600;

    public static final int CHANNEL_CONTROL_PANEL_WIDTH = 400;
    public static final int DEFAULT_DISPLAY_PANEL_WIDTH = 3000;
    public static final int MAX_DISPLAY_PANEL_WIDTH = 60000;

    public static final int DEFAULT_TERMINAL_PANEL_HEIGHT = 350;


    /** === Miscellaneous Constants ------------------------------------------------ **/

    public static final int SCROLL_WEIGHT = 10;

    public static final int SAMPLES_PER_PIXEL = 1;

    public static final int ZOOM_PER_TICK = 80;

    public static final int ZOOM_SLIDER_TICKS = 1000;

    /** === Margin Constants ------------------------------------------------------- **/

    public static final Insets CHANNEL_OPTIONS_FRAME_MARGINS = new Insets(20, 20, 20, 20);
    public static final Insets GENERIC_BOX_MARGINS = new Insets(20, 20, 20, 20);
    public static final Insets POP_UP_PANEL_MARGINS = new Insets(5, 10, 5, 10);
    public static final Insets NOTIFICATION_BOX_MARGINS = new Insets(20, 20, 20, 20);
    public static final Insets CONFIRMATION_BOX_MARGINS = new Insets(20, 20, 20, 20);
    public static final Insets FORM_BOX_MARGINS = new Insets(20, 20, 20, 20);


    /** === Color Constants -------------------------------------------------------- **/

    public static final Color FOREGROUND_COLOR = new Color(245, 245, 245);
    public static final Color BACKGROUND_COLOR = Color.DARK_GRAY.darker();
    public static final Color BORDER_COLOR = Color.DARK_GRAY;

    public static final Color TRANSPARENT_FOREGROUND_COLOR = new Color(245, 245, 245, 5);
    public static final Color TRANSPARENT_BACKGROUND_COLOR = new Color(35, 34, 37, 5);
    public static final Color ERROR_TEXT_COLOR = new Color(200, 20, 70);
    public static final Color INFO_BORDER_COLOR = new Color(6, 147, 224);
    public static final Color ERROR_BORDER_COLOR = new Color(200, 20, 70);
    public static final Color WARNING_BORDER_COLOR = new Color(212, 204, 55);
    public static final Color PLOT_LINE_COLOR = new Color(66, 167, 245, 215);
    public static final Color REGULAR_GRID_LINE_COLOR = new Color(250, 250, 250, 30);
    public static final Color FINE_GRID_LINE_COLOR = new Color(250, 250, 250, 15);
    public static final Color MARKER_LINE_COLOR = new Color(247, 255, 10, 247);


    /** === Font Constants --------------------------------------------------------- **/

    public static final String FONT_STYLE = Font.SERIF;
    public static final Font DEFAULT_FONT = loadFont(ASSETS_DIR + "/Fonts/SourceSansPro/SourceSansPro-Regular.ttf", 16.5f);

    public static final Font CONTROL_PANEL_FONT = loadFont(ASSETS_DIR + "/Fonts/SourceSansPro/SourceSansPro-Regular.ttf", 16.5f);
    public static final Font CONTROL_PANEL_FONT_LIGHT = loadFont(ASSETS_DIR + "/Fonts/SourceSansPro/SourceSansPro-Light.ttf", 16.5f);
    public static final Font POP_UP_FONT = CONTROL_PANEL_FONT.deriveFont(20f);

    public static final Font CHANNEL_HEADER_FONT = new Font(FONT_STYLE, Font.BOLD, 13);
    public static final Font CHANNEL_OPTIONS_FONT = new Font(FONT_STYLE, Font.BOLD, 14);
    public static final Font PLOT_FONT = Font.getFont(Font.MONOSPACED);


    /** === Image Constants -------------------------------------------------------- **/

    public static final int START_BUTTON_WIDTH = 25;
    public static final int START_BUTTON_HEIGHT = 30;

    public static final int STOP_BUTTON_WIDTH = 30;
    public static final int STOP_BUTTON_HEIGHT = 30;

    public static final int SETTINGS_BUTTON_WIDTH = 30;
    public static final int SETTINGS_BUTTON_HEIGHT = 30;

    public static final int NEW_CHANNEL_BUTTON_WIDTH = 90;
    public static final int NEW_CHANNEL_BUTTON_HEIGHT = 40;

    public static final int REFRESH_BUTTON_WIDTH = 30;
    public static final int REFRESH_BUTTON_HEIGHT = 30;

    public static final int REMOVE_BUTTON_WIDTH = 30;
    public static final int REMOVE_BUTTON_HEIGHT = 30;

    public static final int CLOSE_BUTTON_WIDTH = 15;
    public static final int CLOSE_BUTTON_HEIGHT = 15;

    public static final int EDIT_BUTTON_WIDTH = 26;
    public static final int EDIT_BUTTON_HEIGHT = 26;

    public static final int RESET_ZOOM_BUTTON_WIDTH = 30;
    public static final int RESET_ZOOM_BUTTON_HEIGHT = 30;

    public static final int MOVE_UP_BUTTON_WIDTH = 10;
    public static final int MOVE_UP_BUTTON_HEIGHT = 10;

    public static final int MOVE_DOWN_BUTTON_WIDTH = 10;
    public static final int MOVE_DOWN_BUTTON_HEIGHT = 10;

    public static final int INFO_BUTTON_WIDTH = 20;
    public static final int INFO_BUTTON_HEIGHT = 20;

    public static final int COLLAPSE_BUTTON_WIDTH = 20;
    public static final int COLLAPSE_BUTTON_HEIGHT = 20;

    public static final int TRIGGER_RISING_BUTTON_WIDTH = 25;
    public static final int TRIGGER_RISING_BUTTON_HEIGHT = 25;

    public static final int TRIGGER_HIGH_BUTTON_WIDTH = 25;
    public static final int TRIGGER_HIGH_BUTTON_HEIGHT = 25;

    public static final int TRIGGER_FALLING_BUTTON_WIDTH = 25;
    public static final int TRIGGER_FALLING_BUTTON_HEIGHT = 25;

    public static final int TRIGGER_LOW_BUTTON_WIDTH = 25;
    public static final int TRIGGER_LOW_BUTTON_HEIGHT = 25;


    public static final ButtonIconRecord START_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Start", START_BUTTON_WIDTH, START_BUTTON_HEIGHT);

    public static final ButtonIconRecord STOP_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Stop", STOP_BUTTON_WIDTH, STOP_BUTTON_HEIGHT);

    public static final ButtonIconRecord SETTINGS_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Settings", SETTINGS_BUTTON_WIDTH, SETTINGS_BUTTON_HEIGHT);

    public static final ButtonIconRecord NEW_CHANNEL_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/NewChannel", NEW_CHANNEL_BUTTON_WIDTH, NEW_CHANNEL_BUTTON_HEIGHT);

    public static final ButtonIconRecord REFRESH_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Refresh", REFRESH_BUTTON_WIDTH, REFRESH_BUTTON_HEIGHT);

    public static final ButtonIconRecord REMOVE_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Remove", REMOVE_BUTTON_WIDTH, REMOVE_BUTTON_HEIGHT);

    public static final ButtonIconRecord CLOSE_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Remove", CLOSE_BUTTON_WIDTH, CLOSE_BUTTON_HEIGHT);

    public static final ButtonIconRecord EDIT_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Edit", EDIT_BUTTON_WIDTH, EDIT_BUTTON_HEIGHT);

    public static final ButtonIconRecord RESET_ZOOM_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/ResetZoom", RESET_ZOOM_BUTTON_WIDTH, RESET_ZOOM_BUTTON_HEIGHT);

    public static final ButtonIconRecord MOVE_UP_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/MoveUp", MOVE_UP_BUTTON_WIDTH, MOVE_UP_BUTTON_HEIGHT);

    public static final ButtonIconRecord MOVE_DOWN_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/MoveDown", MOVE_DOWN_BUTTON_WIDTH, MOVE_DOWN_BUTTON_HEIGHT);

    public static final ButtonIconRecord INFO_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Info", INFO_BUTTON_WIDTH, INFO_BUTTON_HEIGHT);

    public static final ButtonIconRecord COLLAPSE_BUTTON_ENABLED_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Collapse/Enabled", COLLAPSE_BUTTON_WIDTH, COLLAPSE_BUTTON_HEIGHT);

    public static final ButtonIconRecord COLLAPSE_BUTTON_DISABLED_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Collapse/Disabled", COLLAPSE_BUTTON_WIDTH, COLLAPSE_BUTTON_HEIGHT);

    public static final ButtonIconRecord TRIGGER_RISING_ENABLED_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Triggers/Rising/Enabled", TRIGGER_RISING_BUTTON_WIDTH, TRIGGER_RISING_BUTTON_HEIGHT);

    public static final ButtonIconRecord TRIGGER_RISING_DISABLED_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Triggers/Rising/Disabled", TRIGGER_RISING_BUTTON_WIDTH, TRIGGER_RISING_BUTTON_HEIGHT);

    public static final ButtonIconRecord TRIGGER_HIGH_ENABLED_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Triggers/High/Enabled", TRIGGER_HIGH_BUTTON_WIDTH, TRIGGER_HIGH_BUTTON_HEIGHT);

    public static final ButtonIconRecord TRIGGER_HIGH_DISABLED_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Triggers/High/Disabled", TRIGGER_HIGH_BUTTON_WIDTH, TRIGGER_HIGH_BUTTON_HEIGHT);

    public static final ButtonIconRecord TRIGGER_FALLING_ENABLED_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Triggers/Falling/Enabled", TRIGGER_FALLING_BUTTON_WIDTH, TRIGGER_FALLING_BUTTON_HEIGHT);

    public static final ButtonIconRecord TRIGGER_FALLING_DISABLED_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Triggers/Falling/Disabled", TRIGGER_FALLING_BUTTON_WIDTH, TRIGGER_FALLING_BUTTON_HEIGHT);

    public static final ButtonIconRecord TRIGGER_LOW_ENABLED_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Triggers/Low/Enabled", TRIGGER_LOW_BUTTON_WIDTH, TRIGGER_LOW_BUTTON_HEIGHT);

    public static final ButtonIconRecord TRIGGER_LOW_DISABLED_BUTTON_ICONS = getIconRecord(
            ASSETS_DIR + "/ButtonIcons/Triggers/Low/Disabled", TRIGGER_LOW_BUTTON_WIDTH, TRIGGER_LOW_BUTTON_HEIGHT);


    /** === Timing Constants ------------------------------------------------------ **/

    public static final int INFO_POPUP_DELAY = 5000;
    public static final int WARNING_POPUP_DELAY = 5000;
    public static final int ERROR_POPUP_DELAY = 5000;

    public static final int PLOT_REFRESH_RATE = 100;


    /** === Static Methods -------------------------------------------------------- **/

    /**
     * Loads a {@link ButtonIconRecord} from the specified folder of images.
     *
     * @param folderPath The path to the folder with icons.
     * @param width The desired width for the icon.
     * @param height The desired height for the icon.
     *
     * @return A {@link ButtonIconRecord} from the images in the specified folder.
     */
    private static ButtonIconRecord getIconRecord(String folderPath, int width, int height) {
        return new ButtonIconRecord(
                IconLoader.getIcon(folderPath + "/Default.png", width, height),
                IconLoader.getIcon(folderPath + "/Rollover.png", width, height),
                IconLoader.getIcon(folderPath + "/Pressed.png", width, height),
                IconLoader.getIcon(folderPath + "/Disabled.png", width, height)
        );
    }

    /**
     * Loads a font from the specified True Type Font file.
     *
     * @param filePath The path to the .ttf file.
     * @param fontSize The desired font size.
     * @return The font as a {@link Font} object.
     */
    private static Font loadFont(String filePath, float fontSize) {

        try {
            Font font = Font.createFont(Font.TRUETYPE_FONT, new File(filePath)).deriveFont(fontSize);
            GraphicsEnvironment.getLocalGraphicsEnvironment().registerFont(font);

            return font;
        } catch (IOException | FontFormatException e) {
            e.printStackTrace();
            System.exit(1);
        }

        return null;
    }
}
