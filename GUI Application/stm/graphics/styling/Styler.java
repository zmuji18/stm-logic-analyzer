package styling;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.util.HashSet;
import java.util.Set;

import static styling.GUIConstants.*;

public class Styler {

    public enum Style {
        DEFAULT,
        TRANSPARENT,
        CONTROL_PANEL,
        CHANNEL_PANEL,
        POP_UP,
        FORM_POP_UP,
        POP_UP_TEXT,
        CHANNEL_CONTROL_PANEL,
        CHANNEL_HEADER,
        CHANNEL_TOOLBAR,
        MEASURE_UNIT,
        GUI_BODY,
        ERROR_TIP,
        BORDERLESS,
        CHANNEL_OPTIONS,
        GENERIC,
        LONG,
    }

    public static JPanel style(JPanel component) {return style(component, Style.DEFAULT);}

    public static JScrollPane style(JScrollPane component) {return style(component, Style.DEFAULT);}

    public static JSplitPane style(JSplitPane component) {return style(component, Style.DEFAULT);}

    public static JTextField style(JTextField component) {return style(component, Style.DEFAULT);}

    public static JLabel style(JLabel component) {return style(component, Style.DEFAULT);}

    public static JEditorPane style(JEditorPane component) {return style(component, Style.DEFAULT);}

    public static JComboBox<?> style(JComboBox<?> component) {return style(component, Style.DEFAULT);}

    public static JButton style(JButton component) {return style(component, Style.DEFAULT);}

    public static Box style(Box component) {return style(component, Style.DEFAULT);}

    public static JComponent style(JComponent component) {return style(component, Style.DEFAULT);}

    public static JPanel style(JPanel component, Style styleOption) {

        if(isMarked(component)) return component;

        switch(styleOption) {
            case DEFAULT, CHANNEL_PANEL -> component.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, BORDER_COLOR));
            case CONTROL_PANEL -> component.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED, BORDER_COLOR.darker(), BORDER_COLOR));
            case CHANNEL_HEADER -> component.setBorder(BorderFactory.createEtchedBorder());
            case POP_UP_TEXT -> {

                component.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED, BORDER_COLOR.darker(), BORDER_COLOR));
                component.setFont(CONTROL_PANEL_FONT);
            }
            case BORDERLESS -> {}
            default -> throw new RuntimeException(styleOption + " is not a valid style option.");
        }

        component.setBackground(BACKGROUND_COLOR);

        mark(component);

        return component;
    }

    public static JScrollPane style(JScrollPane component, Style styleOption) {

        if(isMarked(component)) return component;

        switch(styleOption) {
            case DEFAULT, CHANNEL_PANEL -> component.setBorder(BorderFactory.createMatteBorder(2, 1, 2 ,1, BORDER_COLOR));
            default -> throw new RuntimeException(styleOption + " is not a valid style option.");
        }

        component.setBackground(BACKGROUND_COLOR);

        mark(component);

        return component;
    }

    public static JSplitPane style(JSplitPane component, Style styleOption) {

        if(isMarked(component)) return component;

        switch(styleOption) {
            case DEFAULT, CHANNEL_PANEL -> {

                component.setBorder(BorderFactory.createMatteBorder(8, 8, 8 ,8, BACKGROUND_COLOR));
                component.setDividerSize(8);
            }
            case GUI_BODY -> component.setBorder(BorderFactory.createMatteBorder(2, 2, 2 ,2, BACKGROUND_COLOR));
            default -> throw new RuntimeException(styleOption + " is not a valid style option.");
        }

        component.setBackground(BACKGROUND_COLOR);

        mark(component);

        return component;
    }

    public static JToolBar style(JToolBar component, Style styleOption) {

        if(isMarked(component)) return component;

        switch(styleOption) {
            case DEFAULT, CHANNEL_PANEL -> component.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, BORDER_COLOR.darker()));
            default -> throw new RuntimeException(styleOption + " is not a valid style option.");
        }

        component.setBackground(BACKGROUND_COLOR);

        mark(component);

        return component;
    }

    public static JCheckBox style(JCheckBox component, Style styleOption) {

        if(isMarked(component)) return component;

        component.setFocusPainted(false);

        switch(styleOption) {
            case DEFAULT -> component.setFont(DEFAULT_FONT);
            case CONTROL_PANEL -> component.setFont(CONTROL_PANEL_FONT);
            default -> throw new RuntimeException(styleOption + " is not a valid style option.");
        }

        mark(component);

        return component;
    }

    public static JTextField style(JTextField component, Style styleOption) {

        if(isMarked(component)) return component;

        switch(styleOption) {
            case DEFAULT -> {
                component.setFont(DEFAULT_FONT);
                component.setMaximumSize(new Dimension(GENERIC_TEXT_FIELD_WIDTH, GENERIC_TEXT_FIELD_HEIGHT));
                component.setPreferredSize(new Dimension(GENERIC_TEXT_FIELD_WIDTH, GENERIC_TEXT_FIELD_HEIGHT));
            }
            case LONG -> {
                component.setFont(DEFAULT_FONT);
                component.setPreferredSize(new Dimension(LARGE_COMBO_BOX_WIDTH, GENERIC_TEXT_FIELD_HEIGHT));
                component.setMaximumSize(new Dimension(LARGE_COMBO_BOX_WIDTH, GENERIC_TEXT_FIELD_HEIGHT));
            }
            case CONTROL_PANEL -> {
                component.setFont(CONTROL_PANEL_FONT);
                component.setPreferredSize(new Dimension(GENERIC_TEXT_FIELD_WIDTH, GENERIC_TEXT_FIELD_HEIGHT));
            }
            case POP_UP -> {
                component.setFont(POP_UP_FONT);
                component.setPreferredSize(new Dimension(GENERIC_TEXT_FIELD_WIDTH, GENERIC_TEXT_FIELD_HEIGHT));
            }
            case FORM_POP_UP -> {
                component.setFont(POP_UP_FONT);
                component.setMaximumSize(new Dimension(GENERIC_TEXT_FIELD_WIDTH, GENERIC_TEXT_FIELD_HEIGHT));
                component.setPreferredSize(new Dimension(GENERIC_TEXT_FIELD_WIDTH, GENERIC_TEXT_FIELD_HEIGHT));
            }
        }

        mark(component);

        return component;
    }

    public static JLabel style(JLabel component, Style styleOption) {

        if(isMarked(component)) return component;

        switch(styleOption) {
            case DEFAULT -> component.setFont(DEFAULT_FONT);
            case CONTROL_PANEL, CHANNEL_CONTROL_PANEL -> component.setFont(CONTROL_PANEL_FONT);
            case POP_UP -> component.setFont(POP_UP_FONT);
            default -> throw new RuntimeException(styleOption + " is not a valid style option.");
        }

        mark(component);

        return component;
    }

    public static JEditorPane style(JEditorPane component, Style styleOption) {

        if(isMarked(component)) return component;

        switch(styleOption) {
            case DEFAULT -> component.setFont(DEFAULT_FONT);
            case CONTROL_PANEL, CHANNEL_CONTROL_PANEL -> component.setFont(CONTROL_PANEL_FONT);
            case POP_UP -> component.setFont(POP_UP_FONT);
            default -> throw new RuntimeException(styleOption + " is not a valid style option.");
        }

        mark(component);

        return component;
    }

    public static JComboBox<?> style(JComboBox<?> component, Style styleOption) {

        if(isMarked(component)) return component;

        component.setOpaque(false);
        component.setFocusable(false);

        switch (styleOption) {
            case DEFAULT -> {

                component.setPreferredSize(new Dimension(GENERIC_COMBO_BOX_WIDTH, GENERIC_COMBO_BOX_HEIGHT));
                component.setMaximumSize(new Dimension(GENERIC_COMBO_BOX_WIDTH, GENERIC_COMBO_BOX_HEIGHT));
                component.setFont(DEFAULT_FONT);
            }
            case MEASURE_UNIT -> {

                component.setPreferredSize(new Dimension(SMALL_COMBO_BOX_WIDTH, GENERIC_COMBO_BOX_HEIGHT));
                component.setFont(CONTROL_PANEL_FONT);
            }
            case LONG -> {

                component.setMaximumSize(new Dimension(LARGE_COMBO_BOX_WIDTH, GENERIC_COMBO_BOX_HEIGHT));
                component.setPreferredSize(new Dimension(LARGE_COMBO_BOX_WIDTH, GENERIC_TEXT_FIELD_HEIGHT));
                component.setFont(DEFAULT_FONT);
            }
            case CONTROL_PANEL -> component.setFont(CONTROL_PANEL_FONT);
            case POP_UP -> component.setFont(POP_UP_FONT);
            default -> throw new RuntimeException(styleOption + " is not a valid style option.");
        }

        mark(component);

        return component;
    }

    public static JButton style(JButton component, Style styleOption) {

        if(isMarked(component)) return component;

        component.setFocusable(false);
        component.setOpaque(false);

        switch (styleOption) {
            case DEFAULT, POP_UP -> {}
            case GENERIC -> component.setPreferredSize(new Dimension(GENERIC_BUTTON_WIDTH, GENERIC_BUTTON_HEIGHT));
            case TRANSPARENT, CONTROL_PANEL, CHANNEL_TOOLBAR, POP_UP_TEXT -> component.setContentAreaFilled(false);
            default -> throw new RuntimeException(styleOption + " is not a valid style option.");
        }

        mark(component);

        return component;
    }

    public static Box style(Box component, Style styleOption) {

        if(isMarked(component)) return component;

        component.setOpaque(true);
        component.setBackground(BACKGROUND_COLOR);
        component.setBorder(BorderFactory.createEtchedBorder());

        switch (styleOption) {
            case DEFAULT -> component.setFont(DEFAULT_FONT);
            case CONTROL_PANEL, CHANNEL_HEADER -> component.setFont(CONTROL_PANEL_FONT);
            case CHANNEL_OPTIONS -> component.setFont(CHANNEL_OPTIONS_FONT);
            default -> throw new RuntimeException(styleOption + " is not a valid style option.");
        }

        mark(component);

        return component;
    }

    public static JComponent style(JComponent component, Style styleOption) {

        if(isMarked(component)) return component;

        component.setForeground(FOREGROUND_COLOR);

        switch (styleOption) {
            case DEFAULT -> component.setFont(DEFAULT_FONT);
            case CONTROL_PANEL, CHANNEL_HEADER -> component.setFont(CONTROL_PANEL_FONT);
            case CHANNEL_OPTIONS -> component.setFont(CHANNEL_OPTIONS_FONT);
            default -> throw new RuntimeException(styleOption + " is not a valid style option.");
        }

        mark(component);

        return component;
    }

    /**
     * Sets the pop-up location for the specified window to be the center of the screen.
     *
     * @param popUp The pop-up window as a {@link Container} object.
     */
    public static void centerPopUp(Container popUp) {

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        popUp.setLocation((screenSize.width - popUp.getWidth())/2, (screenSize.height - popUp.getHeight())/2);
    }

    /**
     * Resizes the component to the specified dimensions.
     *
     * @param component The {@link Component} to be resized.
     * @param width The new width of the component.
     * @param height The new height of the component.
     */
    public static void setSize(Component component, int width, int height) {

        Dimension newDimension = new Dimension(width, height);

        component.setMinimumSize(newDimension);
        component.setMaximumSize(newDimension);
        component.setPreferredSize(newDimension);

        SwingUtilities.invokeLater(() -> {
                component.repaint();
                component.revalidate();
        });
    }

    /**
     * Creates and adds a rigid area of the specified size to the Container.
     * The specified container is assumed to have a BoxLayout.
     *
     * @param container The Container to which the rigid area will be added.
     * @param gapSize Width and Height of the rigid area.
     */
    public static void addGap(Container container, int gapSize) {

        container.add(Box.createRigidArea(new Dimension(gapSize, gapSize)));
    }

    /**
     * Adds a visible separator block to the given Container.
     * The specified container is assumed to have a BoxLayout.
     *
     * @param container The container to which the separator will be added.
     * @param includeGap If true, adds an additional rigid area to the startTime and endTime of the separator block.
     */
    public static void addSeparator(Container container, int orientation, boolean includeGap) {

        if(includeGap) addGap(container, GENERIC_COMPONENT_GAP);

        JSeparator separator = new JSeparator(orientation);
        container.add(separator);

        if(orientation == JSeparator.VERTICAL) {
            separator.setMaximumSize(new Dimension(JSeparator.WIDTH, separator.getMaximumSize().height));
        } else {
            separator.setMaximumSize(new Dimension(separator.getMaximumSize().width, JSeparator.HEIGHT));
        }

        Styler.mark(separator);

        if(includeGap) addGap(container, GENERIC_COMPONENT_GAP);
    }

    /**
     * @param stylizedObject A styleable object.
     * @return True if the object has already been styled, false otherwise.
     */
    public static boolean isMarked(Object stylizedObject) {return stylizedObjects.contains(stylizedObject);}

    /**
     * Marks the specified object as styled.
     *
     * @param stylizedObject The {@link Object} to be marked as styled.
     */
    public static void mark(Object stylizedObject) {stylizedObjects.add(stylizedObject);}

    
    /** === Static Variables ------------------------------------------------- **/

    private static final Set<Object> stylizedObjects;

    static {stylizedObjects = new HashSet<>();}

    /* An instance of this class is not meant to be created. */
    private Styler() {};
}
