package protocols.options;

import java.util.Map;

public final class ParallelEncoderOptions extends ProtocolOptions {

    private static final String N_DATA_CHANNELS = "Number of Data Channels";


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * @return The name of the field specifying the number of data channels to be used.
     */
    public static String getDataChannelCountOption() {return N_DATA_CHANNELS;}

    /**
     * @return The number of data channels specified for this {@link ParallelEncoderOptions}.
     */
    public int getDataChannelCount() {return this.getIntOption(getDataChannelCountOption());}


    /** === Private Functions ------------------------------------------------ **/

    /**
     * Creates a {@link ParallelEncoderOptions} object from the specified map.
     *
     * @param options The protocol options as a {@link Map}.
     */
    ParallelEncoderOptions(Map<String, Object> options) {

        super(options);

        int nDataChannels = this.getIntOption(getDataChannelCountOption());
        assert nDataChannels > 0: "A positive number of data channels must be specified. Received: " + nDataChannels;
    }
}
