package protocols.options;

import java.util.Arrays;
import java.util.Map;

public final class SPIOptions extends ProtocolOptions {

    public static final Integer[] BITS_PER_PACKET_VALUES = {4, 8, 12, 16};

    private static final String N_SS_CHANNELS = "Number of SS Channels";
    private static final String N_BITS_PER_PACKET = "Bits/Packet";
    private static final String CLOCK_POLARITY = "Clock Polarity";
    private static final String CLOCK_PHASE = "Clock Phase";
    private static final String BIT_ORDER = "Bit Order";


    /** === Enum Definitions ------------------------------------------------- **/

    private interface StringEnum {
        @Override
        String toString();

        static <T extends StringEnum> T toEnum(T[] values, String value) {
            return Arrays.stream(values).filter(enumValue -> enumValue.toString().equals(value)).findFirst().orElseThrow();
        }
    }

    public enum ClockPolarity implements StringEnum {
        DEFAULT("Default"), INVERTED("Inverted");

        ClockPolarity(String name) {this.name = name;}

        @Override
        public String toString() {return this.name;}

        private final String name;
    }

    public enum ClockPhase implements StringEnum {
        DEFAULT("Default"), DELAYED("Delayed");

        ClockPhase(String name) {this.name = name;}

        @Override
        public String toString() {return this.name;}

        private final String name;
    }

    public enum BitOrder implements StringEnum {
        LSB("Least Significant Bit First"), MSB("Most Significant Bit First");

        BitOrder(String name) {this.name = name;}

        @Override
        public String toString() {return this.name;}

        private final String name;
    }


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * @return The name of the field specifying the number of slave select channels to be used.
     */
    public static String getSSChannelCountOption() {return N_SS_CHANNELS;}

    /**
     * @return The number of slave select channels specified for this {@link SPIOptions}.
     */
    public int getSSChannelCount() {return this.getIntOption(getSSChannelCountOption());}

    /**
     * @return The name of the field specifying the number of bits present in a single SPI data packet.
     */
    public static String getBitsPerPacketOption() {return N_BITS_PER_PACKET;}

    /**
     * @return The possible values for the Bits/Packet option.
     */
    public static Integer[] getBitsPerPacketOptionValues() {return BITS_PER_PACKET_VALUES;}

    /**
     * @return The number of bits present in a single SPI data packet.
     */
    public int getBitsPerPacket() {return this.getIntOption(getBitsPerPacketOption());}

    /**
     * @return The name of the field specifying the clock polarity for the SPI protocol.
     */
    public static String getClockPolarityOption() {return CLOCK_POLARITY;}

    /**
     * @return The specified clock polarity as {@link ClockPolarity} object.
     */
    public ClockPolarity getClockPolarity() {return this.clockPolarity;}

    /**
     * @return The name of the field specifying the clock phase for the SPI protocol.
     */
    public static String getClockPhaseOption() {return CLOCK_PHASE;}

    /**
     * @return The specified clock phase as {@link ClockPhase} object.
     */
    public ClockPhase getClockPhase() {return this.clockPhase;}

    /**
     * @return The name of the field specifying the bit order for the SPI protocol.
     */
    public static String getBitOrderOption() {return BIT_ORDER;}

    /**
     * @return The specified bit order as {@link BitOrder} object.
     */
    public BitOrder getBitOrder() {return this.bitOrder;}


    /** === Private Functions ------------------------------------------------ **/

    /**
     * Creates a {@link SPIOptions} object from the specified map.
     *
     * @param options The protocol options as a {@link Map}.
     */
    SPIOptions(Map<String, Object> options) {

        super(options);

        int nSSChannels = this.getSSChannelCount();
        assert nSSChannels >= 0: "A non-negative number of slave select channels must be specified. Received: " + nSSChannels;

        int nBitsPerPacket = this.getBitsPerPacket();
        assert nBitsPerPacket > 0: "A positive number of Bits/Packet must be specified. Received: " + nBitsPerPacket;

        this.clockPolarity = StringEnum.toEnum(ClockPolarity.values(), this.getStringOption(getClockPolarityOption()));
        this.clockPhase = StringEnum.toEnum(ClockPhase.values(), this.getStringOption(getClockPhaseOption()));
        this.bitOrder = StringEnum.toEnum(BitOrder.values(), this.getStringOption(getBitOrderOption()));
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final ClockPolarity clockPolarity;
    private final ClockPhase clockPhase;
    private final BitOrder bitOrder;
}