package protocols.protocol;

import java.util.LinkedHashMap;
import java.util.Map;

public final class UART extends Protocol {

    private static final String TRANSMIT_CHANNEL = "Transmit Channel";
    private static final String RECEIVE_CHANNEL = "Receive Channel";

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * @return The protocol type of this {@link Protocol} as a {@link ProtocolType}.
     */
    @Override
    public ProtocolType getProtocol() {return ProtocolType.UART;}

    @Override
    public Map<String, Integer> getChannelMapping() {

        Map<String, Integer> channelMapping = new LinkedHashMap<>();
        channelMapping.put(getReceiveChannelName(), this.getReceiveChannel());
        channelMapping.put(getTransmitChannelName(), this.getTransmitChannel());

        return channelMapping;
    }

    /**
     * @return The name of the Tx channel as a {@link String}.
     */
    public static String getTransmitChannelName() {return TRANSMIT_CHANNEL;}

    /**
     * @return The integer index of the Tx channel.
     */
    public int getTransmitChannel() {return this.getChannelMapping(getTransmitChannelName());}

    /**
     * @return The name of the Rx channel as a {@link String}.
     */
    public static String getReceiveChannelName() {return RECEIVE_CHANNEL;}

    /**
     * @return The integer index of the Rx channel.
     */
    public int getReceiveChannel() {return this.getChannelMapping(getReceiveChannelName());}


    /** === Private Functions ------------------------------------------------ **/

    UART(Map<String, Integer> channelMapping) {super(channelMapping, null);}
}
