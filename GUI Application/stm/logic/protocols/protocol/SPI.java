package protocols.protocol;

import protocols.options.SPIOptions;

import java.util.LinkedHashMap;
import java.util.Map;

public final class SPI extends Protocol {

    private static final String CLOCK_CHANNEL = "Clock Channel";
    private static final String MOSI_CHANNEL = "MOSI";
    private static final String MISO_CHANNEL = "MISO";
    private static final String SS_CHANNEL_PREFIX = "SS Channel ";


    /** === Interface Functions ---------------------------------------------- **/

    @Override
    public ProtocolType getProtocol() {return ProtocolType.SPI;}

    @Override
    public Map<String, Integer> getChannelMapping() {

        SPIOptions options = this.getProtocolOptions();
        Map<String, Integer> channelMapping = new LinkedHashMap<>();

        channelMapping.put(getClockChannelName(), this.getClockChannel());
        channelMapping.put(getMOSIChannelName(), this.getMOSIChannel());
        channelMapping.put(getMISOChannelName(), this.getMISOChannel());

        for(int i = 0; i < options.getSSChannelCount(); i++) {
            channelMapping.put(getSlaveSelectChannelName(i), this.getSlaveSelectChannel(i));
        }

        return channelMapping;
    }

    /**
     * @return The name of the clock channel as a {@link String}.
     */
    public static String getClockChannelName() {return CLOCK_CHANNEL;}

    /**
     * @return The integer index of the clock channel.
     */
    public int getClockChannel() {return this.getChannelMapping(getClockChannelName());}

    /**
     * @return The name of the MOSI channel as a {@link String}.
     */
    public static String getMOSIChannelName() {return MOSI_CHANNEL;}

    /**
     * @return The integer index of the MOSI channel.
     */
    public int getMOSIChannel() {return this.getChannelMapping(getMOSIChannelName());}

    /**
     * @return The name of the MISO channel as a {@link String}.
     */
    public static String getMISOChannelName() {return MISO_CHANNEL;}

    /**
     * @return The integer index of the MISO channel.
     */
    public int getMISOChannel() {return this.getChannelMapping(getMISOChannelName());}

    /**
     * @param index The index of the slave select channel.
     * @return The name of the slave select channel with the specified index.
     */
    public static String getSlaveSelectChannelName(int index) {return SS_CHANNEL_PREFIX + index;}

    /**
     * @param index The index of the slave select channel.
     * @return The index of the specified slave select channel.
     */
    public int getSlaveSelectChannel(int index) {return this.getChannelMapping(getSlaveSelectChannelName(index));}

    @Override
    public SPIOptions getProtocolOptions() {return (SPIOptions) super.getProtocolOptions();}

    /** === Private Functions ------------------------------------------------ **/

    SPI(Map<String, Integer> channelMapping, SPIOptions options) {super(channelMapping, options);}
}
