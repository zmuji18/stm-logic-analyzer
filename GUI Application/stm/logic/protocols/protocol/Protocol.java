package protocols.protocol;

import protocols.options.I2COptions;
import protocols.options.ParallelEncoderOptions;
import protocols.options.ProtocolOptions;
import protocols.options.SPIOptions;

import java.util.Collections;
import java.util.Map;

public abstract class Protocol {

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a new {@link Protocol} of the given type from the specified channel mapping.
     *
     * @param type The protocol type of the new {@link Protocol} as a {@link ProtocolType}.
     * @param channelMapping The channel mapping as a {@link Map}.
     * @param protocolOptions Additional options for the protocol of the specified type as a {@link Map}.
     *
     * @return A new {@link Protocol} object of the specified type.
     */
    public static Protocol from(ProtocolType type, Map<String, Integer> channelMapping, Map<String, Object> protocolOptions) {

        Protocol protocol;
        ProtocolOptions options = ProtocolOptions.from(type, protocolOptions);

        switch (type) {
            case PARALLEL_ENCODER -> protocol = new ParallelEncoder(channelMapping, (ParallelEncoderOptions) options);
            case I2C -> protocol = new I2C(channelMapping, (I2COptions) options);
            case SPI -> protocol = new SPI(channelMapping, (SPIOptions) options);
            case UART -> protocol = new UART(channelMapping);
            default -> throw new RuntimeException(type + " is not a valid protocol type.");
        }
        return protocol;
    }

    /**
     * @return The protocol type of this {@link Protocol} as a {@link ProtocolType}.
     */
    public abstract ProtocolType getProtocol();

    /**
     * @return The channel mapping for this {@link Protocol} as a {@link Map}.
     */
    public abstract Map<String, Integer> getChannelMapping();

    /**
     * @return The {@link ProtocolOptions} specified for this {@link Protocol}, or null if none are specified.
     */
    public ProtocolOptions getProtocolOptions() {return this.options;}

    /** === Private Functions ------------------------------------------------ **/

    /**
     * Creates a protocol with the specified channel mapping.
     *
     * @param channelMapping The channel mapping as a {@link Map}.
     */
    protected Protocol(Map<String, Integer> channelMapping, ProtocolOptions options) {

        assert channelMapping != null: "The specified channel mapping cannot be null.";

        this.channelMapping = Collections.unmodifiableMap(channelMapping);
        this.options = options;
    }

    /**
     * Returns the channel index for the specified channel type for this protocol.
     *
     * @param channel The name of the channel type as a {@link String}.
     *
     * @return The channel index for the specified channel type.
     */
    protected final Integer getChannelMapping(String channel) {

        assert this.channelMapping.containsKey(channel) :
                "There is no channel mapping for " + channel + " in this protocol.";

        return this.channelMapping.get(channel);
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final Map<String, Integer> channelMapping;
    private final ProtocolOptions options;
}
