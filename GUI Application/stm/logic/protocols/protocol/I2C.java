package protocols.protocol;

import protocols.options.I2COptions;

import java.util.LinkedHashMap;
import java.util.Map;

public final class I2C extends Protocol {

    public static final int N_BITS_PER_DATA_FRAME = Byte.SIZE + 1;
    public static final int N_ACK_BITS = 1;
    public static final int N_DIRECTION_BITS = 1;

    private static final String CLOCK_CHANNEL = "Clock Channel";
    private static final String DATA_CHANNEL = "Data Channel";


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * @return The protocol type of this {@link Protocol} as a {@link ProtocolType}.
     */
    @Override
    public ProtocolType getProtocol() {return ProtocolType.I2C;}

    @Override
    public Map<String, Integer> getChannelMapping() {

        Map<String, Integer> channelMapping = new LinkedHashMap<>();
        channelMapping.put(getClockChannelName(), this.getClockChannel());
        channelMapping.put(getDataChannelName(), this.getDataChannel());

        return channelMapping;
    }

    /**
     * @return The name of the clock channel as a {@link String}.
     */
    public static String getClockChannelName() {return CLOCK_CHANNEL;}

    /**
     * @return The integer index of the clock channel.
     */
    public int getClockChannel() {return this.getChannelMapping(getClockChannelName());}

    /**
     * @return The name of the data channel as a {@link String}.
     */
    public static String getDataChannelName() {return DATA_CHANNEL;}

    /**
     * @return The integer index of the data channel.
     */
    public int getDataChannel() {return this.getChannelMapping(getDataChannelName());}

    @Override
    public I2COptions getProtocolOptions() {return (I2COptions) super.getProtocolOptions();}

    /** === Private Functions ------------------------------------------------ **/

    I2C(Map<String, Integer> channelMapping, I2COptions options) {super(channelMapping, options);}
}

