package protocols.protocol;

import protocols.options.ParallelEncoderOptions;

import java.util.LinkedHashMap;
import java.util.Map;

public final class ParallelEncoder extends Protocol {

    private static final String CLOCK_CHANNEL = "Clock Channel";
    private static final String DATA_CHANNEL_PREFIX = "Bit Data Channel ";


    /** === Interface Functions ---------------------------------------------- **/

    @Override
    public ProtocolType getProtocol() {return ProtocolType.PARALLEL_ENCODER;}

    @Override
    public Map<String, Integer> getChannelMapping() {

        ParallelEncoderOptions options = this.getProtocolOptions();
        Map<String, Integer> channelMapping = new LinkedHashMap<>();

        channelMapping.put(getClockChannelName(), this.getClockChannel());

        for(int i = 0; i < options.getDataChannelCount(); i++) {
            channelMapping.put(getDataChannelName(i), this.getDataChannel(i));
        }

        return channelMapping;
    }

    /**
     * @return The protocol type of this class as a {@link ProtocolType}.
     */
    public static ProtocolType getProtocolType() {return ProtocolType.PARALLEL_ENCODER;}

    /**
     * @return The name of the clock channel as a {@link String}.
     */
    public static String getClockChannelName() {return CLOCK_CHANNEL;}

    /**
     * @return The integer index of the clock channel.
     */
    public int getClockChannel() {return this.getChannelMapping(getClockChannelName());}

    /**
     * @param index The index of the data channel.
     * @return The name of the data channel with the specified index.
     */
    public static String getDataChannelName(int index) {return DATA_CHANNEL_PREFIX + index;}

    /**
     * @param index The index of the data channel.
     * @return The integer index of the specified data channel.
     */
    public int getDataChannel(int index) {return this.getChannelMapping(getDataChannelName(index));}

    @Override
    public ParallelEncoderOptions getProtocolOptions() {return (ParallelEncoderOptions) super.getProtocolOptions();}

    /** === Private Functions ------------------------------------------------ **/

    ParallelEncoder(Map<String, Integer> channelMapping, ParallelEncoderOptions options) {
        super(channelMapping, options);
    }
}
