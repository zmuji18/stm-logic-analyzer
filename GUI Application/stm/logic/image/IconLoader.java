package image;

import com.mortennobel.imagescaling.MultiStepRescaleOp;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Class: IconLoader
 * Date Created: 08.07.21 22:13
 *
 * === Description ============================================= *
 * Helper class for easily loading images.
 */
public class IconLoader {

    /** === Static Interface Functions --------------------------------------- **/

    /**
     * Returns an icon from the specified file.
     *
     * @param path Path to an image file.
     * @return The image as an {@link Icon} object or null if one couldn't be loaded.
     */
    public static Icon getIcon(String path) {

        if(!loadedIcons.containsKey(path)) {
            try {
                loadedIcons.put(path, new ImageIcon(ImageIO.read(new File(path))));

            } catch (IllegalArgumentException | IOException e) {
                System.out.println("Couldn't find image at location: " + path);
                e.printStackTrace();
            }
        }
        return loadedIcons.get(path);
    }

    /**
     * Returns a scaled icon from the specified file.
     *
     * @param path Path to an image file.
     * @param width The desired width of the icon.
     * @param height The desired height of the icon.
     * @return The image as an {@link Icon} object or null if one couldn't be loaded.
     */
    public static Icon getIcon(String path, int width, int height) {

        // The key for the memo is the a pair of the path to the image and it's dimensions.
        Set<?> memoKey = Set.of(path, new Dimension(width, height));

        if(!loadedScaledIcons.containsKey(memoKey)) {

            try {
                BufferedImage image = ImageIO.read(new File(path));

                MultiStepRescaleOp rescaleOp = new MultiStepRescaleOp(width, height);
                image = rescaleOp.filter(image, new BufferedImage(width, height, image.getType()));

                loadedScaledIcons.put(memoKey, new ImageIcon(image));

            } catch (IllegalArgumentException | IOException e) {
                System.out.println("Couldn't find image at location: " + path);
                e.printStackTrace();
            }
        }
        return loadedScaledIcons.get(memoKey);
    }


    /** === Static Variables ------------------------------------------------- **/

    private static final Map<String, Icon> loadedIcons;
    private static final Map<Set<?>, Icon> loadedScaledIcons;

    static {
        loadedIcons = new HashMap<>();
        loadedScaledIcons = new HashMap<>();
    }
}
