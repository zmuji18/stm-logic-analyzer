package data;

import configuration.STMConfiguration;
import logging.ConfiguredLogger;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SampleDataModel {

    private static final Logger logger = ConfiguredLogger.getLogger("SampleDataModel");

    public class Snapshot {

        /** === Interface Functions ---------------------------------------------- **/

        /**
         * @return The time values array for the currently stored samples.
         */
        public Double[] getSampleTime() {
            return this.sampleTime;
        }

        /**
         * @param channelIndex The index of the channel.
         *
         * @return The sample values array for the channel with the specified index.
         */
        public Byte[] getSampleValues(int channelIndex) {
            return this.sampleValues[channelIndex];
        }

        /**
         * @return A list of all channels for which there is sample information present in this snapshot.
         */
        public List<Integer> getChannelIndices() {return this.channelIndices;}

        /**
         * @return The number of samples in this {@link SampleDataModel.Snapshot}.
         */
        public int getSampleCount() {return this.nSamples;}

        /**
         * @return The sampling period of the samples as a double (in seconds).
         */
        public double getSamplingPeriod() {return this.samplingPeriod;}


        /** === Private Methods -------------------------------------------------- **/

        /**
         * Creates a {@link Snapshot} with the specified number of samples from the current {@link SampleDataModel}.
         *
         * @param nSamples The number of samples in the snapshot.
         */
        private Snapshot(int nSamples) {

            assert nSamples >= 0 : "The number of samples must be a non-negative number.";
            assert SampleDataModel.this.sampleTime != null : "The sample time vector cannot be null.";
            assert SampleDataModel.this.sampleValues != null : "The sample value map cannot be null.";

            this.channelIndices = SampleDataModel.this.getSTMConfiguration().getSelectedChannelIndices();
            this.samplingPeriod = SampleDataModel.this.samplingPeriod;

            this.sampleTime = Arrays.stream(Arrays.copyOf(SampleDataModel.this.sampleTime, nSamples)).boxed().toArray(Double[]::new);
            this.sampleValues = new Byte[SampleDataModel.this.sampleValues.length][];

            for(Integer channelIndex: this.channelIndices) {

                this.sampleValues[channelIndex] = new Byte[nSamples];
                for(int i = 0; i < nSamples; i++) {
                    this.sampleValues[channelIndex][i] = SampleDataModel.this.sampleValues[channelIndex][i];
                }
            }

            this.nSamples = nSamples;
        }


        /** === Instance Variables ----------------------------------------------- **/

        private final List<Integer> channelIndices;

        private final Double[] sampleTime;
        private final Byte[][] sampleValues;

        private final int nSamples;
        private final double samplingPeriod;
    }


    /** === Interface Functions ---------------------------------------------- **/

    /**
     * @return An instance of the {@link SampleDataModel}.
     *
     * @throws RuntimeException If an attempt has been made to create more than one instance of this class.
     */
    public static synchronized SampleDataModel createInstance() {

        if(instance == null) {
            instance = new SampleDataModel();
        } else {
            throw new RuntimeException("Request to create multiple instances of the sample data model.");
        }

        return instance;
    }

    /**
     * @return The currently set STM configuration as an {@link STMConfiguration} object.
     */
    public STMConfiguration getSTMConfiguration() {return this.stmConfiguration;}

    /**
     * Sets the specified STM configuration for this sample data model.
     *
     * @param configuration The STM configuration as an {@link STMConfiguration} object.
     */
    public void setSTMConfiguration(STMConfiguration configuration) {

        this.isInitialized = false;
        this.stmConfiguration = configuration;
    }

    /**
     * Initializes the {@link SampleDataModel} using the currently specified STM configuration.
     *
     * @param sampleBuffer The byte array that will be filled with encoded sample bytes.
     * @param sampleBufferSemaphore The semaphore used to synchronize byte decoding and receiving.
     */
    public void initializeModel(byte[] sampleBuffer, Semaphore sampleBufferSemaphore, Runnable processingFinishedTask) {

        assert this.stmConfiguration != null : "No configuration has been set for the sample data model.";

        this.calculateSamplingParameters();
        this.allocateDataArrays();
        this.startSampleByteDecoder(sampleBuffer, sampleBufferSemaphore, processingFinishedTask);

        this.isInitialized = true;
        this.processingCompleteLatch = new CountDownLatch(1);
    }

    /**
     * Stops the {@link SampleDataModel} from processing any further samples.
     */
    public void stopProcessing() {

        assert this.isInitialized : "The sample data model is not initialized.";
        this.decoderThread.interrupt();
    }

    /**
     * @return True if this {@link SampleDataModel} has been initialized.
     */
    public boolean isInitialized() {return this.isInitialized;}

    /**
     * @return True if sample processing is complete, false otherwise.
     */
    public boolean isComplete() {
        return this.getTotalSamples() == this.getSampleCount();
    }

    /**
     * @return The number of samples currently added or queued for this {@link SampleDataModel}.
     */
    public int getSampleCount() {

        assert this.isInitialized : "The sample data model is not initialized.";
        return this.nSamples;
    }

    /**
     * @return The total number of samples to be stored in the sample model.
     */
    public int getTotalSamples() {

        assert this.isInitialized : "The sample data model is not initialized.";
        return this.totalSamples;
    }

    /**
     * @return A snapshot of the current state of the sample model as a {@link Snapshot} object.
     */
    public Snapshot getSnapshot() {
        return this.getSnapshot(this.getSampleCount());
    }

    /**
     * @return A snapshot of the final state of the sample model as a {@link Snapshot} object.
     */
    public Snapshot getFinalSnapshot() {

        logger.info("Final snapshot of " + this.getTotalSamples() + " has been requested.");

        try {
            processingCompleteLatch.await();
        } catch (InterruptedException e) {
            logger.warning("Thread interrupted while waiting for final snapshot.");
        }

        if(!this.isComplete()) {
            logger.info("Final snapshot incomplete, because sampling was interrupted.\n" +
                    "Returning a snapshof of " + this.getSampleCount() + " samples.");
        }

        return this.getSnapshot(this.getSampleCount());
    }

    /**
     * Returns a snapshot of this sample data model for the specified number of samples.
     *
     * @param nSamples The number of samples that should be included in the snapshot.
     * @return A {@link Snapshot} of the first {@code nSamples} samples.
     */
    public Snapshot getSnapshot(int nSamples) {

        assert this.isInitialized : "The sample data model has not been initialized.";

        logger.fine("Requesting snapshot for " + nSamples + " samples.");
        return new Snapshot(nSamples);
    }


    /** === Private Methods -------------------------------------------------- **/

    /**
     * Creates a blank, uninitialized {@link SampleDataModel}.
     */
    private SampleDataModel() {

        this.isInitialized = false;
        this.stmConfiguration = null;
    }

    /**
     * Calculates values for general sampling parameters based on the currently set STM configuration.
     */
    private void calculateSamplingParameters() {

        STMConfiguration configuration = this.getSTMConfiguration();
        int nSelectedChannels = configuration.getSelectedChannelIndices().size();

        this.nSamples = 0;
        this.totalSamples = configuration.getSamplingMemory() * Byte.SIZE / nSelectedChannels;
        this.samplingPeriod = 1 / configuration.getSamplingRate();

        this.totalBytes = configuration.getSamplingMemory();

        logger.info("Expecting " + this.totalSamples + " samples for each channel.");
    }

    /**
     * Allocates arrays for storing sampled data.
     */
    private void allocateDataArrays() {

        STMConfiguration configuration = this.getSTMConfiguration();
        List<Integer> selectedChannels = configuration.getSelectedChannelIndices();

        this.sampleTime = new double[this.totalSamples + 1];
        this.sampleValues = new byte[selectedChannels.stream().max(Integer::compareTo).orElseThrow() + 1][];

        for(Integer channelIndex : selectedChannels) {
            this.sampleValues[channelIndex] = new byte[this.totalSamples + 1];
        }
    }

    /**
     * Starts a new {@link Thread} that will query available samples bytes from the specified buffer and decode them.
     *
     * @param sampleBuffer The byte buffer for the samples.
     * @param sampleBufferSemaphore The {@link Semaphore} used for synchronization.
     * @param processingFinishedTask The {@link Runnable} that should be executed once processing is complete.
     */
    private void startSampleByteDecoder(byte[] sampleBuffer, Semaphore sampleBufferSemaphore, Runnable processingFinishedTask) {

        STMConfiguration configuration = this.getSTMConfiguration();

        Integer[] selectedChannels = configuration.getSelectedChannelIndices()
                .stream().sorted().toArray(Integer[]::new);

        int nChannels = selectedChannels.length;

        this.decoderThread = new Thread(new Runnable() {
            @Override
            public void run() {

                logger.info("Sample decoding has started.");

                while(!Thread.interrupted() && this.nDecodedBytes < SampleDataModel.this.totalBytes) {

                    try {
                        sampleBufferSemaphore.acquire();
                    } catch (InterruptedException e) {
                        break;
                    }

                    for (int i = 0; i < Byte.SIZE; i++) {

                        // NOTE: This line just gets the value for the bit at index i from the current byte.
                        byte currSampleValue = (byte) ((sampleBuffer[this.nDecodedBytes] & (1 << i)) != 0 ? 1 : 0);
                        SampleDataModel.this.sampleValues[selectedChannels[this.lastChannelIndex]][SampleDataModel.this.nSamples] = currSampleValue;

                        this.lastChannelIndex++;

                        if(this.lastChannelIndex == nChannels) {

                            SampleDataModel.this.sampleTime[SampleDataModel.this.nSamples] = this.currentSampleTime;
                            this.currentSampleTime += SampleDataModel.this.samplingPeriod;

                            this.lastChannelIndex = 0;
                            SampleDataModel.this.nSamples++;
                        }
                    }

                    this.nDecodedBytes++;

                    logger.fine("Decoded " + this.nDecodedBytes + " bytes.");
                }

                if(this.nDecodedBytes == SampleDataModel.this.totalBytes) {
                    logger.info("Sample processing complete.");
                }
                else {
                    logger.info("Sample processing has been interrupted.");
                }

                processingCompleteLatch.countDown();
                processingFinishedTask.run();
            }

            private int lastChannelIndex;
            private int nDecodedBytes;
            private double currentSampleTime;
        });

        this.decoderThread.start();
    }

    /** === Instance Variables ----------------------------------------------- **/

    private CountDownLatch processingCompleteLatch;
    private Thread decoderThread;

    private boolean isInitialized;
    private STMConfiguration stmConfiguration;

    private int nSamples;
    private int totalSamples;

    private int totalBytes;
    private double samplingPeriod;

    private double[] sampleTime;
    private byte[][] sampleValues;

    /** === Static Variables ------------------------------------------------- **/

    private static SampleDataModel instance;

    static {
        logger.setLevel(Level.INFO);
    }
}
