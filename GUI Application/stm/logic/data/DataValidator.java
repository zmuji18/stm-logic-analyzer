package data;

public interface DataValidator {

    /**
     * Checks the specified data. if it is invalid, returns the reason as a String, else
     * null.
     *
     * @param data The data as a {@link String} object.
     * @return null if the data is valid, else the reason for being invalid.
     */
    String on(String data);

    /**
     * Validates the specified string data.
     *
     * @param data The data as a {@link String} object.
     * @return True if the data is valid for this {@link DataValidator}, false otherwise.
     */
    default boolean validate(String data) {return on(data) == null;}
}
