package data;

import com.fazecast.jSerialComm.SerialPort;
import protocols.protocol.ProtocolType;

import java.nio.ByteOrder;
import java.util.Map;

public final class DataConstants {

    public static final String STM32_PORT_NAME = "STM32 Virtual ComPort";

    public static final int N_CHANNELS = 12;

    public static final int MAX_MEMORY = 1_000_000;

    public static final ByteOrder CONFIGURATION_BYTE_ORDERING = ByteOrder.LITTLE_ENDIAN;

    public static final int DATA_WRITE_TIMEOUT = 3_000;
    public static final int DATA_READ_TIMEOUT = 3_000;

    public static final int ACKNOWLEDGEMENT_TIMEOUT = 10_000;

    public static final byte START_SIGNAL = (byte) 0b00000001;
    public static final byte STOP_SIGNAL = (byte) 0b00000000;

    public static final byte ACK_SIGNAL = (byte) 0b00000001;

    public static final int STM_REFERENCE_VOLTAGE = 3;

    public static final int MAXIMUM_N_DECIMAL_POINTS = 10;

    public static final int BAUD_RATE = 115200;
    public static final int BYTES_PER_WORD = 8;
    public static final int STOP_BITS = SerialPort.ONE_STOP_BIT;
    public static final int PARITY = SerialPort.EVEN_PARITY;

    public static final Map<String, Integer> SAMPLING_RATES = Map.ofEntries(
            Map.entry("2 MS/s", 2_000_000),
            Map.entry("1 MS/s", 1_000_000),
            Map.entry("500 KS/s", 500_000),
            Map.entry("200 KS/s", 200_000),
            Map.entry("100 KS/s", 100_000),
            Map.entry("50 KS/s", 50_000),
            Map.entry("20 KS/s", 20_000),
            Map.entry("10 KS/s", 10_000),
            Map.entry("1 KS/s", 1_000),
            Map.entry("500 S/s", 500),
            Map.entry("100 S/s", 100),
            Map.entry("50 S/s", 50),
            Map.entry("10 S/s", 10)
    );

    public static final Map<String, Number> SAMPLING_DURATION_UNITS = Map.ofEntries(
            Map.entry("s", 1),
            Map.entry("ms", 1e-3),
            Map.entry("us", 1e-6)
    );

    public static final Map<String, Integer> SAMPLING_MEMORY_UNITS = Map.ofEntries(
            Map.entry("B", 1),
            Map.entry("KB", 1_000),
            Map.entry("MB", 1_000_000)
    );

    public static final Map<String, Number> LOGIC_LEVELS = Map.ofEntries(
            Map.entry("5 V",  5),
            Map.entry("4 V",  4),
            Map.entry("3.5 V",  3.5),
            Map.entry("3.0 V",  3.0),
            Map.entry("2.5 V",  2.5),
            Map.entry("2.0 V",  2.0),
            Map.entry("1.75 V",  1.75),
            Map.entry("1.5 V",  1.5),
            Map.entry("1.0 V",  1.0)
    );


    public static final ProtocolType[] PROTOCOLS = ProtocolType.values();

    public static final Map<String, Integer> CHANNELS;

    static {

        Map.Entry<String, Integer>[] channelEntries = new Map.Entry[N_CHANNELS];

        for(int i = 1; i <= N_CHANNELS; i++) channelEntries[i - 1] = Map.entry("Channel " + i, i);

        CHANNELS = Map.ofEntries(channelEntries);
    }

    /* This class is meant for holding constants */
    private DataConstants() {}
}
