package decoder;

import decoder.data.frame.SPIFrame;
import decoder.data.marker.MessageMarker;
import decoder.data.message.DecodedMessage;
import decoder.data.message.SPIMessage;
import logging.ConfiguredLogger;
import protocols.options.SPIOptions;
import protocols.protocol.SPI;
import utility.Timestamp;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

public class SPIDecoder extends Decoder {

    private static final Logger logger = ConfiguredLogger.getLogger("SPIDecoder");


    /** === Interface Functions ---------------------------------------------- **/

    @Override
    public List<Integer> getUsedChannelIndices() {

        SPI protocol = this.getProtocol();
        List<Integer> channelIndices = new ArrayList<>();

        channelIndices.addAll(List.of(protocol.getClockChannel(), protocol.getMOSIChannel(), protocol.getMISOChannel()));
        channelIndices.addAll(IntStream.range(0, protocol.getProtocolOptions().getSSChannelCount())
                .map(protocol::getSlaveSelectChannel).boxed().toList());

        return Collections.unmodifiableList(channelIndices);
    }

    @Override
    public List<DecodedMessage> decode(Double[] sampleTime, Map<Integer, Byte[]> sampleValues, int startIndex, int endIndex) {

        List<DecodedMessage> decodedMessages = new ArrayList<>();

        SPI protocol = this.getProtocol();
        SPIOptions options = protocol.getProtocolOptions();

        Byte[] clockChannel = sampleValues.get(protocol.getClockChannel());
        Byte[] MOSIChannel = sampleValues.get(protocol.getMOSIChannel());
        Byte[] MISOChannel = sampleValues.get(protocol.getMISOChannel());

        for(int i = 0; i < options.getSSChannelCount(); i++) {

            Byte[] ssChannel = sampleValues.get(protocol.getSlaveSelectChannel(i));

            List<Integer> startSignalIndices = getFallingEdgeIndices(ssChannel, startIndex, endIndex);
            List<Integer> stopSignalIndices = getRisingEdgeIndices(ssChannel, startIndex, endIndex);

            List<MessageMarker> messageMarkers =
                    MessageMarker.getMessageMarkers(startSignalIndices, stopSignalIndices, MessageMarker::from);

            for(MessageMarker marker: messageMarkers) {

                List<Integer> clockEdges = Collections.unmodifiableList(
                        this.getClockEdgeIndices(clockChannel, marker.getStartIndex(), marker.getEndIndex()));

                decodedMessages.add(this.decodeMessage(sampleTime, MOSIChannel, clockEdges, marker, DataDirection.SEND));
                decodedMessages.add(this.decodeMessage(sampleTime, MISOChannel, clockEdges, marker, DataDirection.RECEIVE));
            }
        }

        return decodedMessages;
    }

    @Override
    protected SPI getProtocol() {return (SPI) super.getProtocol();}

    /** === Private Functions ------------------------------------------------ **/

    SPIDecoder(SPI protocol) {super(protocol);}

    /**
     * Returns the indices of the data loading clock edges in the specified range.
     *
     * @param clockChannel The clock channel samples as a byte array.
     * @param startIndex The start index of the range.
     * @param endIndex The end index of the range.
     *
     * @return The {@link List} of clock edge indices.
     */
    private List<Integer> getClockEdgeIndices(Byte[] clockChannel, int startIndex, int endIndex) {

        SPI protocol = this.getProtocol();

        boolean polarityIsInverted = (protocol.getProtocolOptions().getClockPolarity() == SPIOptions.ClockPolarity.INVERTED);
        boolean phaseIsDelayed = (protocol.getProtocolOptions().getClockPhase() == SPIOptions.ClockPhase.DELAYED);

        // NOTE: This lines up nicely to give "data loading" on the rising edges of the clock.
        if(polarityIsInverted == phaseIsDelayed) {
            return getRisingEdgeIndices(clockChannel, startIndex, endIndex);
        } else { // The data loads on the falling edges.
            return getFallingEdgeIndices(clockChannel, startIndex, endIndex);
        }
    }

    /**
     * Decodes the message in the range indicated by the specified message marker.
     *
     * @param sampleTime The array of sample timestamps.
     * @param dataChannel The array of sample values on the data channel.
     * @param clockEdges The clock edge indices as a {@link List}.
     * @param messageMarker The message marker as a {@link MessageMarker}.
     * @param direction The direction of the data as a {@link DataDirection}.
     *
     * @return The decoded message as a {@link SPIMessage} object.
     */
    private SPIMessage decodeMessage(Double[] sampleTime,
                                     Byte[] dataChannel,
                                     List<Integer> clockEdges,
                                     MessageMarker messageMarker,
                                     DataDirection direction) {

        final int BITS_PER_PACKET = this.getProtocol().getProtocolOptions().getBitsPerPacket();
        final int N_CLOCK_EDGES = clockEdges.size();

        List<SPIFrame> decodedFrames = new ArrayList<>();

        for(int i = 0; i < N_CLOCK_EDGES; i += BITS_PER_PACKET) {
            decodedFrames.add(
                    this.decodeFrame(dataChannel, clockEdges.subList(i, Math.min(i + BITS_PER_PACKET, N_CLOCK_EDGES))));
        }

        return SPIMessage.newBuilder()
                .addDecodedFrames(decodedFrames)
                .setDataDirection(direction)
                .setStartTime(Timestamp.of(sampleTime[messageMarker.getStartIndex()]))
                .setEndTime(Timestamp.of(sampleTime[messageMarker.getEndIndex()]))
                .setStartIndex(messageMarker.getStartIndex())
                .setEndIndex(messageMarker.getEndIndex()).build();
    }

    /**
     * Decodes the data frame using sample values at the specified indices.
     *
     * @param sampleValue The byte array of sample values.
     * @param sampleIndices The indices of the samples to be used as a {@link List}.
     *
     * @return The decoded data frame as an {@link SPIFrame}.
     */
    private SPIFrame decodeFrame(Byte[] sampleValue, List<Integer> sampleIndices) {

        SPIOptions options = this.getProtocol().getProtocolOptions();

        final int BITS_PER_PACKET = options.getBitsPerPacket();
        final SPIOptions.BitOrder bitOrder = options.getBitOrder();

        if(sampleIndices.size() != BITS_PER_PACKET) {
            logger.log(Level.WARNING, "The data frame should contain: " + BITS_PER_PACKET + " samples.\n" +
                    "Received: " + sampleIndices.size());

            return SPIFrame.from(new byte[]{0});
        }

        byte[] packetData = new byte[(BITS_PER_PACKET - 1)/Byte.SIZE + 1];
        Arrays.fill(packetData, (byte) 0);

        for(int i = 0; i < BITS_PER_PACKET; i++) {

            int currBit = (sampleValue[sampleIndices.get(i)] == 0 ? 0 : 1);
            int currIndex;

            switch (bitOrder){
                case LSB -> currIndex = i % Byte.SIZE;
                case MSB -> currIndex = Byte.SIZE - (i % Byte.SIZE) - 1;
                default -> throw new RuntimeException("Invalid bit order: " + bitOrder);
            }

            packetData[i / Byte.SIZE] |= (currBit << currIndex);
        }

        return SPIFrame.from(packetData);
    }
}
