package decoder.data.frame;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Objects;

public abstract class DecodedFrame {

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * @return The byte ordering of the data in this {@link DecodedFrame}.
     */
    public abstract ByteOrder getByteOrdering();

    /**
     * @return THe number of bits that were decoded for this {@link DecodedFrame}.
     */
    public abstract int getDecodedBitCount();

    /**
     * @return A human readable string representation of this {@link DecodedFrame}.
     */
    @Override
    public abstract String toString();

    /**
     * @return A human readable string representation of this {@link DecodedFrame} in HTML format.
     */
    public abstract String toHTMLString();

    /**
     * @return The decoded bytes as a char value (may underflow).
     */
    public char toChar() {return this.getParsingBuffer(Character.BYTES).rewind().getChar();}

    /**
     * @return The decoded bytes as a short value (may underflow).
     */
    public short toShort() {return this.getParsingBuffer(Short.BYTES).rewind().getShort();}

    /**
     * @return The decoded bytes as an int value (may underflow).
     */
    public int toInt() {return this.getParsingBuffer(Integer.BYTES).rewind().getInt();}

    /**
     * @return The decoded bytes as a long value (may underflow).
     */
    public long toLong() {return this.getParsingBuffer(Long.BYTES).rewind().getLong();}


    /** === Private Methods -------------------------------------------------- **/

    protected DecodedFrame(byte[] data) {

        Objects.requireNonNull(data, "The array of decoded bytes cannot be null.");

        assert data.length > 0: "At least one byte should be decoded.";

        this.data = Arrays.copyOf(data, data.length);
        this.parsingBuffer = ByteBuffer.wrap(this.data);

        this.parsingBuffer.order(this.getByteOrdering());
    }

    /**
     * @return The raw decoded data as an array of bytes.
     */
     protected final byte[] getData() {return this.data;}

    /**
     * @return The number of raw decoded bytes in this {@link DecodedFrame}.
     */
    protected final int getDataSize() {return this.data.length;}

    /**
     * Creates a buffer from the byte values stored in this {@link DecodedFrame} such that it has at least the
     * specified number of bytes.
     *
     * @param minBufferSize The minimum size for the buffer.
     * @return The byte buffer as a {@link ByteBuffer}.
     */
    private ByteBuffer getParsingBuffer(int minBufferSize) {

        assert this.parsingBuffer.hasArray(): "The parsing buffer does not have a backing array.";

        if(this.parsingBuffer.array().length < minBufferSize) {
            this.parsingBuffer = ByteBuffer.allocate(minBufferSize).put(this.data);
            this.parsingBuffer.order(this.getByteOrdering());
        }

        return this.parsingBuffer;
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final byte[] data;
    private ByteBuffer parsingBuffer;
}
