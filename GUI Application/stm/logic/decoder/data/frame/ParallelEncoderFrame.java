package decoder.data.frame;

import java.math.BigInteger;
import java.nio.ByteOrder;

public class ParallelEncoderFrame extends DecodedFrame {

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a {@link ParallelEncoderFrame} from the specified byte array.
     *
     * @param data The data to be wrapped as a byte array.
     * @param nDecodedBits The number of bits decoded for this {@link DecodedFrame}.
     *
     * @return A {@link ParallelEncoderFrame} containing the specified data.
     */
    public static ParallelEncoderFrame from(byte[] data, int nDecodedBits) {
        return new ParallelEncoderFrame(data, nDecodedBits);
    }

    /**
     * @return The byte ordering of the data in this {@link DecodedFrame}.
     */
    @Override
    public ByteOrder getByteOrdering() {return ByteOrder.LITTLE_ENDIAN;}

    /**
     * @return THe number of bits that were decoded for this {@link DecodedFrame}.
     */
    @Override
    public int getDecodedBitCount() {return this.nDecodedBits;}

    /**
     * @return A human readable string representation of this {@link DecodedFrame}.
     */
    @Override
    public String toString() {
        return "Data: " + new BigInteger(1, this.getData()).toString(10) + ", Bits: " + this.getDecodedBitCount();
    }

    /**
     * @return A human readable string representation of this {@link DecodedFrame} in HTML format.
     */
    @Override
    public String toHTMLString() {
        return this.toString();
    }


    /** === Private Methods -------------------------------------------------- **/

    private ParallelEncoderFrame(byte[] data, int nDecodedBits) {

        super(data);
        this.nDecodedBits = nDecodedBits;
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final int nDecodedBits;
}
