package decoder.data.message;

import decoder.DataDirection;
import decoder.data.frame.DecodedFrame;
import protocols.protocol.ProtocolType;
import utility.Timestamp;

import java.util.Collection;

public class SPIMessage extends DecodedMessage {


    /** === Builder Class Definition ----------------------------------------- **/

    /**
     * Builder class for the {@link I2CMessage}.
     */
    public static class SPIMessageBuilder extends DecodedMessageBuilder {

        /**
         * Sets the index of the device associated with the resulting message.
         *
         * @param deviceIndex The index of the device.
         *
         * @return This {@link SPIMessageBuilder} object.
         */
        public SPIMessageBuilder setDeviceIndex(int deviceIndex) {
            this.deviceIndex = deviceIndex;
            return this;
        }

        @Override
        public SPIMessage build() {
            return (SPIMessage) super.build();
        }

        @Override
        public SPIMessageBuilder addDecodedFrame(DecodedFrame frame) {
            return (SPIMessageBuilder) super.addDecodedFrame(frame);
        }

        @Override
        public <T extends DecodedFrame> SPIMessageBuilder addDecodedFrames(Collection<T> frames) {
            return (SPIMessageBuilder) super.addDecodedFrames(frames);
        }

        @Override
        public SPIMessageBuilder setStartTime(Timestamp startTime) {
            return (SPIMessageBuilder) super.setStartTime(startTime);
        }

        @Override
        public SPIMessageBuilder setEndTime(Timestamp endTime) {
            return (SPIMessageBuilder) super.setEndTime(endTime);
        }

        @Override
        public SPIMessageBuilder setStartIndex(int startIndex) {
            return (SPIMessageBuilder) super.setStartIndex(startIndex);
        }

        @Override
        public SPIMessageBuilder setEndIndex(int endIndex) {
            return (SPIMessageBuilder) super.setEndIndex(endIndex);
        }

        @Override
        public SPIMessageBuilder setDataDirection(DataDirection direction) {
            return (SPIMessageBuilder) super.setDataDirection(direction);
        }

        @Override
        protected DecodedMessage buildDecodedMessage() {

            SPIMessage message = new SPIMessage();
            message.deviceIndex = this.deviceIndex;

            return message;
        }

        private SPIMessageBuilder() {}


        /** === Instance Variables ----------------------------------------------- **/

        private int deviceIndex;
    }

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * @return A builder for this {@link SPIMessage} as a {@link SPIMessageBuilder}.
     */
    public static SPIMessageBuilder newBuilder() {return new SPIMessageBuilder();}

    /**
     * @return The protocol which was used to decoded this data as a {@link ProtocolType}.
     */
    @Override
    public ProtocolType getProtocol() {return ProtocolType.SPI;}

    /**
     * @return The index of the device this message was sent to/by.
     */
    public int getDeviceIndex() {return this.deviceIndex;}


    /** === Instance Variables ----------------------------------------------- **/

    private int deviceIndex;
}
