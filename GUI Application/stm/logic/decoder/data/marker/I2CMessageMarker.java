package decoder.data.marker;

import decoder.data.message.I2CMessage.StopType;

public class I2CMessageMarker extends MessageMarker {

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a message marker for the I2C protocol from the specified parameters.
     *
     * @param startIndex The start index of the message.
     * @param endIndex The stop index of the message.
     * @param stopType The stop type of the message as a {@link StopType}.
     *
     * @return The message marker as a {@link I2CMessageMarker}.
     */
    public static I2CMessageMarker from(int startIndex, int endIndex, StopType stopType) {
        return new I2CMessageMarker(startIndex, endIndex, stopType);
    }

    /**
     * @return The stop type for this {@link I2CMessageMarker} as a {@link StopType}.
     */
    public StopType getStopType() {return this.stopType;}


    /** === Private Methods -------------------------------------------------- **/

    protected I2CMessageMarker(int startIndex, int endIndex, StopType stopType) {
        super(startIndex, endIndex);
        this.stopType = stopType;
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final StopType stopType;
}
