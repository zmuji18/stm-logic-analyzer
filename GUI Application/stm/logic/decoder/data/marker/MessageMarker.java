package decoder.data.marker;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utility class used to store start and end indices of a message.
 */
public class MessageMarker {

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Returns a list of markers outlining a single message. Some signals may be left unmatched.
     *
     * @param startSignalIndices The indices of all I2C start signals.
     * @param stopSignalIndices The indices of all I2C stop signals.
     *
     * @return A {@link List} of markers as {@link MessageMarker} objects.
     */
    public static <T extends MessageMarker> List<T> getMessageMarkers(List<Integer> startSignalIndices,
                                                                      List<Integer> stopSignalIndices,
                                                                      MessageMarkerSupplier<T> markerSupplier) {
        List<T> messageMarkers = new ArrayList<>();

        List<Signal> signals = Stream.concat(startSignalIndices.stream().map(Signal::startSignalOf),
                stopSignalIndices.stream().map(Signal::stopSignalOf))
                .sorted(Comparator.comparingInt(Signal::index)).collect(Collectors.toList());

        Signal previousSignal = null;

        for(Signal currentSignal: signals) {

            if(previousSignal != null && previousSignal.type().equals(Signal.SignalType.START)) {
                messageMarkers.add(markerSupplier.from(previousSignal, currentSignal));
            }
            previousSignal = currentSignal;
        }

        return messageMarkers;
    }

    /**
     * Creates a message marker from the specified start and stop signals.
     *
     * @param start The start {@link Signal}.
     * @param stop The stop {@link Signal}.
     *
     * @return The message marker as a {@link MessageMarker}.
     */
    public static MessageMarker from(Signal start, Signal stop) {

        assert stop.type() == Signal.SignalType.STOP: "The second signal must be a stop. Received: " + stop.type();

        return new MessageMarker(start.index(), stop.index());
    }

    /**
     * @return The index of the start sample for this message.
     */
    public int getStartIndex() {return this.startIndex;}

    /**
     * @return The index of the stop sample for this message.
     */
    public int getEndIndex() {return this.endIndex;}


    /** === Private Methods -------------------------------------------------- **/

    protected MessageMarker(int startIndex, int endIndex) {

        assert startIndex < endIndex: "The end of the message cannot precede the start.";

        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final int startIndex;
    private final int endIndex;
}
