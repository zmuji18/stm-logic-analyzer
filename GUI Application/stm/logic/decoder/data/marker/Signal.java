package decoder.data.marker;

public record Signal(int index, SignalType type) {

    public enum SignalType {START, STOP}

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a start signal from the specified index.
     *
     * @param index The index of the start signal sample.
     *
     * @return The start signal as a {@link Signal}.
     */
    public static Signal startSignalOf(int index) {
        return new Signal(index, SignalType.START);
    }

    /**
     * Creates a stop signal from the specified index.
     *
     * @param index The index of the stop signal sample.
     *
     * @return The stop signal as a {@link Signal}.
     */
    public static Signal stopSignalOf(int index) {
        return new Signal(index, SignalType.STOP);
    }
}