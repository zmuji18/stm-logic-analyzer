package decoder;

import decoder.data.frame.I2CFrame;
import decoder.data.marker.I2CMessageMarker;
import decoder.data.marker.MessageMarker;
import decoder.data.message.DecodedMessage;
import decoder.data.message.I2CMessage;
import logging.ConfiguredLogger;
import protocols.protocol.I2C;
import utility.Timestamp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static protocols.protocol.I2C.*;

public class I2CDecoder extends Decoder {

    private static final Logger logger = ConfiguredLogger.getLogger("I2CDecoder");


    /** === Interface Functions ---------------------------------------------- **/

    @Override
    public List<Integer> getUsedChannelIndices() {
        return List.of(this.getProtocol().getClockChannel(), this.getProtocol().getDataChannel());
    }

    @Override
    public List<DecodedMessage> decode(Double[] sampleTime, Map<Integer, Byte[]> sampleValues, int startIndex, int endIndex) {

        List<DecodedMessage> decodedMessages = new ArrayList<>();

        Byte[] clockChannel = sampleValues.get(this.getProtocol().getClockChannel());
        Byte[] dataChannel = sampleValues.get(this.getProtocol().getDataChannel());

        List<Integer> startSignals = getStartSignalIndices(clockChannel, dataChannel, startIndex, endIndex);
        List<Integer> stopSignals = getStopSignalIndices(clockChannel, dataChannel, startIndex, endIndex);

        List<I2CMessageMarker> messageMarkers = MessageMarker.getMessageMarkers(startSignals, stopSignals, (start, startOrStop) -> {

            I2CMessage.StopType stopType;

            switch (startOrStop.type()) {
                case START -> stopType = I2CMessage.StopType.REPEATED_START;
                case STOP -> stopType = I2CMessage.StopType.STOP;
                default -> throw new RuntimeException(startOrStop.type() + " is not a valid stop type.");
            }

            return I2CMessageMarker.from(start.index(), startOrStop.index(), stopType);
        });

        for(I2CMessageMarker messageMarker : messageMarkers) {
            decodedMessages.add(this.decodeMessage(sampleTime, clockChannel, dataChannel, messageMarker));
        }

        return decodedMessages;
    }


    /** === Private Functions ------------------------------------------------ **/

    I2CDecoder(I2C protocol) {super(protocol);}

    @Override
    protected I2C getProtocol() {return (I2C) super.getProtocol();}

    /**
     * Decodes a single message marked by the specified {@link MessageMarker}.
     *
     * @param sampleTime An array of timestamps for the samples.
     * @param clockChannel The clock channel samples.
     * @param dataChannel The data channel samples.
     * @param messageMarker The {@link I2CMessageMarker} marking the start and end of the message to decode.
     *
     * @return The decoded message as a {@link DecodedMessage} object.
     */
    private DecodedMessage decodeMessage(Double[] sampleTime, Byte[] clockChannel, Byte[] dataChannel, I2CMessageMarker messageMarker) {

        List<Integer> clockEdges = getRisingEdgeIndices(clockChannel, messageMarker.getStartIndex(), messageMarker.getEndIndex());
        clockEdges = clockEdges.subList(0, clockEdges.size() - 1); // Remove the last rising edge as it is for the stop signal.

        I2CFrame addressFrame = this.decodeAddressFrame(dataChannel, clockEdges);

        I2CMessage.I2CMessageBuilder dataBuilder = I2CMessage.newBuilder();
        dataBuilder.addDecodedFrame(addressFrame);
        dataBuilder.setDataDirection(addressFrame.getDirection());

        if(!addressFrame.getDirection().equals(DataDirection.UNKNOWN)) {

            clockEdges = clockEdges.subList(addressFrame.getDecodedBitCount(), clockEdges.size());
            final int N_CLOCK_PULSES = clockEdges.size();

            for(int i = 0; i < N_CLOCK_PULSES; i += N_BITS_PER_DATA_FRAME) {
                dataBuilder.addDecodedFrame(this.decodeDataFrame(dataChannel,
                        clockEdges.subList(i, Math.min(N_CLOCK_PULSES, i + N_BITS_PER_DATA_FRAME))));
            }
        }

        return dataBuilder.setStopType(messageMarker.getStopType())
                .setStartTime(Timestamp.of(sampleTime[messageMarker.getStartIndex()]))
                .setEndTime(Timestamp.of(sampleTime[messageMarker.getEndIndex()]))
                .setStartIndex(messageMarker.getStartIndex())
                .setEndIndex(messageMarker.getEndIndex()).build();
    }

    /**
     * Decodes the address frame from the specified array of samples.
     *
     * @param sampleValue The values of the samples an array.
     * @param sampleIndices The indices of the samples to be decoded.
     *
     * @return The decoded sample frame as an {@link I2CFrame}.
     */
    private I2CFrame decodeAddressFrame(Byte[] sampleValue, List<Integer> sampleIndices) {

        final int N_ADDRESS_BITS = this.getProtocol().getProtocolOptions().getAddressBitCount();
        final int ADDRESS_FRAME_BITS = N_ADDRESS_BITS + N_DIRECTION_BITS + N_ACK_BITS;

        if(sampleIndices.size() < ADDRESS_FRAME_BITS) {

            logger.log(Level.WARNING, "Insufficient clock pulses in address frame.\n" +
                    "Expected: " + ADDRESS_FRAME_BITS + "\n" +
                    "Received: " + sampleIndices.size());

            return I2CFrame.asAddressFrame(new byte[]{0}, false, DataDirection.UNKNOWN, N_ADDRESS_BITS);
        }


        byte[] address = new byte[(N_ADDRESS_BITS - 1)/Byte.SIZE + 1];

        for(int i = 0; i < N_ADDRESS_BITS; i++) {

            int currBit = (sampleValue[sampleIndices.get(i)] == 0 ? 0 : 1);
            int currIndex = (N_ADDRESS_BITS - i - 1) % Byte.SIZE;

            address[i/Byte.SIZE] |= (currBit << currIndex);
        }

        DataDirection direction = (sampleValue[sampleIndices.get(N_ADDRESS_BITS + N_DIRECTION_BITS - 1)] == 0) ?
                DataDirection.SEND : DataDirection.RECEIVE;

        boolean acknowledged = (sampleValue[sampleIndices.get(N_ADDRESS_BITS + N_DIRECTION_BITS + N_ACK_BITS - 1)] == 0);

        return I2CFrame.asAddressFrame(address, acknowledged, direction, N_ADDRESS_BITS);
    }

    /**
     * Decodes a raw data frame from the specified array of samples.
     *
     * @param sampleValue The values of the samples an array.
     * @param sampleIndices The indices of the samples to be decoded.
     *
     * @return The decoded sample frame as an {@link I2CFrame}.
     */
    private I2CFrame decodeDataFrame(Byte[] sampleValue, List<Integer> sampleIndices) {

        if(sampleIndices.size() != N_BITS_PER_DATA_FRAME) {
            logger.log(Level.WARNING, "The data frame should contain: " + N_BITS_PER_DATA_FRAME + " samples.\n" +
                    "Received: " + sampleIndices.size());

            return I2CFrame.asDataFrame(new byte[]{0}, false);
        }

        byte frameData = 0;

        for(int i = 0; i < Byte.SIZE; i++) {

            int currBit = (sampleValue[sampleIndices.get(i)] == 0 ? 0 : 1);
            int currIndex = Byte.SIZE - i - 1;

            frameData |= (currBit << currIndex);
        }

        boolean acknowledged = (sampleValue[sampleIndices.get(Byte.SIZE + N_ACK_BITS - 1)] == 0);

        return I2CFrame.asDataFrame(new byte[]{frameData}, acknowledged);
    }

    /**
     * Returns the indices of all I2C start signals.
     *
     * @param clockValues The sample values for the clock channel.
     * @param dataValues The sample values for the data channel.
     * @param startIndex The starting index.
     * @param endIndex The end index (exclusive).
     *
     * @return The {@link List} of indices.
     */
    private List<Integer> getStartSignalIndices(Byte[] clockValues, Byte[] dataValues, int startIndex, int endIndex) {

        return getFallingEdgeIndices(dataValues, startIndex, endIndex)
                .stream().filter(index -> clockValues[index] == 1).collect(Collectors.toList());
    }

    /**
     * Returns the indices of all I2C stop signals.
     *
     * @param clockValues The sample values for the clock channel.
     * @param dataValues The sample values for the data channel.
     * @param startIndex The starting index.
     * @param endIndex The end index (exclusive).
     *
     * @return The {@link List} of indices.
     */
    private List<Integer> getStopSignalIndices(Byte[] clockValues, Byte[] dataValues, int startIndex, int endIndex) {

        return getRisingEdgeIndices(dataValues, startIndex, endIndex)
                .stream().filter(index -> clockValues[index] == 1).collect(Collectors.toList());
    }
}
