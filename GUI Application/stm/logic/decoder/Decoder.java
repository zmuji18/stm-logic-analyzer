package decoder;

import decoder.data.message.DecodedMessage;
import protocols.protocol.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public abstract class Decoder {

    /** === Interface Functions ---------------------------------------------- **/

    /**
     * Creates a new {@link Decoder} from the specified protocol type and the channel mapping.
     *
     * @param type The protocol type for the decoder as a {@link ProtocolType}.
     * @param channelMapping The channel mapping as a {@link Map}.
     * @param protocolOptions Additional options for the protocol of the specified type as a {@link Map}.
     *
     * @return A new {@link Decoder} object of the specified type.
     */
    public static Decoder from(ProtocolType type, Map<String, Integer> channelMapping, Map<String, Object> protocolOptions) {

        Decoder decoder;
        Protocol protocol = Protocol.from(type, channelMapping, protocolOptions);

        switch (type) {
            case PARALLEL_ENCODER -> decoder = new ParallelEncoderDecoder((ParallelEncoder) protocol);
            case I2C -> decoder = new I2CDecoder((I2C) protocol);
            case SPI -> decoder = new SPIDecoder((SPI) protocol);
            // TODO: Add UART Decoder.
            default -> throw new RuntimeException(type + " is not a valid protocol type.");
        }

        return decoder;
    }

    /**
     * @return A {@link List} of indices of all channels used by this decoder.
     */
    public abstract List<Integer> getUsedChannelIndices();

    /**
     * Decodes the given samples (in the specified range) according to the specified protocol.
     *
     * @param sampleTime The time values for the samples.
     * @param sampleValues The sample values for channels as a {@link Map}.
     * @param startIndex The index of the sample from which decoding should startTime.
     * @param endIndex The index up until where the samples should be decoded (exclusive).
     *
     * @return The decoded samples as {@link List} of {@link DecodedMessage} objects.
     */
    public abstract List<DecodedMessage> decode(Double[] sampleTime, Map<Integer, Byte[]> sampleValues, int startIndex, int endIndex);

    /**
     * Decodes the given samples (starting from index 0) according to the specified protocol.
     *
     * @param sampleTime The time values for the samples.
     * @param sampleValues The sample values for channels as a {@link Map}.
     * @param endIndex The index up until where the samples should be decoded (exclusive).
     *
     * @return The decoded samples as {@link List} of {@link DecodedMessage} objects.
     */
    public List<DecodedMessage> decode(Double[] sampleTime, Map<Integer, Byte[]> sampleValues, int endIndex) {
        return this.decode(sampleTime, sampleValues, 0, endIndex);
    }

    /**
     * Decodes the given samples according to the specified protocol.
     *
     * @param sampleTime The time values for the samples.
     * @param sampleValues The sample values for channels as a {@link Map}.
     *
     * @return The decoded samples as {@link List} of {@link DecodedMessage} objects.
     */
    public List<DecodedMessage> decode(Double[] sampleTime, Map<Integer, Byte[]> sampleValues) {
        return this.decode(sampleTime, sampleValues, sampleTime.length);
    }

    /**
     * @return The channel mapping for this {@link Decoder} as a {@link Map}.
     */
    public Map<String, Integer> getChannelMapping() {
        return this.getProtocol().getChannelMapping();
    }


    /** === Private Functions ------------------------------------------------ **/

    /**
     * Creates a {@link Decoder} from the specified protocol.
     *
     * @param protocol The protocol as a {@link Protocol} object.
     */
    protected Decoder(Protocol protocol) {
        Objects.requireNonNull(this.protocol = protocol, "The protocol must be non-null.");
    }

    /**
     * @return The protocol for the decoder as a {@link Protocol} object.
     */
    protected Protocol getProtocol() {return this.protocol;}

    /**
     * Returns the indices of all rising edges in the sample values.
     *
     * @param sampleValue The array of sample values.
     * @param startIndex The starting index.
     * @param endIndex The end index (exclusive).
     *
     * @return The {@link List} of indices.
     */
    protected static List<Integer> getRisingEdgeIndices(Byte[] sampleValue, int startIndex, int endIndex) {

        List<Integer> risingEdgeIndices = new ArrayList<>();

        for(int i = startIndex + 1; i < endIndex; i++) {
            if(sampleValue[i] - sampleValue[i - 1] == 1) risingEdgeIndices.add(i);
        }
        return risingEdgeIndices;
    }

    /**
     * Returns the indices of all falling edges in the sample values.
     *
     * @param sampleValue The array of sample values.
     * @param startIndex The starting index.
     * @param endIndex The end index (exclusive).
     *
     * @return The {@link List} of indices.
     */
    protected static List<Integer> getFallingEdgeIndices(Byte[] sampleValue, int startIndex, int endIndex) {

        List<Integer> risingEdgeIndices = new ArrayList<>();

        for(int i = startIndex + 1; i < endIndex; i++) {
            if(sampleValue[i] - sampleValue[i - 1] == -1) risingEdgeIndices.add(i);
        }
        return risingEdgeIndices;
    }


    /** === Instance Variables ----------------------------------------------- **/

    private final Protocol protocol;
}
