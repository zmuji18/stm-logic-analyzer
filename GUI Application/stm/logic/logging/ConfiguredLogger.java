package logging;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.*;

public final class ConfiguredLogger {

    /**
     * Returns a preconfigured logger for this Logic Analyzer GUI application. Creates and configures a new logger
     * if one with the specified name doesn't exist.
     *
     * @param loggerName The name of the logger
     * @return A preconfigured {@link Logger} with the specified name.
     */
    public static Logger getLogger(String loggerName) {

        if(!loggers.containsKey(loggerName)) {

            Logger newLogger = Logger.getLogger(loggerName);
            newLogger.setUseParentHandlers(false);

            Handler handler = new ConsoleHandler();
            handler.setFormatter(new Formatter() {
                @Override
                public String format(LogRecord record) {

                    return "%s %s %s\n%s: %s\n".formatted(
                            DateTimeFormatter.ofPattern("LLL dd, yyyy HH:mm:ss:SSS a").format(LocalDateTime.now()),
                            record.getSourceClassName(),
                            record.getSourceMethodName(),
                            record.getLevel(),
                            record.getMessage()
                    );
                }
            });

            newLogger.addHandler(handler);

            loggers.put(loggerName, newLogger);
        }

        return loggers.get(loggerName);
    }

    /* Instances of this class aren't meant to be created */
    private ConfiguredLogger() {}

    private static Map<String, Logger> loggers;

    static {
        loggers = new HashMap<>();
    }
}
