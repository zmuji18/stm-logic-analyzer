package communication;

import boxes.NotificationBox;
import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import com.fazecast.jSerialComm.SerialPortInvalidPortException;
import configuration.PortConfiguration;
import configuration.STMConfiguration;
import data.SampleDataModel;
import logging.ConfiguredLogger;
import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;
import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Logger;

import static data.DataConstants.*;

public class STMController {

    private static final Logger logger = ConfiguredLogger.getLogger("STMController");

    private static record Task(Runnable task, int priority) implements Comparable<Task> {

        private Task {
            Objects.requireNonNull(task, "The specified task cannot be null.");
        }

        @Override
        public int compareTo(@NotNull STMController.Task other) {
            return Integer.compare(priority, other.priority);
        }
    }

    /** === Interface Functions ---------------------------------------------- **/


    /**
     * Creates a new {@link STMController} instance associated with the specified sample data model.
     *
     * @param dataModel The sample data model as a {@link SampleDataModel}.
     *
     * @return An instance of the {@link STMController}.
     *
     * @throws RuntimeException If an attempt has been made to create more than one instance of this class.
     */
    public static synchronized STMController createInstance(SampleDataModel dataModel) {

        if(instance == null) {
            instance = new STMController(dataModel);
        } else {
            throw new RuntimeException("Request to create multiple instances of the STM controller.");
        }

        return instance;
    }

    /**
     * Sends the currently specified configuration to the STM board requests that it starts sampling.
     *
     * @return True if the configuration was successfully sent and sampling has been initiated, false otherwise.
     */
    public boolean startSampling() {

        try {
            SerialPort serialPort = openSerialPort(this.getPortConfiguration());
            serialPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_BLOCKING, DATA_READ_TIMEOUT, DATA_WRITE_TIMEOUT);

            this.sendSamplingConfiguration(serialPort);
            if(!this.isAcked(serialPort)) {

                serialPort.closePort();
                throw new SerialPortInvalidPortException("No response from device.");
            }

            this.resetBuffer();
            this.createSampleDataListener(serialPort);

            this.dataModel.setSTMConfiguration(this.getSTMConfiguration());
            this.dataModel.initializeModel(this.sampleBuffer, this.sampleBufferSemaphore, this::finishProcessing);

            return true;

        } catch (SerialPortInvalidPortException | InvalidParameterException e) {

            NotificationBox.display(e.getMessage());
            return false;
        }
    }

    /**
     * Interrupts the sampling process as soon as possible.
     */
    public void stopSampling() {

        if(this.isInterrupted || this.dataModel.isComplete()) return;

        this.isInterrupted = true;

        this.dataModel.stopProcessing();
        this.sampleDataListener.serialEvent(null);
    }

    /**
     * @return The currently set port configuration as a {@link STMConfiguration} object.
     */
    public STMConfiguration getSTMConfiguration() {return this.stmConfiguration;}

    /**
     * Sets the specified STM configuration for the STM Controller.
     *
     * @param configuration The STM configuration as an {@link STMConfiguration} object.
     */
    public void setSTMConfiguration(STMConfiguration configuration) {

        assert configuration.getSamplingRate() != null : "The sampling rate must be specified.";
        assert configuration.getSamplingMemory() != null : "The total sampling memory must be satisfied.";
        assert configuration.getLogicLevel() != null : "The logic level must be specified";

        assert configuration.getSelectedChannelIndices().size() > 0 : "At least one channel must be selected.";

        this.stmConfiguration = configuration;
    }

    /**
     * @return The currently set port configuration as a {@link PortConfiguration} object.
     */
    public PortConfiguration getPortConfiguration() {return this.portConfiguration;}

    /**
     * Sets a new port configuration for the {@link STMController}.
     *
     * @param portConfiguration The new port confi
     */
    public void setPortConfiguration(PortConfiguration portConfiguration) {
        this.portConfiguration = portConfiguration;
    }

    /**
     * Schedules a task to be executed whenever a sampling error occurs.
     *
     * @param samplingErrorTask A {@link Runnable} to be executed whenever a sampling error occurs.
     * @param priority The priority of this task as an integer. Lower values correspond to higher priority.
     */
    public void addSamplingErrorTask(Runnable samplingErrorTask, int priority) {

        assert samplingErrorTask != null : "The specified runnable for the sampling error task cannot be null.";

        this.samplingErrorTasks.add(new Task(samplingErrorTask, priority));
    }

    /**
     * Schedules a task to be executed whenever sampling successfully finishes.
     *
     * @param samplingCompletionTask A {@link Runnable} to be executed whenever sampling successfully finishes.
     * @param priority The priority of this task as an integer. Lower values correspond to higher priority.
     */
    public void addSamplingCompletionTask(Runnable samplingCompletionTask, int priority) {

        assert samplingCompletionTask != null : "The specified runnable for the sampling completion task cannot be null.";

        this.samplingCompletionTasks.add(new Task(samplingCompletionTask, priority));
    }


    /** === Private Methods -------------------------------------------------- **/

    /**
     * Creates an {@link STMController} object with default configurations.
     *
     * @param dataModel The sample data model as a {@link SampleDataModel}.
     */
    private STMController(SampleDataModel dataModel) {

        this.dataModel = dataModel;

        this.stmConfiguration = null;
        this.portConfiguration = getDefaultPortConfiguration();

        this.sampleBuffer = new byte[MAX_MEMORY];

        this.samplingErrorTasks = new PriorityQueue<>();
        this.samplingCompletionTasks = new PriorityQueue<>();
    }

    /**
     * Sends the currently specified sampling configuration to the STM Board.
     *
     * @param serialPort The serial port used for exchanging data with the STM as a {@link SerialPort} object.s
     */
    private void sendSamplingConfiguration(SerialPort serialPort) {

        while(serialPort.bytesAvailable() > 0) {
            serialPort.readBytes(this.sampleBuffer, serialPort.bytesAvailable());
        }

        byte[] configurationBytes = this.getSTMConfiguration().toBytes();

        ByteBuffer startMessage = ByteBuffer.allocate(Byte.BYTES + configurationBytes.length);

        startMessage.put(START_SIGNAL);
        startMessage.put(configurationBytes);

        assert startMessage.hasArray(): "The start message has no backing array.";

        byte[] startMessageBytes = startMessage.array();
        logger.info("Sent message and configuration bytes: " + Arrays.toString(startMessageBytes));

        serialPort.writeBytes(startMessageBytes, startMessageBytes.length);
    }

    /**
     * Checks if an Acknowledgment byte has been received at the specified serial port.
     *
     * @param serialPort The serial port as a {@link SerialPort} object.
     * @return True if an Acknowledge byte was received, false otherwise.
     */
    private boolean isAcked(SerialPort serialPort) {

        ByteBuffer buffer = ByteBuffer.allocate(1);
        long timer = System.currentTimeMillis();

        while(true) {

            if (serialPort.readBytes(buffer.array(), 1) == -1) {
                logger.info("No bytes were received as acknowledgement.");
            } else {
                logger.info("Received byte while waiting for acknowledgement: " +
                        Integer.toBinaryString(buffer.array()[0]));
            }

            if(buffer.array()[0] == ACK_SIGNAL) return true;
            else if(System.currentTimeMillis() - timer >= ACKNOWLEDGEMENT_TIMEOUT) return false;
        }
    }

    /**
     * Resets the sampling buffers of this {@link STMController}.
     */
    private void resetBuffer() {

        this.nSampleBytes = 0;
        this.sampleBufferSemaphore = new Semaphore(0);
    }

    /**
     * Schedules a timer to read samples from the Serial Port at a fixed rate.
     *
     * @param serialPort The serial port ot read data from as a {@link SerialPort} object.
     */
    private void createSampleDataListener(SerialPort serialPort) {

        this.isInterrupted = false;
        int totalSamples = STMController.this.getSTMConfiguration().getSamplingMemory();

        serialPort.setComPortTimeouts(SerialPort.TIMEOUT_NONBLOCKING, 1, 1);
        serialPort.addDataListener(this.sampleDataListener = new SerialPortDataListener() {
            @Override
            public int getListeningEvents() {
                return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
            }

            @Override
            public synchronized void serialEvent(SerialPortEvent serialPortEvent) {

                try {

                    if (STMController.this.isInterrupted) {

                        if(!interruptAcknowledged.compareAndSet(false, true)) return;

                        STMController.this.sendStopSignal(serialPort);
                        throw new InterruptedException("The sampling process has been interrupted.");
                    }

                    if (!serialPort.isOpen()) return;

                    int nAvailableBytes = serialPort.bytesAvailable();
                    if (serialPort.readBytes(STMController.this.sampleBuffer, nAvailableBytes, STMController.this.nSampleBytes) == -1) {

                        if(!interruptAcknowledged.compareAndSet(false, true)) return;

                        throw new InterruptedException("Connection lost to device.");
                    }

                    STMController.this.nSampleBytes += nAvailableBytes;
                    STMController.this.sampleBufferSemaphore.release(nAvailableBytes);

                    logger.fine("Received sample packet of size: " + nAvailableBytes + " bytes.\n" +
                            "Total samples collected: " + STMController.this.nSampleBytes + " bytes.");

                    if (STMController.this.nSampleBytes == totalSamples) {

                        logger.info("Sample transfer complete. closing serial port.");

                        serialPort.closePort();
                        serialPort.removeDataListener();
                    }
                } catch (SerialPortInvalidPortException | InvalidParameterException | InterruptedException e) {

                    logger.info(e.getMessage());
                    NotificationBox.display(e.getMessage());

                    STMController.this.dataModel.stopProcessing();

                    serialPort.closePort();
                    serialPort.removeDataListener();
                }
            }
            private final AtomicBoolean interruptAcknowledged = new AtomicBoolean(false);
        });
    }

    /**
     * The callback method for the {@link SampleDataModel}. This method is invoked right after sample processing stops (
     * unless it was caused by a user triggered interrupt). The method executes other callback functions based on the
     * result of the processing.
     */
    private void finishProcessing() {

        if(this.dataModel.isComplete()) {
            this.samplingCompletionTasks.forEach(task -> task.task().run());
        } else if(!this.isInterrupted) {
            this.samplingErrorTasks.forEach(task -> task.task().run());
        }
    }

    /**
     * Signals the STM32 to stop sampling immediately.
     *
     * @param serialPort The serial port through which the signal should be sent as a {@link SerialPort} object.
     */
    private void sendStopSignal(SerialPort serialPort) {
        serialPort.writeBytes(new byte[]{STOP_SIGNAL}, 1);
    }

    /**
     * @return The default port configuration for the STM Logic Analyzer as a {@link PortConfiguration}.
     */
    private static PortConfiguration getDefaultPortConfiguration() {

        SerialPort defaultPort = Arrays.stream(SerialPort.getCommPorts())
                .filter(serialPort -> serialPort.getDescriptivePortName().equals(STM32_PORT_NAME)).findFirst().orElse(null);

        return new PortConfiguration(defaultPort == null ? null : defaultPort.getSystemPortName());
    }

    /**
     * Attempts to open a serial port according to specified port configuration.
     *
     * @param portConfiguration The serial port configuration as a {@link PortConfiguration} object.
     * @return The opened serial port as a {@link SerialPort} object.
     */
    private static SerialPort openSerialPort(PortConfiguration portConfiguration) throws InvalidParameterException, SerialPortInvalidPortException {

        String selectedPortName = portConfiguration.selectedPortName();
        if(selectedPortName == null) throw new InvalidParameterException("The serial port is not selected.");

        SerialPort selectedPort = Arrays.stream(SerialPort.getCommPorts())
            .filter(serialPort -> serialPort.getSystemPortName().equals(selectedPortName)).findFirst().orElse(null);

        if(selectedPort == null) throw new InvalidParameterException("Cannot find the serial port: " + selectedPortName);

        selectedPort.setComPortParameters(BAUD_RATE, BYTES_PER_WORD, STOP_BITS, PARITY);
        if(!selectedPort.isOpen() && !selectedPort.openPort()) {
            throw new InvalidParameterException("Can't open the serial port: " + selectedPort.getSystemPortName());
        }

        return selectedPort;
    }

    /** === Instance Variables ----------------------------------------------- **/

    private final SampleDataModel dataModel;

    private STMConfiguration stmConfiguration;
    private PortConfiguration portConfiguration;

    private final byte[] sampleBuffer;
    private int nSampleBytes;

    private Semaphore sampleBufferSemaphore;

    private SerialPortDataListener sampleDataListener;
    private boolean isInterrupted;

    private final PriorityQueue<Task> samplingErrorTasks;
    private final PriorityQueue<Task> samplingCompletionTasks;

    /** === Static Variables ------------------------------------------------- **/

    private static STMController instance;
}
