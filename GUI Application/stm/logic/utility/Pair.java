package utility;

public record Pair<T1, T2>(T1 first, T2 second) {

    public Pair<T1, T2> of(T1 first, T2 second) {return new Pair<>(first, second);}
}
