package utility;

import java.util.Objects;

/**
 * Class: Sample
 * Date Created: 11.07.21 22:39
 *
 * === Description ============================================= *
 * Objects of this class represent a single sample.
 */
public record Sample(Number x, Number y) implements Comparable<Sample> {

    /**
     * Creates a sample from the specified numbers.
     *
     * @param x The X value of the sample.
     * @param y The Y value of the sample.
     */
    public Sample {
        Objects.requireNonNull(x);
        Objects.requireNonNull(y);
    }

    /**
     * A convenience factory method for creating a sample.
     *
     * @param x The X value of the sample.
     * @param y The Y value of the sample.
     * @return A {@link Sample} object created from the specified values.
     */
    public static Sample of(Number x, Number y) {return new Sample(x, y);}

    /**
     * @return The double value of the x coordinate of the sample.
     */
    public double getX() {return this.x.doubleValue();}

    /**
     * @return The double value of the y coordinate of the sample.
     */
    public double getY() {return this.y.doubleValue();}

    @Override
    public int compareTo(Sample other) {return Double.compare(this.x.doubleValue(), other.x.doubleValue());}

    @Override
    public String toString() {
        return "(X: " + this.x + ", Y: " + this.y + ")";
    }
}
