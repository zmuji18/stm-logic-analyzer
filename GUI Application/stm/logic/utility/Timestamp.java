package utility;

import java.time.LocalTime;

public record Timestamp(long nanoSeconds) implements Comparable<Timestamp> {

    private static final int NANO_SECONDS_PER_SECOND = 1000_000_000;

    /**
     * Creates a timestamp from the specified elapsed time.
     *
     * @param nanoSeconds The elapsed time in nano seconds.
     */
    public Timestamp {
        assert nanoSeconds >= 0: "The number of elapsed nanoseconds cannot be negative.";
    }

    /**
     * Creates a new timestamp from the specified time (in nanoseconds).
     *
     * @param nanoSeconds The elapsed time in nano seconds.
     *
     * @return A {@link Timestamp} object created from the specified time.
     */
    public static Timestamp of(long nanoSeconds) {return new Timestamp(nanoSeconds);}

    /**
     * Creates a new timestamp from the specified time (in seconds).
     *
     * @param seconds The elapsed time in seconds.
     *
     * @return A {@link Timestamp} object created from the specified time.
     */
    public static Timestamp of(double seconds) {return new Timestamp((long) (seconds * NANO_SECONDS_PER_SECOND));}

    @Override
    public int compareTo(Timestamp other) {
        return Long.compare(this.nanoSeconds(), other.nanoSeconds());
    }

    @Override
    public String toString() {return LocalTime.ofNanoOfDay(nanoSeconds).toString();}
}
