package configuration;

import java.util.Objects;
import java.util.Set;

import static data.DataConstants.N_CHANNELS;

/**
 * Class: ChannelConfiguration
 * Date Created: 11.07.21 22:39
 *
 * === Description ============================================= *
 * Objects of this class represent the configuration of a single channel.
 */
public record ChannelConfiguration(int channelIndex, Trigger trigger) {

    /**
     * This enum represents the various trigger mode of a channel.
     *
     * A trigger mode specifies when data sampling will start on a particular channel. <p>
     *
     * <h3>The sampling can start on the following events:          </h3>
     *   <li>- NONE: As soon as the user presses start.             </li>
     *   <li>- RISING: After the first rising edge occurs.          </li>
     *   <li>- HIGH: After the first High signal on the channel.    </li>
     *   <li>- FALLING: After the first falling edge occurs.        </li>
     *   <li>- LOW: After the first Low signal on the channel.      </li>
     */
    public enum Trigger {
        NONE(0),
        RISING(1),
        HIGH(2),
        FALLING(3),
        LOW(4);

        public int getValue() {return this.value;}

        /**
         * @return True if the specified trigger is an edge condition (i.e. A rising/falling edge trigger).
         */
        public boolean isEdgeCondition() {
            return EDGE_CONDITIONS.contains(this);
        }

        Trigger(int value) {this.value = value;}

        private final int value;

        private static final Set<Trigger> EDGE_CONDITIONS;

        static {
            EDGE_CONDITIONS = Set.of(RISING, FALLING);
        }
    }

    /**
     * Creates a channel configuration record with the specified parameters.
     *
     * @param channelIndex The index of the channel on the STM Microprocessor
     * @param trigger The trigger mode for the channel.
     */
    public ChannelConfiguration {

        assert 0 < channelIndex && channelIndex <= N_CHANNELS: channelIndex + " is not a valid channel index.";
        Objects.requireNonNull(trigger);
    }

    /**
     * Returns this configuration as a sequence of bytes.
     *
     * <h3>Format</h3>
     *  <li>Byte 1 - The channel index.</li>
     *  <li>Byte 2 - The channel's state (selected/not selected).</li>
     *  <li>Byte 3 - The channel's trigger mode.</li>
     *
     * @return The configuration as an array of bytes.
     */
    public byte[] toBytes() {

        return new byte[]{
                (byte) this.channelIndex(),
                (byte) this.trigger().getValue()
        };
    }


    /** === Static Variables ------------------------------------------------- **/

    public static final int BYTES;

    static {
        BYTES = new ChannelConfiguration(1, Trigger.NONE).toBytes().length;
    }
}
