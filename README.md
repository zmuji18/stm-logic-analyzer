# STM32 Based Digital Logic Analyzer

This repository contains the source code for the GUI application for the Digital Logic Analyzer, as well as the code for the STM Microcontroller.

### Additional information can be found in the following directories:
1. **GUI Application** - contains the source code for the desktop application.
2. **STM Logic Analyzer** - contains the STMCubeIDE project and source code for the STM Microcontroller.
