/*
 * volatile_mem_utilities.h
 *
 *  Created on: Aug 27, 2021
 *      Author: zuka
 */

#ifndef INC_VOLATILE_MEM_UTILITIES_H_
#define INC_VOLATILE_MEM_UTILITIES_H_

#include "main.h"

/**
 *	Copies the specified number of bytes from the source address to the destination address.
 *
 *	@param destination - The address of the memory location where the bytes should be copied to.
 *	@oaram source - The address of the memory location where the bytes should be copied from.
 *	@param n_bytes - The number of bytes to be copied.
 *
 *	@retval None.
 */
void Volatile_Copy(volatile void *destination, volatile void *source, size_t n_bytes);

/**
 *  Copies the specified value into the following n_bytes of memory starting from the given address.
 *
 *  @param address - The address of the start of the memory block.
 *  @param value - The value used to fill the memory block.
 *  @param n_bytes - The number of bytes to fill.
 *
 *  @retval None.
 */
void Volatile_Set(volatile void *address, uint8_t value, size_t n_bytes);

#endif /* INC_VOLATILE_MEM_UTILITIES_H_ */
