/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.h
 * @brief          : Header for main.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
#define OVERFLOW_ROOM 100
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
#define DEBUG_ENABLED
/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/**
 * Processes the data received via CDC USB.
 *
 * @param buffer: The byte array where the received data is stored.
 * @param n_data_bytes: The number of bytes received.
 */
void Process_USB_Data(uint8_t *buffer, uint32_t n_data_bytes);

/**
 * Checks channel inputs with HIGH/LOW triggers to see if the conditions
 * are satisfied. This function is called from the IRQ of TIM5.
 *
 * @param None.
 * @retval None.
 */
void Check_Triggers();

/**
 * Starts sampling channels and transferring the data to the GUI application.
 * This function is called from the IRQ of TIM2.
 *
 * @param None.
 * @retval None.
 */
void Sample_Channels();

/**
 * Finalizes the reconfiguration process of the microcontroller. Calling this
 * function will move the STM32 to a state where it waits for trigger conditions
 * to be satisfied.
 *
 * @param None.
 * @retval None.
 */
void Complete_Reconfiguration();

/**
 * Transfers the current sample information present in the sample buffer to the GUI application.
 *
 * @param None.
 * @retval None.
 */
uint8_t Transfer_Buffer();
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define DATA_TRANSFER_PRESCALER 8400
#define CONFIGURATION_DELAY_PRESCALER 8400
#define CHANNEL_3_Pin GPIO_PIN_2
#define CHANNEL_3_GPIO_Port GPIOE
#define CHANNEL_4_Pin GPIO_PIN_3
#define CHANNEL_4_GPIO_Port GPIOE
#define CHANNEL_5_Pin GPIO_PIN_4
#define CHANNEL_5_GPIO_Port GPIOE
#define CHANNEL_6_Pin GPIO_PIN_5
#define CHANNEL_6_GPIO_Port GPIOE
#define CHANNEL_7_Pin GPIO_PIN_6
#define CHANNEL_7_GPIO_Port GPIOE
#define PC14_OSC32_IN_Pin GPIO_PIN_14
#define PC14_OSC32_IN_GPIO_Port GPIOC
#define PC15_OSC32_OUT_Pin GPIO_PIN_15
#define PC15_OSC32_OUT_GPIO_Port GPIOC
#define PH0_OSC_IN_Pin GPIO_PIN_0
#define PH0_OSC_IN_GPIO_Port GPIOH
#define PH1_OSC_OUT_Pin GPIO_PIN_1
#define PH1_OSC_OUT_GPIO_Port GPIOH
#define OTG_FS_PowerSwitchOn_Pin GPIO_PIN_0
#define OTG_FS_PowerSwitchOn_GPIO_Port GPIOC
#define BOOT1_Pin GPIO_PIN_2
#define BOOT1_GPIO_Port GPIOB
#define CHANNEL_8_Pin GPIO_PIN_7
#define CHANNEL_8_GPIO_Port GPIOE
#define CHANNEL_9_Pin GPIO_PIN_8
#define CHANNEL_9_GPIO_Port GPIOE
#define CHANNEL_10_Pin GPIO_PIN_9
#define CHANNEL_10_GPIO_Port GPIOE
#define CHANNEL_11_Pin GPIO_PIN_10
#define CHANNEL_11_GPIO_Port GPIOE
#define CHANNEL_12_Pin GPIO_PIN_11
#define CHANNEL_12_GPIO_Port GPIOE
#define IDLE_LED_Pin GPIO_PIN_12
#define IDLE_LED_GPIO_Port GPIOD
#define CONFIGURING_LED_Pin GPIO_PIN_13
#define CONFIGURING_LED_GPIO_Port GPIOD
#define ERROR_LED_Pin GPIO_PIN_14
#define ERROR_LED_GPIO_Port GPIOD
#define SAMPLING_LED_Pin GPIO_PIN_15
#define SAMPLING_LED_GPIO_Port GPIOD
#define VBUS_FS_Pin GPIO_PIN_9
#define VBUS_FS_GPIO_Port GPIOA
#define OTG_FS_ID_Pin GPIO_PIN_10
#define OTG_FS_ID_GPIO_Port GPIOA
#define OTG_FS_DM_Pin GPIO_PIN_11
#define OTG_FS_DM_GPIO_Port GPIOA
#define OTG_FS_DP_Pin GPIO_PIN_12
#define OTG_FS_DP_GPIO_Port GPIOA
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define OTG_FS_OverCurrent_Pin GPIO_PIN_5
#define OTG_FS_OverCurrent_GPIO_Port GPIOD
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define CHANNEL_1_Pin GPIO_PIN_0
#define CHANNEL_1_GPIO_Port GPIOE
#define CHANNEL_2_Pin GPIO_PIN_1
#define CHANNEL_2_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
