/*
 * volatile_mem_utilities.c
 *
 *  Created on: Aug 27, 2021
 *      Author: zuka
 */

#include "volatile_mem_utilities.h"

void Volatile_Copy(volatile void *destination, volatile void *source, size_t n_bytes) {

	for(int i = 0; i < n_bytes; i++) {
		*((volatile char *) destination + i) = *((volatile char *) source + i);
	}
}

void Volatile_Set(volatile void *address, uint8_t value, size_t n_bytes) {

	for(int i = 0; i < n_bytes; i++) {
		*((volatile char *) address + i) = value;
	}
}
