/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usb_device.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usbd_cdc_if.h"
#include "string.h"
#include "strings.h"
#include "volatile_mem_utilities.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/**
 * Represents a trigger mode for a particular channel.
 */
typedef enum {
	NONE = 0, RISING = 1, HIGH = 2, FALLING = 3, LOW = 4
} Trigger_Mode;

/**
 * Stores configuration information about general sampling parameters.
 */
typedef struct {
	double sampling_rate;
	uint32_t sampling_memory;
	uint8_t logic_level;
	uint8_t n_channels;
} General_ConfigurationTypeDef;

/**
 * Stores configuration information about channel specific sampling parameters.
 */
typedef struct {
	uint8_t channel_index;
	uint8_t trigger_mode;
} Channel_ConfigurationTypeDef;

/**
 * Represents the various possible states of the microcontroller.
 */
typedef enum {
	IDLE, CONFIGURING, WAITING, SAMPLING
} State;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define N_CHANNELS 12

#define MAX_PIN_NUMBER 16

#define START ((uint8_t) 1)
#define STOP ((uint8_t) 0)

#define ACK_BYTE ((uint8_t) 1)

#define GENERAL_CONFIG_BYTES ((size_t) 14)

#define MAIN_TIMER_CLOCK_SPEED ((uint32_t) 84000000)
#define DELAY_TIMER_CLOCK_SPEED ((uint32_t) MAIN_TIMER_CLOCK_SPEED/CONFIGURATION_DELAY_PRESCALER)
#define DATA_TRANSMIT_TIMER_CLOCK_SPEED ((uint32_t) MAIN_TIMER_CLOCK_SPEED/DATA_TRANSFER_PRESCALER)

#define BITS_PER_BYTE 8

#define MS_PER_SECOND 1000
#define CONFIGURATION_DELAY 10
#define DATA_TRANSMIT_DELAY 25

#define UNKNOWN_VALUE ((GPIO_PinState)-1)
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
DAC_HandleTypeDef hdac;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim5;
TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim7;

/* USER CODE BEGIN PV */
/** Data received over USB CDC is stored in this buffer. */
extern uint8_t UserRxBufferFS[APP_RX_DATA_SIZE];

/** Data to send over USB CDC are stored in this buffer. */
extern uint8_t UserTxBufferFS[APP_TX_DATA_SIZE + OVERFLOW_ROOM];
volatile uint8_t backup_buffer[APP_TX_DATA_SIZE + OVERFLOW_ROOM];

// Mapping of channel indices to their GPIO Ports.
GPIO_TypeDef *const CHANNEL_PORTS[] = { NULL,
CHANNEL_1_GPIO_Port,
CHANNEL_2_GPIO_Port,
CHANNEL_3_GPIO_Port,
CHANNEL_4_GPIO_Port,
CHANNEL_5_GPIO_Port,
CHANNEL_6_GPIO_Port,
CHANNEL_7_GPIO_Port,
CHANNEL_8_GPIO_Port,
CHANNEL_9_GPIO_Port,
CHANNEL_10_GPIO_Port,
CHANNEL_11_GPIO_Port,
CHANNEL_12_GPIO_Port, };

// Mapping of channel indices to their GPIO Pin Numbers.
uint16_t const CHANNEL_PINS[] = { 0,
CHANNEL_1_Pin,
CHANNEL_2_Pin,
CHANNEL_3_Pin,
CHANNEL_4_Pin,
CHANNEL_5_Pin,
CHANNEL_6_Pin,
CHANNEL_7_Pin,
CHANNEL_8_Pin,
CHANNEL_9_Pin,
CHANNEL_10_Pin,
CHANNEL_11_Pin,
CHANNEL_12_Pin, };

// Array of all used EXTI lines.
uint16_t const INTERRUPT_LINES[] = { EXTI0_IRQn, EXTI1_IRQn, EXTI2_IRQn,
		EXTI3_IRQn, EXTI4_IRQn, EXTI9_5_IRQn, EXTI15_10_IRQn, };

// Total number of interrupt lines in EXTI lines array.
const uint8_t N_INTERRUPT_LINES = sizeof(INTERRUPT_LINES) / sizeof(uint16_t);

// Holds the currently set general and channel specific sampling configurations.
volatile General_ConfigurationTypeDef general_configuration;
volatile Channel_ConfigurationTypeDef channel_configurations[N_CHANNELS];

// A mapping of channel indices to their trigger condition state. Whenever a trigger condition is satisfied,
// the index of the corresponding channel will be updated with a non-zero value and vice versa.
// The number of trigger conditions that are currently satisfied is also stored.
volatile uint8_t trigger_condition_satisfied[N_CHANNELS + 1];
volatile uint8_t n_satisfied_triggers;

// Hold the last checked pin state for a given channel index.
volatile GPIO_PinState previous_channel_state[N_CHANNELS + 1];

// Stores the indices of all selected channels in ascending order.
volatile uint8_t selected_channel_indices[N_CHANNELS];

volatile State current_state;

volatile uint8_t *sample_data;
volatile uint32_t n_bytes_remaining;
volatile uint16_t n_buffered_bytes;
volatile uint32_t n_lost_packets;

volatile uint8_t last_bit_index;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM5_Init(void);
static void MX_TIM6_Init(void);
static void MX_TIM7_Init(void);
static void MX_DAC_Init(void);
/* USER CODE BEGIN PFP */
static uint8_t Send_Acknowledgement();
static void Update_Sampling_State();
static void Set_State(State new_state);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void) {
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */
	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_USB_DEVICE_Init();
	MX_TIM2_Init();
	MX_TIM5_Init();
	MX_TIM6_Init();
	MX_TIM7_Init();
	MX_DAC_Init();
	/* USER CODE BEGIN 2 */
	Set_State(IDLE);
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1) {
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/** Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 336;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 7;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}
	/** Initializes the CPU, AHB and APB buses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK) {
		Error_Handler();
	}
}

/**
 * @brief DAC Initialization Function
 * @param None
 * @retval None
 */
static void MX_DAC_Init(void) {

	/* USER CODE BEGIN DAC_Init 0 */

	/* USER CODE END DAC_Init 0 */

	DAC_ChannelConfTypeDef sConfig = { 0 };

	/* USER CODE BEGIN DAC_Init 1 */

	/* USER CODE END DAC_Init 1 */
	/** DAC Initialization
	 */
	hdac.Instance = DAC;
	if (HAL_DAC_Init(&hdac) != HAL_OK) {
		Error_Handler();
	}
	/** DAC channel OUT1 config
	 */
	sConfig.DAC_Trigger = DAC_TRIGGER_NONE;
	sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
	if (HAL_DAC_ConfigChannel(&hdac, &sConfig, DAC_CHANNEL_1) != HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN DAC_Init 2 */

	/* USER CODE END DAC_Init 2 */

}

/**
 * @brief TIM2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM2_Init(void) {

	/* USER CODE BEGIN TIM2_Init 0 */

	/* USER CODE END TIM2_Init 0 */

	TIM_ClockConfigTypeDef sClockSourceConfig = { 0 };
	TIM_MasterConfigTypeDef sMasterConfig = { 0 };

	/* USER CODE BEGIN TIM2_Init 1 */

	/* USER CODE END TIM2_Init 1 */
	htim2.Instance = TIM2;
	htim2.Init.Prescaler = 0;
	htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim2.Init.Period = 0;
	htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim2) != HAL_OK) {
		Error_Handler();
	}
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK) {
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig)
			!= HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN TIM2_Init 2 */

	/* USER CODE END TIM2_Init 2 */

}

/**
 * @brief TIM5 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM5_Init(void) {

	/* USER CODE BEGIN TIM5_Init 0 */

	/* USER CODE END TIM5_Init 0 */

	TIM_ClockConfigTypeDef sClockSourceConfig = { 0 };
	TIM_MasterConfigTypeDef sMasterConfig = { 0 };

	/* USER CODE BEGIN TIM5_Init 1 */

	/* USER CODE END TIM5_Init 1 */
	htim5.Instance = TIM5;
	htim5.Init.Prescaler = 0;
	htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim5.Init.Period = 0;
	htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim5) != HAL_OK) {
		Error_Handler();
	}
	sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	if (HAL_TIM_ConfigClockSource(&htim5, &sClockSourceConfig) != HAL_OK) {
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig)
			!= HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN TIM5_Init 2 */

	/* USER CODE END TIM5_Init 2 */

}

/**
 * @brief TIM6 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM6_Init(void) {

	/* USER CODE BEGIN TIM6_Init 0 */

	/* USER CODE END TIM6_Init 0 */

	TIM_MasterConfigTypeDef sMasterConfig = { 0 };

	/* USER CODE BEGIN TIM6_Init 1 */

	/* USER CODE END TIM6_Init 1 */
	htim6.Instance = TIM6;
	htim6.Init.Prescaler = CONFIGURATION_DELAY_PRESCALER - 1;
	htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim6.Init.Period = 0;
	htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim6) != HAL_OK) {
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig)
			!= HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN TIM6_Init 2 */

	/* USER CODE END TIM6_Init 2 */

}

/**
 * @brief TIM7 Initialization Function
 * @param None
 * @retval None
 */
static void MX_TIM7_Init(void) {

	/* USER CODE BEGIN TIM7_Init 0 */

	/* USER CODE END TIM7_Init 0 */

	TIM_MasterConfigTypeDef sMasterConfig = { 0 };

	/* USER CODE BEGIN TIM7_Init 1 */

	/* USER CODE END TIM7_Init 1 */
	htim7.Instance = TIM7;
	htim7.Init.Prescaler = DATA_TRANSFER_PRESCALER - 1;
	htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
	htim7.Init.Period = 0;
	htim7.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
	if (HAL_TIM_Base_Init(&htim7) != HAL_OK) {
		Error_Handler();
	}
	sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig)
			!= HAL_OK) {
		Error_Handler();
	}
	/* USER CODE BEGIN TIM7_Init 2 */

	/* USER CODE END TIM7_Init 2 */

}

/**
 * @brief GPIO Initialization Function
 * @param None
 * @retval None
 */
static void MX_GPIO_Init(void) {
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOE_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOH_CLK_ENABLE();
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(OTG_FS_PowerSwitchOn_GPIO_Port, OTG_FS_PowerSwitchOn_Pin,
			GPIO_PIN_SET);

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOD,
	IDLE_LED_Pin | CONFIGURING_LED_Pin | ERROR_LED_Pin | SAMPLING_LED_Pin,
			GPIO_PIN_RESET);

	/*Configure GPIO pins : CHANNEL_3_Pin CHANNEL_4_Pin CHANNEL_5_Pin CHANNEL_6_Pin
	 CHANNEL_7_Pin CHANNEL_8_Pin CHANNEL_9_Pin CHANNEL_10_Pin
	 CHANNEL_11_Pin CHANNEL_12_Pin CHANNEL_1_Pin CHANNEL_2_Pin */
	GPIO_InitStruct.Pin = CHANNEL_3_Pin | CHANNEL_4_Pin | CHANNEL_5_Pin
			| CHANNEL_6_Pin | CHANNEL_7_Pin | CHANNEL_8_Pin | CHANNEL_9_Pin
			| CHANNEL_10_Pin | CHANNEL_11_Pin | CHANNEL_12_Pin | CHANNEL_1_Pin
			| CHANNEL_2_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

	/*Configure GPIO pin : OTG_FS_PowerSwitchOn_Pin */
	GPIO_InitStruct.Pin = OTG_FS_PowerSwitchOn_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(OTG_FS_PowerSwitchOn_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pin : BOOT1_Pin */
	GPIO_InitStruct.Pin = BOOT1_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(BOOT1_GPIO_Port, &GPIO_InitStruct);

	/*Configure GPIO pins : IDLE_LED_Pin CONFIGURING_LED_Pin ERROR_LED_Pin SAMPLING_LED_Pin */
	GPIO_InitStruct.Pin = IDLE_LED_Pin | CONFIGURING_LED_Pin | ERROR_LED_Pin
			| SAMPLING_LED_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	/*Configure GPIO pin : OTG_FS_OverCurrent_Pin */
	GPIO_InitStruct.Pin = OTG_FS_OverCurrent_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(OTG_FS_OverCurrent_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/**
 * Sets a new state for the STM32 microcontroller. ALso turns on/off the appropriate
 * LEDs for debugging aid.
 */
static void Set_State(State new_state) {

	switch (new_state) {

	case IDLE:

		HAL_GPIO_WritePin(IDLE_LED_GPIO_Port, IDLE_LED_Pin, GPIO_PIN_SET);

		HAL_GPIO_WritePin(CONFIGURING_LED_GPIO_Port, CONFIGURING_LED_Pin,
				GPIO_PIN_RESET);
		HAL_GPIO_WritePin(SAMPLING_LED_GPIO_Port, SAMPLING_LED_Pin,
				GPIO_PIN_RESET);
		break;
	case CONFIGURING:

		if (current_state != IDLE) {
			Error_Handler();
		}

		HAL_GPIO_WritePin(CONFIGURING_LED_GPIO_Port, CONFIGURING_LED_Pin,
				GPIO_PIN_SET);
		break;
	case WAITING:

		if (current_state != CONFIGURING) {
			Error_Handler();
		}

		HAL_GPIO_WritePin(IDLE_LED_GPIO_Port, IDLE_LED_Pin, GPIO_PIN_RESET);
		break;
	case SAMPLING:
		if (current_state != WAITING && current_state != CONFIGURING) {
			Error_Handler();
		}

		HAL_GPIO_WritePin(SAMPLING_LED_GPIO_Port, SAMPLING_LED_Pin,
				GPIO_PIN_SET);

		HAL_GPIO_WritePin(IDLE_LED_GPIO_Port, IDLE_LED_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(CONFIGURING_LED_GPIO_Port, CONFIGURING_LED_Pin,
				GPIO_PIN_RESET);
		break;
	default:
		Error_Handler();
	}

	current_state = new_state;
}

/**
 * Sends a single byte to the GUI application to confirm that the microcontroller has
 * received the sampling configuration correctly.
 *
 * @param None.
 * @retval None.
 */
static uint8_t Send_Acknowledgement() {

	static uint8_t usb_buffer[] = { ACK_BYTE };

	return CDC_Transmit_FS(usb_buffer, 1);
}

/**
 * Parses the configuration information from the specified sequence of bytes.
 *
 * @param STM_configuration - The configuration bytes for the STM.
 * @param n_bytes - The Number of configuration bytes.
 *
 * @retval None.
 */
static void Parse_Configuration(uint8_t *STM_configuration, uint32_t n_bytes) {

	Set_State(CONFIGURING);

	if (n_bytes < GENERAL_CONFIG_BYTES) {
		Error_Handler();
	} else if (n_bytes % 2 != 0) {
		Error_Handler();
	}

	Volatile_Copy(&general_configuration, STM_configuration,
	GENERAL_CONFIG_BYTES);

	Volatile_Copy(channel_configurations,
			STM_configuration + GENERAL_CONFIG_BYTES,
			n_bytes - GENERAL_CONFIG_BYTES);

	if (general_configuration.n_channels > N_CHANNELS) {
		Error_Handler();
	}
}

/**
 * Resets the counter for the specified timer and then starts it.
 *
 * @param timer The handle for the specified timer.
 *
 * @retval None.
 */
static void Start_Timer(volatile TIM_HandleTypeDef *timer) {

	__HAL_TIM_CLEAR_IT(timer, TIM_IT_UPDATE);
	__HAL_TIM_SET_COUNTER(timer, 0);

	HAL_TIM_Base_Start_IT((TIM_HandleTypeDef*) timer);
}

/**
 * Stops the specified timer, resets the count and clears any pending interrupts.
 *
 * @param timer The handle for the specified timer.
 *
 * @retval None.
 */
static void Stop_Timer(volatile TIM_HandleTypeDef *timer) {

	HAL_TIM_Base_Stop_IT((TIM_HandleTypeDef*) timer);

	__HAL_TIM_CLEAR_IT(timer, TIM_IT_UPDATE);
	__HAL_TIM_SET_COUNTER(timer, 0);
}

/**
 * Resets all data about the sampling progress.
 *
 * @param None.
 * @retval None.
 */
static void Reset_Sampling_Data() {

	// Reset memory usage counter.
	n_bytes_remaining = general_configuration.sampling_memory;
	n_buffered_bytes = 0;
	n_lost_packets = 0;

	last_bit_index = 0;

	sample_data = UserTxBufferFS;
	sample_data[0] = 0;
}

/**
 * Configures GPIO pins and pin interrupts according to specific channel configurations.
 *
 * @param None.
 * @retval None.
 */
static void Configure_GPIO_Pins() {

	// De-initialize GPIO Pins.
	for (uint8_t i = 1; i <= N_CHANNELS; i++) {
		HAL_GPIO_DeInit(CHANNEL_PORTS[i], CHANNEL_PINS[i]);
	}

	// Initialize GPIO Pins
	GPIO_InitTypeDef GPIO_InitStruct = { 0 };

	for (int i = 0; i < general_configuration.n_channels; i++) {

		uint8_t channel_index = channel_configurations[i].channel_index;

		GPIO_TypeDef *port = CHANNEL_PORTS[channel_index];
		uint16_t pin = CHANNEL_PINS[channel_index];

		GPIO_InitStruct.Pin = pin;
		GPIO_InitStruct.Pull = GPIO_PULLDOWN;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Mode = GPIO_MODE_INPUT;

		previous_channel_state[channel_index] = UNKNOWN_VALUE;

		HAL_GPIO_Init(port, &GPIO_InitStruct);
	}
}

/**
 * Configures timers TIM2 and TIM5 for trigger checking and sampling according to the
 * currently set configuration data.
 *
 * @param None.
 * @retval None.
 */
static void Configure_Timers() {

	Stop_Timer(&htim2);
	Stop_Timer(&htim5);
	Stop_Timer(&htim6);
	Stop_Timer(&htim7);

	HAL_TIM_Base_DeInit(&htim2);
	HAL_TIM_Base_DeInit(&htim5);
	HAL_TIM_Base_DeInit(&htim6);
	HAL_TIM_Base_DeInit(&htim7);

	// Configure Timer Max values according to the sample rate.

	htim2.Init.Period = MAIN_TIMER_CLOCK_SPEED
			/ general_configuration.sampling_rate;

	htim5.Init.Period = MAIN_TIMER_CLOCK_SPEED
			/ general_configuration.sampling_rate;

	htim6.Init.Period = DELAY_TIMER_CLOCK_SPEED * CONFIGURATION_DELAY
			/ MS_PER_SECOND;
	htim7.Init.Period = DATA_TRANSMIT_TIMER_CLOCK_SPEED * DATA_TRANSMIT_DELAY
			/ MS_PER_SECOND;

	if (HAL_TIM_Base_Init(&htim2) != HAL_OK
			|| HAL_TIM_Base_Init(&htim5) != HAL_OK
			|| HAL_TIM_Base_Init(&htim6) != HAL_OK
			|| HAL_TIM_Base_Init(&htim7) != HAL_OK) {
		Error_Handler();
	}
}

/**
 * Configures several GPIO Pins to control a resistor ladder DAC to set the
 * appropriate logic level reference voltage.
 *
 * @param None.
 * @retval None.
 */
static void Configure_Logic_Level() {

	HAL_DAC_Start(&hdac, DAC1_CHANNEL_1);
	HAL_DAC_SetValue(&hdac, DAC1_CHANNEL_1, DAC_ALIGN_8B_R,
			general_configuration.logic_level);
}

/**
 * Sorts the indices of selected channels and stores them in a global array
 * @selected_channels.
 *
 * @param None.
 * @retval None.
 */
static void Sort_Selected_Channels() {

	uint8_t channel_is_selected[N_CHANNELS + 1] = { 0 };

	for (int i = 0; i < general_configuration.n_channels; i++) {

		uint8_t channel_index = channel_configurations[i].channel_index;
		channel_is_selected[channel_index] = 1;
	}

	uint8_t channel_count = 0;
	for (int i = 1; i <= N_CHANNELS; i++) {
		if (channel_is_selected[i]) {

			selected_channel_indices[channel_count] = i;
			channel_count++;
		}
	}
}

/**
 * Sets up the GPIO Pins and Timers according to the currently set sampling configuration and
 * starts sampling.
 *
 * @param None.
 * @retval None.
 */
static void Start_Sampling() {

	Reset_Sampling_Data();

	Configure_GPIO_Pins();

	Configure_Timers();

	Configure_Logic_Level();

	Sort_Selected_Channels();

	// Start the configuration delay timer.
	Start_Timer(&htim6);
}

/**
 * Disables all peripheral devices used for sampling and moves the STM to
 * an idle state.
 *
 * @param None.
 * @retval None.
 */
static void Stop_Sampling() {

	// Stop data transfer timer.
	Stop_Timer(&htim7);

	// Stop configuration delay timer.
	Stop_Timer(&htim6);

	// Stop timer that checks trigger conditions.
	Stop_Timer(&htim5);

	// Stop timer that samples and transfers data.
	Stop_Timer(&htim2);

	HAL_DAC_Stop(&hdac, DAC1_CHANNEL_1);

	Set_State(IDLE);
}

/**
 * Processes the most recently received information sent to the STM via USB.
 *
 * The data should contain:
 * 		1. A request to start sampling and the sampling configuration.
 * 		2. A request to stop sampling immediately.
 *
 * @param buffer - The byte array containing the received data.
 * @param n_data_bytes - The number of bytes in the received data.
 *
 * @retval None.
 */
void Process_USB_Data(uint8_t *buffer, uint32_t n_data_bytes) {

	uint8_t header_byte = buffer[0];

	if (header_byte == START) {

		if (current_state != IDLE) {
			Stop_Sampling();
		}

		Parse_Configuration(buffer + 1, n_data_bytes - 1);
		Start_Sampling();

	} else if (header_byte == STOP) {

		if (current_state != IDLE) {
			Stop_Sampling();
		}
	} else {
		Error_Handler();
	}
}

/**
 * Updates the STM configuration to start data transfer if all
 * trigger conditions have been satisfied. Also disables GPIO Interrupts
 * and TIM5 IRQ.
 *
 * @param None.
 * @retval None.
 */
static void Update_Sampling_State() {

	if (n_satisfied_triggers == general_configuration.n_channels) {

		// Stop timer that checks trigger conditions.
		Stop_Timer(&htim5);

		// Start timers for sampling and data transfer.
		Start_Timer(&htim2);
		Start_Timer(&htim7);

		Set_State(SAMPLING);
	}
}

/**
 * Callback function for IRQ of TIM7.
 */
uint8_t Transfer_Buffer() {

	uint8_t cdc_is_free = CDC_IsFreeToTransmit();

	if (cdc_is_free && n_buffered_bytes > 0) {

		if (CDC_Transmit_FS((uint8_t*) sample_data, n_buffered_bytes)) {
			n_lost_packets++;
		}

		if (sample_data == backup_buffer) {

			UserTxBufferFS[0] = backup_buffer[n_buffered_bytes];
			sample_data = UserTxBufferFS;
		} else {

			backup_buffer[0] = UserTxBufferFS[n_buffered_bytes];
			sample_data = backup_buffer;
		}

		n_buffered_bytes = 0;

		return 1;
	}

	return 0;
}

/**
 * Callback function for IRQ of TIM6.
 */
void Complete_Reconfiguration() {

	if (Send_Acknowledgement() != USBD_OK) {
		return;
	}

	// Stop the reconfiguration (this) timer.
	Stop_Timer(&htim6);

	Set_State(WAITING);

	// Configure the voltage on the DAC.
	HAL_DAC_SetValue(&hdac, DAC1_CHANNEL_1, DAC_ALIGN_12B_R, 0xfff * 3.0 / 5);

	// Start timer that checks trigger conditions.
	Start_Timer(&htim5);
}

/**
 * Callback function for IRQ of TIM5.
 */
void Check_Triggers() {

	n_satisfied_triggers = 0;

	for (int i = 0; i < general_configuration.n_channels; i++) {

		uint8_t channel_index = channel_configurations[i].channel_index;
		Trigger_Mode trigger_mode = channel_configurations[i].trigger_mode;

		GPIO_TypeDef *port = CHANNEL_PORTS[channel_index];
		uint16_t pin = CHANNEL_PINS[channel_index];

		GPIO_PinState actual_state = HAL_GPIO_ReadPin(port, pin);

		uint8_t trigger_satisfied;

		switch (trigger_mode) {

		case NONE:
			trigger_satisfied = 1;
			break;
		case RISING:
			trigger_satisfied = (previous_channel_state[channel_index]
					== GPIO_PIN_RESET && actual_state == GPIO_PIN_SET);
			break;
		case HIGH:
			trigger_satisfied = actual_state == GPIO_PIN_SET;
			break;
		case FALLING:
			trigger_satisfied = (previous_channel_state[channel_index]
					== GPIO_PIN_SET && actual_state == GPIO_PIN_RESET);
			break;
		case LOW:
			trigger_satisfied = actual_state == GPIO_PIN_RESET;
			break;
		default:
			Error_Handler();
		}

		trigger_condition_satisfied[channel_index] = trigger_satisfied;
		previous_channel_state[channel_index] = actual_state;

		if (trigger_condition_satisfied[channel_index]) {
			n_satisfied_triggers++;
		}
	}

	Update_Sampling_State();
}

/**
 * Callback function for IRQ of TIM2.
 */
void Sample_Channels() {

	if (n_bytes_remaining > 0 && n_buffered_bytes < APP_TX_DATA_SIZE) {

		for (uint8_t i = 0; i < general_configuration.n_channels; i++) {

			uint8_t channel_index = selected_channel_indices[i];

			GPIO_TypeDef *port = CHANNEL_PORTS[channel_index];
			uint16_t pin = CHANNEL_PINS[channel_index];

			GPIO_PinState channel_state = HAL_GPIO_ReadPin(port, pin);

			if (channel_state == GPIO_PIN_SET) {
				sample_data[n_buffered_bytes] |= (1 << last_bit_index);
			}

			last_bit_index++;

			if (last_bit_index == BITS_PER_BYTE) {

				last_bit_index = 0;

				n_buffered_bytes++;
				sample_data[n_buffered_bytes] = 0;

				n_bytes_remaining--;

				if (n_bytes_remaining == 0) {
					break;
				}
			}
		}
	}

	if (n_bytes_remaining == 0) {
		if (Transfer_Buffer()) {
			Stop_Sampling();
		}
	}
}
/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1) {
		HAL_GPIO_TogglePin(ERROR_LED_GPIO_Port, ERROR_LED_Pin);
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
